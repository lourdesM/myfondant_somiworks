﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.Services" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %> <%--added--%>
<%@ Import Namespace="System.Web.Configuration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>JournalEntry</title>
    <script runat="server">
       
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdnSessionId.Value = (string)Request.QueryString["sessionId"];
            Fondant.myFondant.Web.Controllers.GLDataController GLDataController = new Fondant.myFondant.Web.Controllers.GLDataController();

            var JE_Id = (string)System.Web.HttpContext.Current.Application["JE_Id_" + this.hdnSessionId.Value];
            var isNewJE = (string)System.Web.HttpContext.Current.Application["isNewJE_" + this.hdnSessionId.Value];

            
            
            if (!Convert.ToBoolean(isNewJE))
            {
              
               DataSet temp = GLDataController.GetJEDetailReader(JE_Id);
                
                foreach (DataRow dr in temp.Tables[0].Rows)
                {
                    var location = dr["LocationInternalId"].ToString() + '-' + dr["LocationName"].ToString();
                    cboLocation.Items.Add(new Ext.Net.ListItem(location.ToString()));
                    txtJE_Id.Text = !Convert.IsDBNull(dr["Id"]) ? dr["Id"].ToString() : "0";
                    cboSubsidiary.Items.Add(new Ext.Net.ListItem(dr["Subsidiary"].ToString()));
                    dtClosedOrderDate.Text = dr["ClosedOrderDate"].ToString();
                    txtPosting_Period.Text = dr["Posting_Period"].ToString();
                    cboCurrency.Items.Add(new Ext.Net.ListItem(dr["gl_currency"].ToString()));
                    txtAccountNumber.Text = !Convert.IsDBNull(dr["gl_account"]) ? dr["gl_account"].ToString() : "";
                    txtAccountName.Text = dr["gl_account_name"].ToString();
                    txtDebit.Text = !Convert.IsDBNull(dr["DEBIT"]) ? dr["DEBIT"].ToString() : "";
                    txtCredit.Text = !Convert.IsDBNull(dr["CREDIT"]) ? dr["CREDIT"].ToString() : "";
                    txtMemo.Text = dr["Memo"].ToString();
                    txtClass.Text = dr["Class"].ToString();
                   
                }
                cboSubsidiary.SelectedItem.Index = 0;
                cboCurrency.SelectedItem.Index=0;
                cboLocation.SelectedItem.Index=0;
            }
            else
            {


                string closedOrderDates = (string)System.Web.HttpContext.Current.Application["JE_ClosedOrderDates_" + this.hdnSessionId.Value];
                string locationIds = (string)System.Web.HttpContext.Current.Application["JE_LocationIds_" + this.hdnSessionId.Value];
                string locationInternalIds = (string)System.Web.HttpContext.Current.Application["JE_LocationInternalIDs_" + this.hdnSessionId.Value];
                string entries = (string)System.Web.HttpContext.Current.Application["JE_Entries_" + this.hdnSessionId.Value];
                string startDate = (string)System.Web.HttpContext.Current.Application["StartDate_" + this.hdnSessionId.Value];
                string endDate = (string)System.Web.HttpContext.Current.Application["EndDate_" + this.hdnSessionId.Value];

                    //FOR GL Summary DataGrid            
                DataSet temp = GLDataController.GetGLDetailReader(closedOrderDates, locationIds, locationInternalIds, entries, startDate, endDate, (string)this.hdnSessionId.Value);
                    ArrayList arrLocationInternalId = new ArrayList();
                    ArrayList arrLocationName = new ArrayList();
                    ArrayList arrLocation = new ArrayList();
                    ArrayList arrSubsidiary = new ArrayList();
                    ArrayList arrCurrency = new ArrayList();

                    foreach (DataRow dr in temp.Tables[0].Rows)
                    {
                       
                       var location = dr["LocationInternalId"].ToString() + '-' + dr["LocationName"].ToString();
                       if (!arrLocation.Contains(location))
                        {
                                if (dr["IsPosted"].ToString() == "false")
                                {
                                arrLocation.Add(location);
                                cboLocation.Items.Add(new Ext.Net.ListItem(location.ToString()));
                                }                        
                        }
                        if (!arrSubsidiary.Contains(dr["Subsidiary"].ToString()))
                        {
                            arrSubsidiary.Add(dr["Subsidiary"].ToString());
                            cboSubsidiary.Items.Add(new Ext.Net.ListItem(dr["Subsidiary"].ToString()));
                        }

                        if (!arrCurrency.Contains(dr["gl_currency"].ToString()))
                        {
                            arrCurrency.Add(dr["gl_currency"].ToString());
                            cboCurrency.Items.Add(new Ext.Net.ListItem(dr["gl_currency"].ToString()));
                        }
                
                    }
                   
                
                        txtJE_Id.Text = "";
                        dtClosedOrderDate.Text = "";
                        txtPosting_Period.Text = "";
                        cboCurrency.Text = "";
                        txtAccountNumber.Text = "";
                        txtAccountName.Text  = "";
                        txtDebit.Text = "";
                        txtCredit.Text = "";
                        txtMemo.Text = "";
                        txtClass.Text = "";
                        cboSubsidiary.SelectedItem.Index = 0;
                        cboLocation.SelectedItem.Index = 0;
                        dtClosedOrderDate.Text = startDate.ToString();
            }

            
        }
    </script>
    <ext:XScript ID="XScript1" runat="server">
     <script type="text/javascript">
        //loadStore Timeout---
         Ext.Ajax.timeout = 1000000; //120000;
         Ext.net.DirectEvent.timeout = 1000000; // 120000;

         var saveJE = function () {
            var JE_Id= #{txtJE_Id}.getValue();
             var loc=(#{cboLocation}.getValue()).split("-");

             if(#{txtDebit}.getValue() != "" && #{txtCredit}.getValue() != "" && #{txtAccountNumber}.getValue() != "" && #{txtAccountName}.getValue() != ""){
                             if(JE_Id != "") {
                                        Ext.Ajax.request 
                                            ({ 
                                                url: '/GLData/saveModifiedJE/?sessionId='+document.getElementById("hdnSessionId").value, 
                                                method: 'POST',    
                                                params: {
                                                    JE_Id : #{txtJE_Id}.getValue(), 
                                                    Subsidiary : #{cboSubsidiary}.getValue(),
                                                    ClosedOrderDate : #{dtClosedOrderDate}.getValue(),
                                                    Posting_Period : #{txtPosting_Period}.getValue(),
                                                    gl_currency : #{cboCurrency}.getValue(),
                                                    gl_account : #{txtAccountNumber}.getValue(),
                                                    gl_account_name: #{txtAccountName}.getValue(),
                                                    debit: #{txtDebit}.getValue(),
                                                    credit: #{txtCredit}.getValue(),
                                                    Memo: #{txtMemo}.getValue(),
                                                    Class: #{txtClass}.getValue(), 
                                                    LocationInternalId: loc[0]
                                                                                
                                                },       
                                                success: function(response) 
                                                {                                     
                                                    Ext.Msg.show({
                                                        title: 'Save',
                                                        msg: 'Save successful.',
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.MessageBox.INFO,
                                                        fn: reloadPage
                                                    });
                                                }, 
                                                failure: function(response) 
                                                { 
                                                   alert('Error: ' + response);
                                                } 
         
                                            }); 
                                }
                                else {

                                        Ext.Ajax.request 
                                            ({ 
                                                url: '/GLData/saveNewJE/?sessionId='+document.getElementById("hdnSessionId").value, 
                                                method: 'POST',    
                                                params: {                                       
                                                    Subsidiary : #{cboSubsidiary}.getValue(),
                                                    ClosedOrderDate : #{dtClosedOrderDate}.getValue(),
                                                    Posting_Period : #{txtPosting_Period}.getValue(),
                                                    gl_currency : #{cboCurrency}.getValue(),
                                                    gl_account : #{txtAccountNumber}.getValue(),
                                                    gl_account_name: #{txtAccountName}.getValue(),
                                                    debit: #{txtDebit}.getValue(),
                                                    credit: #{txtCredit}.getValue(),
                                                    Memo: #{txtMemo}.getValue(),
                                                    Class: #{txtClass}.getValue(), 
                                                    LocationInternalId:loc[0]
                                                                                
                                                },       
                                                success: function(response) 
                                                {                                     
                                                    Ext.Msg.show({
                                                        title: 'Save',
                                                        msg: 'Save successful.',
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.MessageBox.INFO,
                                                        fn: reloadPage
                                                    });
                                                }, 
                                                failure: function(response) 
                                                { 
                                                   alert('Error: ' + response);
                                                } 
         
                                            }); 
                        
                                }
                    }
                    else
                    {
                       alert('Please complete required fields.');
                    }
         };

         function reloadPage() {
              parent.reloadGridPanel();
         }
     </script>
    </ext:XScript>
</head>
<body>
   <ext:ResourceManager ID="ResourceManager1" runat="server" />
   <ext:Viewport runat="server" Layout="FitLayout" >
   <Items>
    <ext:FormPanel runat="server" ID="JournalEntryForm" Title="Journal Entry" BodyStyle="background-color:#FFFFFF;padding:7px;" ButtonAlign="Right"
                 TitleCollapse="true" Url="/GLData/SaveJournalEntry/" StandardSubmit="false" > 
            <Items>
                <ext:TextField ID="txtJE_Id" runat="server" FieldLabel="JE Id"  MarginSpec="5 5 5 5" Width="300"  LabelWidth="100" Hidden="true"  />
                <ext:ComboBox ID="cboLocation" runat="server" Editable="false" FieldLabel="Location" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100"  />
                <ext:ComboBox ID="cboSubsidiary" runat="server" Editable="false" FieldLabel="Subsidiary" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" />
                <ext:DateField ID="dtClosedOrderDate" runat="server" FieldLabel="ClosedOrderDate" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" />
                <ext:TextField ID="txtPosting_Period" runat="server" FieldLabel="Postin Period" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100"  />
                <ext:ComboBox ID="cboCurrency" runat="server" FieldLabel="Currency" Width="300"  MarginSpec="5 5 5 5"  LabelWidth="100"  />
                <ext:TextField ID="txtAccountNumber" runat="server" FieldLabel="Account Number" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" AllowBlank="false" >
                     <AfterLabelTextTpl ID="AfterLabelTextTpl1" runat="server">
                          <Html>
                                                <span style="color:red;font-weight:bold" data-qtip="Required">*</span>
                          </Html>
                      </AfterLabelTextTpl> 
                </ext:TextField>
                <ext:TextField ID="txtAccountName" runat="server" FieldLabel="Account Name" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" AllowBlank="false" >
                 <AfterLabelTextTpl ID="AfterLabelTextTpl2" runat="server">
                          <Html>
                                                <span style="color:red;font-weight:bold" data-qtip="Required">*</span>
                          </Html>
                      </AfterLabelTextTpl> 
                </ext:TextField>
                <ext:TextField ID="txtDebit" runat="server" MaskRe="/[0-9.]/" FieldLabel="Debit" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" AllowBlank="false" >
                 <AfterLabelTextTpl ID="AfterLabelTextTpl3" runat="server">
                          <Html>
                                                <span style="color:red;font-weight:bold" data-qtip="Required">*</span>
                          </Html>
                      </AfterLabelTextTpl> 
                </ext:TextField>
                <ext:TextField ID="txtCredit" runat="server" MaskRe="/[0-9.]/" FieldLabel="Credit" Width="300" MarginSpec="5 5 5 5"  LabelWidth="100" AllowBlank="false" >
                 <AfterLabelTextTpl ID="AfterLabelTextTpl4" runat="server">
                          <Html>
                                                <span style="color:red;font-weight:bold" data-qtip="Required">*</span>
                          </Html>
                      </AfterLabelTextTpl> 
                </ext:TextField>
                <ext:TextField ID="txtMemo" runat="server" FieldLabel="Memo"  MarginSpec="5 5 5 5" Width="300" LabelWidth="100" />
                <ext:TextField ID="txtClass" runat="server" FieldLabel="Class"  MarginSpec="5 5 5 5" Width="300" LabelWidth="100"  />
            
            </Items>
            <Buttons>
                 <ext:Button ID="SaveButton" runat="server" Icon="Disk" Text="Save" >
                 <Listeners>
                    <Click Handler="saveJE()" />
                 </Listeners>
               </ext:Button>
            </Buttons>
   </ext:FormPanel>
   </Items>
   </ext:Viewport>
   <ext:Hidden ID="hdnSessionId" runat="server" />
</body>
</html>
