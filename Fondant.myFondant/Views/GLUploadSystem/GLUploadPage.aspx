﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

 <script type="text/javascript" src="/Scripts/myFondant.js"></script>
<script runat="server">
    public const string SQL_CONNECTION_STRING = "server=54.149.142.14;database=RN_THEDRYBAR_SETUP;user id=sa; Password=H9mhbr2k";
    private class StoreGLSummary
    {
        public string Subsidiary { get; set; }
        public string ClosedOrderDate { get; set; }
        public string Posting_Period { get; set; }
        public decimal Debit_Total { get; set; }
        public decimal Credit_Total { get; set; }
        public int LocationInternalId { get; set; }
        public string Status { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string Entries { get; set; }
        public string OutOfBalanceAmount { get; set; }
       
        
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.frmDateSearch.MinDate=DateTime.Now.AddDays(-15);
        this.frmDateSearch.MaxDate = DateTime.Now;

        this.toDateSearch.MinDate = DateTime.Now.AddDays(-15);
        this.toDateSearch.MaxDate = DateTime.Now;

        this.hdnSessionId.Value = Request.QueryString["sessionId"];
     
       if (!X.IsAjaxRequest)
        {
       
            Fondant.myFondant.Web.Controllers.GLDataController GLDataController = new Fondant.myFondant.Web.Controllers.GLDataController();
            this.toDateSearch.Text = this.frmDateSearch.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");

            //FOR GL Summary DataGrid     
           IList<StoreGLSummary> GLSummaryData = new List<StoreGLSummary>();
           
                   
            DataSet temp = GLDataController.GetDataSummaryReader("", "", 0, "");
            foreach (DataRow dr in temp.Tables[0].Rows)
            {
                StoreGLSummary GLData = new StoreGLSummary();
                GLData.Subsidiary = dr["Subsidiary"].ToString();
                GLData.ClosedOrderDate = dr["ClosedOrderDate"].ToString();
                GLData.Posting_Period = dr["Posting_Period"].ToString();
                GLData.Debit_Total = !Convert.IsDBNull(dr["Debit_Total"]) ? Convert.ToDecimal(dr["Debit_Total"]) : 0;
                GLData.Credit_Total = !Convert.IsDBNull(dr["Credit_Total"]) ? Convert.ToDecimal(dr["Credit_Total"]) : 0;
                GLData.LocationInternalId = !Convert.IsDBNull(dr["LocationInternalId"]) ? Convert.ToInt32(dr["LocationInternalId"]) : 0;
                GLData.Status = dr["Status"].ToString();
                GLData.LocationID = !Convert.IsDBNull(dr["LocationID"]) ? Convert.ToInt32(dr["LocationID"]) : 0;
                GLData.LocationName = dr["LocationName"].ToString();
                GLData.Entries = (!Convert.IsDBNull(dr["Debit_Total"]) && !Convert.IsDBNull(dr["Credit_Total"])) ? (Convert.ToDecimal(dr["Debit_Total"]) == Convert.ToDecimal(dr["Credit_Total"])) ? "balanced" : "unbalanced" : "";
                GLData.OutOfBalanceAmount = (Convert.ToDecimal(dr["Debit_Total"]) > Convert.ToDecimal(dr["Credit_Total"])) ? (Convert.ToDecimal(dr["Debit_Total"]) - Convert.ToDecimal(dr["Credit_Total"])).ToString() : (Convert.ToDecimal(dr["Credit_Total"]) - Convert.ToDecimal(dr["Debit_Total"])).ToString();
                GLSummaryData.Add(GLData);
                
            }
            
            this.strGLUpload.DataSource = GLSummaryData;
            this.strGLUpload.DataBind();
           
            //For Locations combobox
            cboLocation.Items.Add(new Ext.Net.ListItem("All Locations", "All Locations"));
            DataSet dsLocations = GLDataController.GetAllLocations();
            foreach (DataRow dr in dsLocations.Tables[0].Rows)
            {
                cboLocation.Items.Add(new Ext.Net.ListItem(dr["LocationName"].ToString(), dr["LocationID"]));
            }
            
            //For All Status combobox
            cboAllStatus.Items.Add(new Ext.Net.ListItem("All Status"));
            cboAllStatus.Items.Add(new Ext.Net.ListItem("posted"));
            cboAllStatus.Items.Add(new Ext.Net.ListItem("unposted"));

            //For All Entries combobox
            cboAllEntries.Items.Add(new Ext.Net.ListItem("All Entries"));
            cboAllEntries.Items.Add(new Ext.Net.ListItem("balanced"));
            cboAllEntries.Items.Add(new Ext.Net.ListItem("unbalanced"));
            
            X.Call("HideLoadIcon");
        }
    }
  
   

    
</script>
 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>GLUploadPage</title>
       <script src="../../Scripts/jquery-1.11.1.min.js" type="text/javascript"></script>
     <style type="text/css">
     
        #loadingmessage, #postingMessage, #loadingmessage1
        {
            position:fixed;
            opacity:0.5;
            top:0px;
            left:0px;
            height:100%;
            width:100%;
            background-color:#ccc;
            z-index:1000;
            cursor:progress;
        }
        #spinner, #spinnerPosting, #spinner1
        {
            position:fixed;
            top:50%;
            left:0%;
            height:100%;
            width:100%;
            text-align:center;
            vertical-align:middle;
        
        }
    </style>
  
    <ext:XScript ID="XScript1" runat="server">
     <script type="text/javascript">

     //loadStore Timeout---
        Ext.Ajax.timeout = 1000000;//120000;
        Ext.net.DirectEvent.timeout = 1000000;// 120000;


         window.onload= function(){
         LoadIcon1();
         }
         
          function LoadIcon() {
             $("#loadingmessage").show();
         }
          function LoadIcon1() {
             $("#loadingmessage1").show();
         }

         function HideLoadIcon() {
             $("#loadingmessage").hide();
              $("#loadingmessage1").hide();
         }

          function LoadPostIcon() {
             $("#postingMessage").show();
         }

         function HidePostIcon() {
             $("#postingMessage").hide();
         }

        //loadStore TIMEOUT---
        Ext.Ajax.timeout = 1000000;//120000;
        Ext.net.DirectEvent.timeout = 1000000;// 120000;

         // SUCCESS FILTER ----
         var successFilter = function (result) {     
         var params= Ext.JSON.decode(result.responseText); 
         #{strGLUpload}.removeAll();        
         #{strGLUpload}.loadRawData(params.result); 
         Ext.getCmp('cboLocation').enable(); 
         Ext.getCmp('cboAllStatus').enable();
         Ext.getCmp('cboAllEntries').enable();
         Ext.getCmp('btnClearSearch').enable();
         HideLoadIcon();
         };

         // APPLY FILTER ----
          var f = [];          
          var filterLocation=true;
          var filterStatus=true;  
          var filterEntries=true;
       
        var applyFilter = function () {                
                var store = #{grdGLUpload}.getStore();
                var locationVal=#{cboLocation}.getDisplayValue();
                var statusVal=#{cboAllStatus}.getDisplayValue(); 
                var entryVal=#{cboAllEntries}.getDisplayValue(); 

                if(locationVal=="All Locations" && statusVal=="All Status" && entryVal == "All Entries"){                   
                   #{strGLUpload}.clearFilter();                   
                }

                else if(locationVal != "All Locations" && statusVal != "All Status" && entryVal != "All Entries" ){
                    filterLocation=true;
                    filterStatus=true; 
                    filterEntries=true;
                    store.filterBy(getRecordFilter());
                }

                else if(locationVal =="All Locations" && statusVal != "All Status" && entryVal != "All Entries"){
                    filterLocation=false;
                    filterStatus=true;
                    filterEntries=true;
                    store.filterBy(getRecordFilter());
                }

                else if(locationVal != "All Locations" && statusVal == "All Status" && entryVal != "All Entries"){
                      filterLocation=true;
                      filterStatus=false;
                      filterEntries=true;
                      store.filterBy(getRecordFilter());
                } 
                
                else if(locationVal != "All Locations" && statusVal != "All Status" && entryVal == "All Entries"){
                      filterLocation=true;
                      filterStatus=true;
                      filterEntries=false;
                      store.filterBy(getRecordFilter());
                }                                  
         };

         // GET RECORD FILTER ----
          var getRecordFilter = function () {
             
                    f.push({
                        filter: function (record) {      
                                 return filterString( filterLocation ? #{cboLocation}.getDisplayValue() : "", "LocationName", record);
                       
                        }
                    });
           

           
                    f.push({
                        filter: function (record) {          
                                return filterNumber(filterStatus ? #{cboAllStatus}.getDisplayValue() : "", "Status", record);
                        }
                    });

                     f.push({
                        filter: function (record) {          
                                return filterNumber(filterEntries ? #{cboAllEntries}.getDisplayValue() : "", "Entries", record);
                        }
                    });
            

               /* f.push({
                    filter: function (record) {        
                                     
                        return filterDate(#{frmDateSearch}.getValue(), #{toDateSearch}.getValue(), "business_date", record);
                    }
                });*/
                
                var len = f.length;
                 
                return function (record) {
                    for (var i = 0; i < len; i++) {
                        if (!f[i].filter(record)) {
                            return false;
                        }
                    }
                    return true;
                };
            };

            //FILTER STRING ---
             var filterString = function (value, dataIndex, record) {
                var val = record.get(dataIndex);
                
                if (typeof val != "string") {
                    return value.length == 0;
                }
                
                return val.toLowerCase().indexOf(value.toLowerCase()) > -1;
            };
            //FILTER DATE -----
            var filterDate = function (value, value2, dataIndex, record) {
                var val = Ext.Date.clearTime(record.get(dataIndex), true).getTime();
 
                if (!Ext.isEmpty(value, false) && val < Ext.Date.clearTime(value, true).getTime()) {
                    return false;
                }
                if (!Ext.isEmpty(value2, false) && val > Ext.Date.clearTime(value2, true).getTime()) {
                    return false;
                }
                return true;
            };
            //FILTER NUMBER ----
            var filterNumber = function (value, dataIndex, record) {
                var val = record.get(dataIndex);                
 
                if (!Ext.isEmpty(value, false) && val != value) {
                    return false;
                }
                
                return true;
            };

            //CLEAR FILTER----
            var clearFilter = function () {
            
                #{cboLocation}.reset();
                #{cboAllStatus}.reset();
                #{cboAllEntries}.reset();
                /*Ext.getCmp('cboLocation').setValue(#{cboLocation}.getStore().getAt('0'));
                Ext.getCmp('cboAllStatus').setValue(#{cboAllStatus}.getStore().getAt('0'));*/
                #{strGLUpload}.clearFilter();
             
            }

            //FILTER DATA SUMMARY READER---
             var applyDateFilter= function (){
             try {
              clearFilter(null);
              Ext.getCmp('cboLocation').disable(); 
              Ext.getCmp('cboAllStatus').disable();
              Ext.getCmp('cboAllEntries').disable();
              Ext.getCmp('btnClearSearch').disable();
              var frmDate=#{frmDateSearch}.getValue();
              var toDate=#{toDateSearch}.getValue();
                     
                 if(frmDate!="" && toDate != "" && frmDate!=null && toDate != null){
                      var fDate= new Date(frmDate);
                      var tDate= new Date(toDate);
                         if(tDate >= fDate) {
                              Ext.Ajax.request ({ 
                                                    url: '/GLData/FilterGLSummaryByDate/',
                                                    method: 'POST', 
                                                    type:'Load',   
                                                    params: {
                                                        frmDate : frmDate, 
                                                        toDate: toDate,                                                        
                                                    },       
                                                    success: function(response) 
                                                    {                                     
                                                        successFilter(response);
                                                    }, 
                                                    failure: function(response) 
                                                    { 
                                                         HideLoadIcon();
                                                    } 
                                 });   
                         }
                         else {
                           HideLoadIcon();
                            alert('Invalid Date Range!');
                         }
                   }
                   else {
                   HideLoadIcon();
                   alert('Please select a date range!');
                   }
                }
                catch (e) {
                         Ext.Msg.alert("Submit", "Your session has timed out.");
                         window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                }
             }


          //VIEW ITEM DETAIL---

          var viewItemDetail= function (){
           try{
           
             var ClosedOrderDates="";
             var LocationIDs="";
             var LocationInternalIds="";
             var Status="";
             var Entries="";
         
             var frmDate=getFormattedDate(#{frmDateSearch}.getValue());
             var toDate=getFormattedDate(#{toDateSearch}.getValue());
             var records = #{grdGLUpload}.selModel.getSelection(); // #{grdGLUpload}.getRowsValues({selectedOnly:true}); 
             
                     if(records.length > 0){
                            for (var i = 0; i < records.length; i++){                
                                    if(i != 0 && i != records.length){
                                        ClosedOrderDates += ",";
                                        LocationIDs += ",";
                                        LocationInternalIds += ",";
                                        Status+= ",";        
                                        Entries+=",";                                
                                    }
                                    
                                    ClosedOrderDates += "'" + getFormattedDate(records[i].data.ClosedOrderDate) + "'";
                                    LocationIDs +=records[i].data.LocationID;
                                    LocationInternalIds +=records[i].data.LocationInternalId;
                                    Status+= "'" + records[i].data.Status + "'";
                                    Entries +=  "'" + records[i].data.Entries + "'";
                             }


                               Ext.Ajax.request 
                                ({ 
                                    url: '/GLData/storeSelectedItems/?sessionId='+document.getElementById("hdnSessionId").value, 
                                    method: 'POST',    
                                    params: {
                                        ClosedOrderDates : ClosedOrderDates, 
                                        LocationIDs : LocationIDs, 
                                        LocationInternalIds : LocationInternalIds, 
                                        Entries : Entries,
                                        Status : Status,
                                        StartDate : frmDate,
                                        EndDate : toDate
                                    },       
                                    success: function(response) 
                                    {             
                                                            
                                      addTab(parent.Ext.getCmp('tpMain'),'GL Detail Report','/GLUploadSystem/GLDetailReport/?sessionId='+document.getElementById("hdnSessionId").value)
                                    }, 
                                    failure: function(response) 
                                    { 
                                       addTab(parent.Ext.getCmp('tpMain'),'GL Detail Report','/GLUploadSystem/GLDetailReport/?sessionId='+document.getElementById("hdnSessionId").value)
                                    } 
         
                                });   
                      }
                    else {
                            alert("Please select an item!");
                    }
               }
               catch (e) {
                        
                         Ext.Msg.alert("Submit", "Your session has timed out.");
                         window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                       
             }
          }

          function getFormattedDate(dt) {
              var date = new Date(dt);
              var year = date.getFullYear();
              var month = (1 + date.getMonth()).toString();
              month = month.length > 1 ? month : '0' + month;
              var day = date.getDate().toString();
              day = day.length > 1 ? day : '0' + day;
              return month + '/' + day + '/' + year;
            }

           //POST ENTRY-----
           var PostEntry={
           
                submitData: function (source) {
                      try {
                                var LocationInternalId="";
                                var ClosedOrderDates="";
                                var LocationId="";
                                var Status="";
                                var frmDate=getFormattedDate(#{frmDateSearch}.getValue());
                                var toDate=getFormattedDate(#{toDateSearch}.getValue());
                                var shouldNotPost=false;
                                var isOutOfBalance=false;
                                var records = #{grdGLUpload}.selModel.getSelection(); //#{grdGLUpload}.getRowsValues({selectedOnly:true});                    
                  
                                if(records.length > 0){
                                  LoadPostIcon();
                                        for (var i = 0; i < records.length; i++){
                                   
                                           if(records[i].data.Status == "posted" ){
                                              shouldNotPost=true;
                                           }
                                           if(records[i].data.Entries == "unbalanced"){
                                                isOutOfBalance=true;
                                           }

                                           /* commented 4/25/2016- Disable Post button if Total Debit and Total Credit are not balance
                                           if(parseFloat(records[i].data.debit_total) != parseFloat(records[i].data.credit_total)){
                                                shouldNotPost=true;
                                           }*/

                                            if(i != 0 && i != records.length){
                                             ClosedOrderDates += ",";
                                             LocationInternalId += ",";
                                             LocationId+= ",";
                                             Status += ",";
                                            }
                                            ClosedOrderDates += "'" + getFormattedDate(records[i].data.ClosedOrderDate) + "'";
                                            LocationInternalId +=records[i].data.LocationInternalId;
                                            LocationId += records[i].data.LocationId;
                                            Status += records[i].data.Status;
                                   
                                        }

                                      if(shouldNotPost){
                                         alert('An item has already been posted!');
                                          HidePostIcon();
                                      } 
                                      else if (isOutOfBalance){
                                         alert('Unable to post Out of Balance Amount!');
                                          HidePostIcon();
                                      }            
                                      else{
                                      var n;
                                      //UPDATE STATUS
                                       Ext.Ajax.request 
                                        ({ 
                                            url: '/GLData/GetUpdatedEntriesJS/',
                                            method: 'POST',    
                                            params: {
                                                ClosedOrderDates : ClosedOrderDates, 
                                                LocationIDs : LocationId,                                        
                                                LocationInternalIds: LocationInternalId,
                                                Status: Status,
                                                StartDate: frmDate,
                                                EndDate: toDate
                                            },       
                                            success: function(response) 
                                            {              
                                            var str=response.responseText;
                                             n = str.indexOf("'unbalanced'");   
                                                
                                                if(n == -1){
                                                  Ext.Ajax.request 
                                                    ({ 
                                                    url: '/GLData/PostGLEntry/?sessionId='+document.getElementById("hdnSessionId").value, 
                                                    method: 'POST',    
                                                    params: {
                                                    ClosedOrderDates : ClosedOrderDates, 
                                                    LocationInternalIds : LocationInternalId,
                                                    StartDate : frmDate,
                                                    EndDate : toDate
                                                    },       
                                                    success: function(response) 
                                                    {          
                                                     var params= Ext.JSON.decode(response.responseText);  
                                                                              
                                                       if(params.extraParamsResponse.messageSuccess=="Success!"){
                                                            Ext.Msg.alert("Submit", "Operation was successful");
                                                       }
                                                       else{
                                                         Ext.Msg.alert("Submit", params.extraParamsResponse.messageSuccess);
                                                        }
                                                          HidePostIcon();
                                                          LoadIcon1();
                                                          applyDateFilter();
                                                    }, 
                                                    failure: function(response) 
                                                    { 
                                                       HidePostIcon();
                                                       Ext.Msg.alert("Submit", "Your session has timed out.");
                                                       window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                                              
                                                    } 
         
                                                });
                                           
                                         }
                                         else{
                                           alert('Unable to post Out of Balance Amount!');
                                           HideLoadIcon();
                                           HidePostIcon();
                                         }                    
                                    
                                            }, 
                                             failure: function(response) 
                                             { 
                                                  HideLoadIcon();
                                             } 
         
                                        });

                                        

                                         
                                        } 
                                    }
                            else {
                                    alert("Please select an item!");
                                    HideLoadIcon();
                                    HidePostIcon();
                            }
                    }
                    catch (e) {
                        
                         Ext.Msg.alert("Submit", "Your session has timed out.");
                         window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                       
                    }
                 }
                   
              };


          //CHECKS if Credit Total and Debit Total are balance for each item row, if not then fon't color would change and the Post button would be disabled.
               
               //Change font color======
                var debitValue="";
                var creditValue=""
                var template = '<span style="color:{0};">{1}</span>';
                var getDebitValue = function (value) {
                     debitValue=value;
                     return Ext.String.format(template,"transparent", value);
                };
                 var getCreditValue = function (value) {
                     creditValue=value;
                     return Ext.String.format(template,"transparent", value);
                };

                 var fontColorChangeIfDebitNotBalance = function (value) {
                 
                   return Ext.String.format(template, (parseFloat(value) == parseFloat(creditValue)) ? "green" : "red", value);
                };

                 var fontColorChangeIfCreditNotBalance = function (value) {
                   return Ext.String.format(template, (parseFloat(value) == parseFloat(debitValue)) ? "green" : "red", value);
                 
                };

                 var fontColorChangeIfNotBalance = function (value) {
                   return Ext.String.format(template, (parseFloat(creditValue) == parseFloat(debitValue)) ? "green" : "red", value);
                 
                };

                var fontColorChangeIfPosted= function(value){
                 return Ext.String.format(template, (value == 'posted') ? "green" : "black", value);
                }



                //disable each Post Entry button in the Grid depending on the Status of each item row======
                 var prepare = function (grid, toolbar, rowIndex, record) {
                        var postEntryButton = toolbar.items.get(1);

                        if (record.data.Status == "posted" || record.data.Entries == "unbalanced"){
                            postEntryButton.setDisabled(true);
                            postEntryButton.setTooltip("Disabled");
                        }
                      

                      /*commented 4/25/2016- Disable Post button if Total Debit and Total Credit are not balance
                         if (parseFloat(record.data.Debit_Total) != parseFloat(record.data.Credit_Total)) {
                            postEntryButton.setDisabled(true);
                            postEntryButton.setTooltip("Disabled");
                        }*/
            
                       
                    };


                    //CHECK ITEM Status WHEN SELECT HANDLER IS TRIGGERED======
                    //This will disable or enable Post Entry and View Detail buttons at the top right side of the grid depending on the Status.
                   
                    var checkStatus = function () {                  
                            var shouldNotPost=false;       
                            var isOutOfBalance=false;        
                            source= App.grdGLUpload;
                        
                        if(source.selModel.hasSelection()){
                           var records = source.selModel.getSelection();
                            if(records.length > 0){
                                for (var i = 0; i < records.length; i++){
                
                                   if(records[i].data.Status == "posted"){
                                     shouldNotPost=true;
                                   }
                                   if (records[i].data.Entries == "unbalanced"){
                                      isOutOfBalance=true;
                                   }
                                   /* commented 4/25/2016- Disable Post button if Total Debit and Total Credit are not balance
                                   if(parseFloat(records[i].data.debit_total) != parseFloat(records[i].data.credit_total)){
                                        shouldNotPost=true;
                                   }*/
                                }
                             }   

                          if(shouldNotPost || isOutOfBalance){
                              Ext.getCmp('btnViewDetail').enable();
                              Ext.getCmp('btnPostEntry').disable();
                          }     
                          else{
                               Ext.getCmp('btnViewDetail').enable();
                               Ext.getCmp('btnPostEntry').enable();
                          }                         
                     
                      }
                      else{
                         Ext.getCmp('btnViewDetail').disable();
                         Ext.getCmp('btnPostEntry').disable();
                      }
                        
                     };  
                       


    </script>
    </ext:XScript>
  
</head>
<body>
   <form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManager1" runat="server" Theme="Gray" DirectMethodNamespace="Transactions" />
  
    <ext:Viewport ID="Viewport1" runat="server" Layout="FitLayout" >            
            <Items>            
                <ext:GridPanel   
                    ID="grdGLUpload"                  
                    runat="server"
                    Region="Center"
                    ColumnWidth="1"
                    ColumnHeight="2"
                    Border="true"
                    Flex="2"
                    Height="590"
                    MarginSpec="0 0 0 0"
                    Frame="false">
                    <Store>
                     <ext:Store ID="strGLUpload" runat="server" >
                            <Model>
                                <ext:Model ID="Model1" runat="server"  Name="GLUpload">
                                    <Fields>                                     
                                        <ext:ModelField Name="Subsidiary" />
                                        <ext:ModelField Name="ClosedOrderDate"  />
                                        <ext:ModelField Name="Posting_Period"/>
                                        <ext:ModelField Name="Debit_Total"/>
                                        <ext:ModelField Name="Credit_Total" />
                                        <ext:ModelField Name="LocationInternalId" Type="Int" />                                    
                                        <ext:ModelField Name="Status" />                                        
                                        <ext:ModelField Name="LocationID" Type="Int" />
                                        <ext:ModelField Name="LocationName"  />
                                        <ext:ModelField Name="Entries"  />
                                        <ext:ModelField Name="OutOfBalanceAmount"  />
                                    </Fields>
                                </ext:Model>
                            </Model>                           
                        </ext:Store>
                    </Store>
                    <Plugins>
                        <ext:CellEditing ID="CellEditing1" runat="server" ClicksToEdit="1" />
                    </Plugins>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                         <ext:Label ID="Label1" runat="server" Text="SEARCH BY " MarginSpec="0 0 5 10"  StyleSpec="font-weight: bold"></ext:Label> 
                                         <ext:DateField ID="frmDateSearch" Name="frmDate" runat="server" FieldLabel="From"  MarginSpec="0 0 5 10"  LabelWidth="40" Width="180" >
                                         </ext:DateField>
                                         <ext:DateField ID="toDateSearch" Name="toDate" runat="server" FieldLabel="To"  MarginSpec="0 0 5 10" LabelWidth="40" Width="180" >
                                         </ext:DateField>
                                         <ext:Button ID="btnSearch" runat="server"  Text="Search" MarginSpec="0 0 0 5" Icon="Magnifier" StyleSpec="background-color:#f9f9f9;background-image:none;" >                                             
                                            <Listeners>
                                                    <Click Handler="LoadIcon();applyDateFilter()" />
                                            </Listeners>
                                         </ext:Button>  

                                           <ext:Label ID="Label2" runat="server" Text="FILTER BY " MarginSpec="0 0 5 10"  StyleSpec="font-weight: bold"></ext:Label> 
                                         <ext:SelectBox ID="cboLocation" Name="location" runat="server" FieldLabel="Location" EmptyText="All Locations"   MarginSpec="0 0 5 10" LabelWidth="50"  Width="200"  >
                                            
                                            <Listeners>
                                                    <Select Handler="applyFilter()" />
                                            </Listeners>
                                         </ext:SelectBox>
                                         <ext:SelectBox ID="cboAllStatus" Name="allStatus" runat="server" FieldLabel="Status" EmptyText="All Status"  MarginSpec="0 0 5 10" LabelWidth="40"  Width="150"  >
                                         <Listeners>
                                                    <Select Handler="applyFilter()" />
                                            </Listeners>
                                         </ext:SelectBox>
                                         <ext:SelectBox ID="cboAllEntries" Name="allEntries" runat="server" FieldLabel="Entries" EmptyText="All Entries"  MarginSpec="0 0 5 10" LabelWidth="40"  Width="150"  >
                                         <Listeners>
                                                    <Select Handler="applyFilter()" />
                                            </Listeners>
                                         </ext:SelectBox>
                                         <ext:Hidden ID="hdnLocationInternalId" runat="server" Text="" />
                                      
                                         <ext:Button ID="btnClearSearch" runat="server"  Text="Clear Filter" MarginSpec="0 0 0 5" Icon="Cancel" StyleSpec="background-color:#f9f9f9;background-image:none;" >                                             
                                            <Listeners>
                                                    <Click Handler="clearFilter(null)"  />
                                            </Listeners>
                                         </ext:Button>   
                                           
                                        <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                         <ext:Button ID="btnViewDetail" runat="server" icon="FolderPageWhite" Text="View Detail" Disabled="true" >
                                           <Listeners>                                           
                                                <Click Handler="viewItemDetail();" />
                                           </Listeners>                                           
                                         </ext:Button>
                                        
                                        <ext:Button ID="btnPostEntry" runat="server" icon="ApplicationGet" Text="Post Entries" Disabled="true" >
                                         <Listeners>
                                            <Click Handler="PostEntry.submitData();" />
                                        </Listeners>                                        
                                        </ext:Button>
                           </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <ColumnModel ID="ColumnModel1" runat="server" >
                        <Columns >
                            <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Width="30" />
                            <ext:Column ID="colLocationID" runat="server" Text="LocationID"  Width="50" DataIndex="LocationID" Hidden="true"  />
                            <ext:Column ID="hdnDebit_Total" runat="server"  DataIndex="Debit_Total" width="0" ColumnWidth="1" Resizable="false"   >
                                <Renderer Fn="getDebitValue" />
                            </ext:Column>
                            <ext:Column ID="hdnCredit_Total" runat="server"  DataIndex="Credit_Total" width="0" ColumnWidth="1" Resizable="false"  >
                                <Renderer Fn="getCreditValue" />
                            </ext:Column>
                             <ext:Column ID="colLocationInternalId" runat="server" Text="LocationInternalId" Width="100" DataIndex="LocationInternalId"  />
                            <ext:Column ID="colLocationName" runat="server" Text="Location" Flex="1" DataIndex="LocationName"   />
                            <ext:Column ID="colSubsidiary" runat="server" Text="Subsidiary" Flex="1" DataIndex="Subsidiary"  />
                            <ext:DateColumn ID="colClosedOrderDate" runat="server" Text="Date" Flex="1" DataIndex="ClosedOrderDate" />
                            <ext:Column ID="colDebit_Total" runat="server" Text="Total Debit" Flex="1" DataIndex="Debit_Total"  >
                                <Renderer Fn="fontColorChangeIfDebitNotBalance" />
                            </ext:Column>
                            <ext:Column ID="colCredit_Total" runat="server" Text="Total Credit" Flex="1" DataIndex="Credit_Total" >
                                  <Renderer Fn="fontColorChangeIfCreditNotBalance" />
                            </ext:Column>
                             <ext:Column ID="colOutOfBalanceAmount" runat="server" Text="Out of Balance Amount" Flex="1" DataIndex="OutOfBalanceAmount" >
                                  <Renderer Fn="fontColorChangeIfNotBalance" />
                            </ext:Column>
                            <ext:Column ID="colEntries" runat="server" Text="Entries" Flex="1" DataIndex="Entries" Hidden="true"  />
                             
                            <ext:Column ID="colStatus" runat="server" Text="Status" DataIndex="Status"   >
                             <Renderer Fn="fontColorChangeIfPosted" />
                            </ext:Column>
                            <ext:CommandColumn ID="CommandColumn1" runat="server" Flex="1">
                                <Commands>
                                    <ext:GridCommand  CommandName="View" Text="View" icon="FolderPageWhite"  >
                                   
                                    </ext:GridCommand>
                                    <ext:GridCommand CommandName="Post" Text="Post" icon="ApplicationGet"  />
                                </Commands>
                                 <PrepareToolbar Fn="prepare" />
                                <Listeners>
                                    <Command Handler="command=='View' ? viewItemDetail() : PostEntry.submitData();" />
                                </Listeners>                        
                            </ext:CommandColumn>
                                                                        
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" >                                   
                                    <Listeners>
                                    <SelectionChange Handler="#{hdnLocationInternalId}.setValue(#{grdGLUpload}.getSelectionModel().hasSelection() ? #{grdGLUpload}.getSelectionModel().getSelection()[0].data.LocationInternalId :''); checkStatus();" />
                                    </Listeners>
                                </ext:CheckboxSelectionModel>                             
                      </SelectionModel>
                           
                </ext:GridPanel>     
                 
          </Items>                    
   </ext:Viewport>
   <ext:Hidden ID="hdnSessionId" runat="server" />
     <div id="loadingmessage" style="display:none;" >
            <div id="spinner" style=" text-align:center">
            <label id = "Label3" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">Searching <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
        <div id="loadingmessage1" style="display:none;" >
            <div id="spinner1" style=" text-align:center">
            <label id = "Label4" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">loading <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
        <div id="postingMessage" style="display:none;" >
            <div id="spinnerPosting" style=" text-align:center">
            <label id = "Label4" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">Posting <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
   </form>
</body>
</html>
