﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Assembly="System.Web.ApplicationServices" Namespace="System.Web.Security" TagPrefix="web" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %>
<%@ Import Namespace="System.Web.Configuration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script runat="server">    
        protected void Page_Load(object sender, EventArgs e)    
        {
            if (!X.IsAjaxRequest)
            {
                ViewData["LoginUserClientID"] = (string)System.Web.HttpContext.Current.Application["LoginUserClientID"];
                this.txtLoginUserClientID.Text = (string)System.Web.HttpContext.Current.Application["LoginUserClientID"];
                this.ResourceManagerAdmin.RegisterIcon(Icon.Information);
               // this.Store1.AutoLoadParams.Add(new Ext.Net.Parameter { Name = "ClientId", Value = Profile.GetProfile("50200_admin").ClientId.ToString(), Mode = ParameterMode.Raw });
                this.Store1.AutoLoadParams.Add(new Ext.Net.Parameter { Name = "ClientId", Value = ViewData["LoginUserClientID"].ToString(), Mode = ParameterMode.Raw });
               
               
                //added
                Fondant.myFondant.Web.Controllers.DataController datacontroller = new Fondant.myFondant.Web.Controllers.DataController();
                Object temp = datacontroller.GetUsers3(Convert.ToInt32(ViewData["LoginUserClientID"]));
                this.Store1.DataSource = temp;
                this.Store1.DataBind();
                //end of added
                
                
            }
           
        }
       
        [DirectMethod(ShowMask = true)]
        public void ResetPassword(string userName)
        {
            string strPwd = string.Empty;
            string strMsg = string.Empty;
            try
            {
                if (userName == string.Empty || userName == "")
                {
                    strMsg = "Please select a username to reset password.";
                }
                else
                {
                    // find out first if User's PasswordFormat in DB is 1, if so, update to 2 before calling ResetPassword
                    
                    strPwd = Membership.Provider.ResetPassword(userName, string.Empty);
                    strMsg = "New password was sent to " + userName + ".";

                    myFondantUtilities myFUtils = new myFondantUtilities();
                    string strEmailMsg = myFUtils.CreateResetPasswordMessage(userName,strPwd);
                  
                    string mUser = this.ViewData["email"].ToString(); //modified
                  
                    if (mUser == string.Empty || mUser == "")
                    {
                        strMsg = "No email was found under the username provided.  Please contact your company administrator.";
                    }
                    else
                    {
                        
                        myFUtils.SendEmail(mUser, "Your myFondant Password", strEmailMsg); //modified
                    }
                }
            }
            catch (Exception ex)
            {
                strMsg = string.Concat("The password could not be reset at this time.  ", ex.Message);
            }
            finally
            {
                Notification.Show(new NotificationConfig
                {
                    Title = "Reset Password",
                    Icon = Icon.Information,
                    AlignCfg = new NotificationAlignConfig
                    {
                        ElementAnchor = AnchorPoint.BottomRight,
                        TargetAnchor = AnchorPoint.BottomRight,
                        OffsetX = -50,
                        OffsetY = -50
                    },
                    ShowFx = new Frame { Color = "#FCD856", Count = 1, Options = new FxConfig { Duration = 5 } },
                    HideFx = new SwitchOff(),
                    Html = strMsg
                });
            }
        }
    </script>
    <ext:XScript ID="XScript2" runat="server">
    <script type="text/javascript" language="javascript">
        Ext.Ajax.timeout = 120000;
        Ext.net.DirectEvent.timeout = 120000;

        var fullName = function(value, metadata, record, rowIndex, colIndex, store) {
            var suffix = record.data.Suffix || '';
            var middlename = record.data.MiddleName || '';
            return '<b>' + record.data.LastName + ' ' + suffix + ', ' + record.data.FirstName + ' ' + middlename + '</b>';
        }

        var commandHandler = function (cmd, record) {
            
            switch (cmd) {
                case "edit":
                    
                    var winUDW = Ext.getCmp('UserDetailWindow');
                    winUDW.getLoader().baseParams = { UserId: record.get('ID'), ASPNETUserId: record.get('UserId'), IsNewUser: false };
                    winUDW.setTitle('Edit ' + record.get('LastName')); //added
                    winUDW.getLoader().load();
                    winUDW.show();
                    break;
                case "delete":
                    Ext.Msg.confirm('Alert', 'Delete Record?', function (btn) {
                        if (btn == "yes") {
                            dsCustomers.remove(record);
                            dsCustomers.save();
                        }
                    });
                    break;
                case "new":
                    var winNUW = NewUserWindow;
                    winNUW.setTitle('New User');
                    winNUW.show();
                    break;
                case "resetPwd":
                    Ext.Msg.confirm('Reset Password', 'Reset and email ' + record.data.UserName + ' their new password?', function (btn) {
                        if (btn == "yes") {
                            if (record.data.UserName == null || record.data.UserName == "")
                            {
                                 Ext.Msg.alert("Reset Password", "Please select a username to reset password.");
                            }
                            else if(record.data.LoweredEmail == null || record.data.LoweredEmail == "") 
                            {
                                 Ext.Msg.alert("Reset Password", "No email was found under the username provided.  Please contact your company administrator.");
                            }
                            else if (record.data.UserName != null && record.data.UserName != "" && record.data.LoweredEmail != null && record.data.LoweredEmail != "")
                            {
                                ResetPassword(record.data.UserName, record.data.LoweredEmail);
                            }
                            else
                            {   
                                 Ext.Msg.alert("Reset Password", "Error Unknown!");
                            }
                           // Ext.net.DirectMethods.ResetPassword(record.data.UserName);

                        }
                    });
                    break;
            }
        }
        function ResetPassword(UserName, EmailAddress){
         Ext.Ajax.request ({ 
                    url: '/Data/ResetPassword/', 
                    method: 'POST', 
                    type:'Load',   
                    params: {
                        userName: UserName,
                        userEmailAdd: EmailAddress                                                     
                    },       
                    success: function(response) 
                    {  
                       var params= Ext.JSON.decode(response.responseText);   
                        if(params.extraParamsResponse.messageSuccess=="Success"){
                            Ext.Msg.alert("Reset Password", "Password has been reset successfully!");
                        }
                        else{
                            Ext.Msg.alert("Reset Password", params.extraParamsResponse.messageSuccess);
                        }
                    }, 
                    failure: function(response) 
                    { 
                         Ext.Msg.alert("Reset Password","Operation failed due to an Internal Error!"); 
                    }                                 
           });
        }
        function nextUserDetail(ID, userId) {
          
            var win = Ext.getCmp('UserDetailWindow');
            win.getLoader().params = { UserId: ID, ASPNETUserId: userId, IsNewUser: true };
            win.setTitle('New User');
            win.show();
        }
        //start
        var CompanyX = {
            _index: 0,

            getIndex: function () {
                return this._index;
            },

            setIndex: function (index) {
                if (index > -1 && index < App.GridPanel1.getStore().getCount()) {
                    this._index = index;
                }
            },

            getRecord: function () {
                var rec = App.GridPanel1.getStore().getAt(this.getIndex());  // Get the Record

                if (rec != null) {
                    return rec;
                }
            },

            edit: function (index) {
                this.setIndex(index);
                this.open();
            },

            next: function () {
                this.edit(this.getIndex() + 1);
            },

            previous: function () {
                this.edit(this.getIndex() - 1);
            },

            refresh: function () {
                App.GridPanel1.getView().refresh();
            }
        }; //end

        function reloadGridPanel() {
            var winUDW = Ext.getCmp('NewUserWindow');
            winUDW.hide();
            reloadGrid();
        }              

        function reloadGrid(){
             Ext.Ajax.request ({ 
                    url: '/Data/GetAllUsersByClientID/', 
                    method: 'POST', 
                    type:'Load',   
                    params: {
                        ClientId: #{txtLoginUserClientID}.getValue()                                                      
                    },       
                    success: function(response) 
                    {                                     
                         successFilter(response);
                    }, 
                    failure: function(response) 
                    { 
                         alert(response.responseText); 
                    }                                 
           });   
        }

         var successFilter = function (result) {     
         var params= Ext.JSON.decode(result.responseText); 
            #{Store1}.removeAll();        
             #{Store1}.loadRawData(params.result);          
         };
    </script>
    </ext:XScript>
</head>
<body>
    <ext:ResourceManager ID="ResourceManagerAdmin" runat="server" />

    <ext:TextField ID="txtUserId" runat="server" Hidden="true"/>
    <ext:TextField ID="txtLoginUserClientID" runat="server" Hidden="true"/>

            <ext:GridPanel ID="GridPanel1" runat="server" Header="false" Border="false" StripeRows="true" AutoExpandColumn="fullName" TrackMouseOver="true"><%-- removed StoreID="Store1" --%>
                <Store>
                <ext:Store ID="Store1" runat="server">
                    <Model>
                        <ext:Model ID="Model1" runat="server">
                            <Fields>
                                <ext:ModelField Name="ID" />
                                <ext:ModelField Name="ClientId" />
                                <ext:ModelField Name="UserId" />
                                <ext:ModelField Name="FirstName" />
                                <ext:ModelField Name="MiddleName" />
                                <ext:ModelField Name="LastName" />
                                <ext:ModelField Name="Suffix" />
                                <ext:ModelField Name="UserName" />
                                <ext:ModelField Name="LoweredEmail" />
                                <ext:ModelField Name="CreateDate" Type="Date" />
                                <ext:ModelField Name="LastLoginDate" Type="Date" />
                                <ext:ModelField Name="IsActive" Type="Boolean"/>
                            </Fields>
                        </ext:Model>
                    </Model>
                    </ext:Store>
                </Store>
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn runat="server" Width="25"></ext:RowNumbererColumn>
                        <ext:Column ColumnID="Id" Hidden="false" runat="server" DataIndex="ID" />
                        <ext:Column ColumnID="UserId" Hidden="true" runat="server" DataIndex="UserId" />
                        <ext:Column ColumnID="fullName" Header="Full Name" runat="server" DataIndex="LastName">
                            <Renderer Fn="fullName" />
                        </ext:Column>
                        <ext:Column DataIndex="UserName" Header="User Name" runat="server" Width="125" />
                        <ext:Column DataIndex="LoweredEmail" Header="Email" runat="server" Width="190" />
                        <ext:Column DataIndex="CreateDate" Header="Created Date" runat="server" Width="120">
                            <Renderer Fn="Ext.util.Format.dateRenderer('m-d-Y g:i a')" />
                        </ext:Column>
                        <ext:Column DataIndex="LastLoginDate" Header="Last Login Date" runat="server" Width="120">
                            <Renderer Fn="Ext.util.Format.dateRenderer('m-d-Y g:i a')" />
                        </ext:Column>
                        <ext:CheckColumn DataIndex="IsActive" Header="Active" runat="server" Width="97" Sortable="false" Align="Center" Editable="false" />
                      
                    </Columns>
                </ColumnModel>
              
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>                      
                            <ext:Button ID="tbNew" runat="server" Text="New" Icon="UserAdd" >
                                <Listeners>
                                      <Click Handler="#{NewUserWindow}.show()" />
                                </Listeners>
                            </ext:Button>
                            <ext:Button ID="tbEdit" runat="server" Text="Edit" Icon="UserEdit" Disabled="true">
                                <Listeners>
                                        <Click Handler="commandHandler('edit', #{GridPanel1}.getSelectionModel().getLastSelected());"/>
                               </Listeners>
                            </ext:Button>
                            <ext:Button ID="tbResetPwd" runat="server" Text="Reset Password" Icon="Key" Disabled="true">
                                <Listeners>
                                    <Click Handler="commandHandler('resetPwd', #{GridPanel1}.getSelectionModel().getLastSelected());" />
                                </Listeners>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                        <Listeners>
                            <Select Handler="#{tbEdit}.enable();#{tbResetPwd}.enable();" />
                            <Deselect Handler="if (!#{GridPanel1}.hasSelection()) {#{tbEdit}.disable();#{tbResetPwd}.disable();}" />
                        </Listeners>
                </ext:RowSelectionModel>
                </SelectionModel>
                <Listeners>
                    <ItemDblClick Handler="commandHandler('edit', #{GridPanel1}.getSelectionModel().getSelected());" />
                </Listeners>
            </ext:GridPanel>

    <ext:Window 
            ID="Window1" 
            runat="server" 
            Icon="House" 
            Title="With PostBack" 
            Hidden="true"
            X="250"
            Y="100"
            />

    <ext:Window
        ID="UserDetailWindow" 
        runat="server" 
        Icon="UserEdit" 
        Width="600" 
        Height="650" 
        Hidden="true" 
        Modal="true"
        Constrain="true">
        <Loader 
            runat="server"
            Url="/Administration/UserDetail/" 
            Mode="Frame"
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="false" 
            MaskMsg="Loading Edit User Form..." >
            <Params>
                <ext:Parameter Name="UserId" Value="" Mode="Value" />
                <ext:Parameter Name="IsNewUser" Value="" Mode="Value" />
            </Params>
        </Loader>
    </ext:Window>
    
    <ext:Window 
        ID="NewUserWindow" 
        runat="server" 
        Icon="UserAdd" 
        Width="370" 
        Height="230" 
        Hidden="true" 
        Modal="true"
        Constrain="true">
        <Loader
            runat="server"
            Url="/Administration/NewUser/" 
            Mode="Frame" 
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="true" 
            MaskMsg="Loading New User Form...">
        </Loader>
    </ext:Window>

</body>
</html>
