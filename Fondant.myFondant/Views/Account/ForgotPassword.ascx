﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %>



<ext:XScript ID="XScript1" runat="server">
     <script type="text/javascript">
        Ext.Ajax.timeout = 120000;
        Ext.net.DirectEvent.timeout = 120000;
         function forgotPassword() {
         
                   Ext.Ajax.request ({
                                            url: '/Account/ForgotPassword/',
                                            method: 'POST',
                                            params: {
                                                userName: #{Username}.getValue(),
                                               
                                            },
                                            success: function (response) {
                                                var params = Ext.JSON.decode(response.responseText);
                                                                  
                                                if (params.extraParamsResponse.messageSuccess == "Success") {
                                                    Ext.Msg.alert("Submit", "Your password was sent to your registered email address.");
                                                }
                                                else {
                                                    Ext.Msg.alert("Submit", params.extraParamsResponse.messageFailed);
                                                }
                                                 
                                            },
                                            failure: function (response) {

                                                Ext.Msg.alert("Submit", "Operation failed due to an Internal Error!");
                                                  parent.closewinChangePassword();
                                            }

                                        }); 
             
         
        }

        
     </script>
     </ext:XScript>
<br />

                <ext:Label runat="server" ID="lblForgotPassword" Text="Forgot your password?"></ext:Label>
                <div>
                <ext:LinkButton ID="LinkButton1" Style="vertical-align: middle;" runat="server" Text="Click here to retrieve password via email" FieldLabel="Forgot your password" LabelSeparator="?">
                    <Listeners>
                    <Click Handler="forgotPassword()" />
                        <%--<Click Handler="#{DirectMethods}.ForgotPassword(#{Username}.getValue());" />--%>
                    </Listeners>
                </ext:LinkButton>
                </div>