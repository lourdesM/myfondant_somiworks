﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%--<%@ Import Namespace="Ext.Net.Utilities" %>--%>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="ForgotPassword.ascx" TagName="ForgotPassword" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>SomiWorks Client Log In</title>
    <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.ResourceManagerClientLogIn.RegisterIcon(Icon.Information);
            }
        }
    </script>
    <%--<script language="JavaScript" src="https://seal.networksolutions.com/siteseal/javascript/siteseal.js" type="text/javascript"></script>--%>
    <script language="javascript" type="text/javascript">
        Ext.onReady(function () {
            if (top.location.href !== window.location.href) {
                parent.location.href = "https://www.works.somidata.com/";
            };
            Username.focus(false, 1000); //commented out because it is causing an error
        });
    </script>
</head>

<body>
<form id="Form1" runat="server">
    <ext:ResourceManager ID="ResourceManagerClientLogIn" runat="server"/>
   <!-- --> <div class="page">
        <div id="header"></div>
     <!--remove main and items and panel to go  back to previous state -->  
               
       <div id="main">
            <ext:Panel ID="NonAuthPanel110" runat="server" Frame="false" Border="false" Height="460" Width="930" Layout="Column">
                <Items>                                                              
                           <ext:Panel ID="NonAuthPanel120" runat="server" Border="false" Width="263" Height="460" StyleSpec="margin-right:10px;">
                                <Items>
                                            <ext:Panel ID="NonAuthPanel130" runat="server" Frame="true" Width="263" Height="263" Border="true" Title="Client Login" BodyStyle="padding:10px;" StyleSpec="padding: 0 10px 0 0 ;" Icon="Lock">
                                                <Items>
                                                <%-- JVA removed from below: BodyBorder="false" --%>
                                                    <ext:FormPanel ID="FormPanel1" 
                                                        runat="server" 
                                                        FormID="form1"
                                                        Border="false"
                                                        BodyStyle="background:transparent;"
                                                        LabelWidth="90">
                                                        <Items>
                                                            <ext:TextField 
                                                                ID="Username" 
                                                                runat="server" 
                                                                FieldLabel="Username" 
                                                                AnchorHorizontal="100%"
                                                                TabIndex="0"
                                                                />
                                                            <ext:TextField 
                                                                ID="Password" 
                                                                runat="server" 
                                                                InputType="Password" 
                                                                FieldLabel="Password" 
                                                                AnchorHorizontal="100%"
                                                                />
                                                            <ext:Checkbox ID="RememberMe" runat="server" InputValue="true" FieldLabel="Remember Me" Note="On a public computer, please keep the checkbox clear for security." NoteCls="font-size:.9em;" />
                                                        </Items>
                                                        <Buttons>
                                                            <ext:Button ID="btnLogin" runat="server" Text="Login" Icon="Accept">
                                                                <DirectEvents>
                                                                    <Click
                                                                        Url="/Account/ClientLogInNow/" 
                                                                        Timeout="60000"
                                                                        FormID="form1"
                                                                        CleanRequest="true" 
                                                                        Before="Ext.Msg.wait('Verifying...', 'Login');"
                                                                        Failure="Ext.Msg.show({
                                                                           title:   'Login Error',
                                                                           msg:     result.errorMessage,
                                                                           buttons: Ext.Msg.OK,
                                                                           icon:    Ext.MessageBox.ERROR
                                                                        });">
                                                                        <EventMask MinDelay="500" />
                                                                        <ExtraParams>
                                                                         <%-- <ext:Parameter Name="ReturnUrl" Value="document.location" Mode="Raw" /> --%>
                                                                         <%--   <ext:Parameter Name="ReturnUrl" Value="Ext.urlDecode(String(document.location).split('?')[1]).r || '/'" Mode="Raw" /> --%>
                                                                         <%--   <ext:Parameter Name="ReturnUrl" Value="/Home/Index" Mode="Raw" /> --%>
                                                                        </ExtraParams>
                                                                    </Click>
                                                                </DirectEvents>
                                                            </ext:Button>
                                                        </Buttons>                
                                                    </ext:FormPanel>                                                   
                                                </Items>
                                                <Content> <uc1:ForgotPassword ID="ForgotPassword1" runat="server"/> </Content>
                                            </ext:Panel>
                                      
                                            <ext:Panel runat="server" ID="NonAuthPanel201" Border="false" Frame="false" BodyStyle="padding:10px;background-color: white;" StyleSpec="padding: 30px 0 0 0 ;"  Width="279" Height="154">
                                                <Content>
                                                    <div align="center">
						                                <!--
						                                SiteSeal Html Builder Code:
						                                Shows the logo at URL https://seal.networksolutions.com/images/basicsqblue.gif
						                                Logo type is  ("NETSP")
						                                //-->
                                                         <span id="siteseal"><script type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=aRDAT445uN7v5smoJbBNTu2JUroeOI0NLgu67uxWrwsjQfDno2Moc0MF6"></script><br/><a style="font-family: arial; font-size: 9px" href="http://www.starfieldtech.com" target="_blank">Secure Server</a></span
						                                <%--<script language="JavaScript" type="text/javascript">SiteSeal("https://seal.networksolutions.com/images/basicsqblue.gif", "NETSP", "none");</script>--%>
                                                </Content>
                                            </ext:Panel>                                                                
                                </Items>
                                </ext:Panel>
                                
                                <ext:Panel ID="Panel1" runat="server" Border="false" Width="651" Height="460">
                                <Items>
                                    <ext:Panel ID="NonAuthPanel103" runat="server" BodyStyle="background-color: white;padding: 7px;" Frame="true" Border="true" Height="360" Width="651">                     
                                         <Content>
                                                <%--<div style="text-align:center;"><img height="375" alt="" width="500" border="0" src="../../Resources/MyFondant.jpg" style="margin:0px 0px 0px 0px;" /></div>--%>
                                         <div style="text-align:center;"><img height="47" alt="" width="237" border="0" src="../../Resources/somidata-logo.png" style="margin:60px 0px 0px 0px;" /></div>
                                         <p style="font-size: 10pt; position:absolute; color:black; margin-top:60px; margin-left:170px; font-family: Arial, Helvetica, sans-serif;"> Access all of the Somi Works products from one place. </p>
                                        <article style="font: normal 11px tahoma,arial,helvetica,sans-serif;     color: #696969;">
  <div id="one" style="position:absolute; width:120px; height:150px; margin-left: 140px;  margin-top: 160px;">
 
  <p>The leading break & meal compliance app. Take charge of your labor compliance now.</p>
</div> 
<p style="font-size: 13pt; position:absolute; color:blue; margin-top:130px; margin-left:140px; font-family: Arial, Helvetica, sans-serif;"> <b>Compliance Works</b> </p>
<p style="font-size: 13pt; position:absolute; color:blue; margin-top:130px; margin-left:350px; font-family: Arial, Helvetica, sans-serif;"> <b>Reporting Works</b> </p>
 <div id="two" style="position:absolute;width:140px; height:150px; margin-left: 350px; margin-top: 160px;  ">
 
  <p>The instant operational reporting and analysis app. Effortlessly integrate your operational data.</p>
</div> 
</article>
                                         </Content>
                                    </ext:Panel>

                                    <ext:Panel ID="NonAuthPanel104" runat="server" BodyStyle="background-color: white;padding:9px; padding-top:-5px;font: normal 11px tahoma,arial,helvetica,sans-serif; color: #696969;" Frame="true" Width="651" Height="95">
                                                        <Content>
                                                            <h7><b>Important Reminder:</b> </h7>
                                                            <div> Fondant Systems technical support staff will <b>never</b> ask for your login information.</div>
                                                            <br>
                                                            <h7><b>New Users:</b></h7>
                                                            <div>Please&#160;check your user name and temporary password from your email account or contact your organization's&#160;Fondant Systems&#160;administrator for further instructions. Unauthorized access is prohibited.</div>    						            
                                                        </Content>
                                     </ext:Panel>
                                </Items>
                                </ext:Panel>
                            

                 </Items>
            </ext:Panel>
            
           
        </div>
       

                   
    </div>
 
    <ext:KeyMap ID="KeyMap1" runat="server" Target="={Ext.isGecko ? Ext.getDoc() : Ext.getBody()}">
    <Binding>
    <ext:KeyBinding Handler="#{btnLogin}.fireEvent('click');">
            <Keys>
                <ext:Key Code="ENTER" />
                <ext:Key Code="RETURN" />
            </Keys>
            <%--<Listeners>
                <Event Handler="#{btnLogin}.fireEvent('click');" />
            </Listeners>--%>
        </ext:KeyBinding>
        </Binding>
    </ext:KeyMap>
</form>
</body>
</html>