﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if(ViewData["UserId"] != null)
            {
                X.ResourceManager.RegisterOnReadyScript(string.Format("loadUser('{0}');", ViewData["UserId"]));
                this.IsNewUser.Text = ViewData["IsNewUser"].ToString();
                //this.ClientName.Text = Profile.ClientLongName;
                this.ClientName.Text = this.ViewData["LongName"].ToString();
            }
        }
    </script>
    <script type="text/javascript">
        function loadUser(UserId) {
            dsUser.load({ params: { ASPNETUserId: UserId} });
            dsUserRoles.load({ params: { ASPNETUserId: UserId} });
            dsUserLocations.load({ params: { ASPNETUserId: UserId} });
        }

        var userLoaded = function (store, records) {
            if (records.length > 0) {
                UserDetailForm.form.loadRecord(records[0]);
                /*
                dsOrders.loaded = false;
                if (UserPanel.getActiveTab().id == tabOrders.id) {
                dsOrders.reload();
                }
                */
            }
            else {
                UserDetailForm.form.reset();
                //dsOrders.removeAll();
            }
            /*
            initUI(false);
            */
            UserDetailForm.el.unmask();
        }

        var failureHandler = function (form, action) {
            var msg = '';
            if (action.failureType == "client" || (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Please check fields";
            } else if (action.result && action.result.extraParams.msg) {
                msg = action.result.extraParams.msg;
            } else if (action.response) {
                msg = action.response.responseText;
            }

            Ext.Msg.show({
                title: 'Save Error',
                msg: msg,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
        }

        var successHandler = function (form, action) {

            if (action.result && action.result.extraParams && action.result.extraParams.newID) {
                dsUser.getAt(0).id = action.result.extraParams.newID;
                if (dsUser.getAt(0).newRecord) {
                    delete dsUser.getAt(0).newRecord;
                }
            }
            else {
                dsUserRoles.save();
                dsUserLocations.save();
                Ext.Msg.show({
                    title: 'Save',
                    msg: 'Save successful.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    fn: saveClose
                });
            }

            customerChanged = true;

            if (action.options.params.setNew) {
                UserDetailForm.form.reset();
                dsUserRoles.removeAll();
                dsUserLocations.removeAll();

                var rec = new dsUser.recordType();
                rec.newRecord = true;
                dsUser.add(rec);
                //initUI(true);
            }
            else {
                //initUI(false);
            }
        }

        function saveClose() {
            CancelButton.fireEvent('click');
            parent.Store1.reload();
        }

        var getUserID = function () {
            return (dsUser.getCount() > 0 && !dsUser.getAt(0).newRecord) ? dsUser.getAt(0).id : ''
        }
    </script>
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

    <ext:Store ID="dsUser" runat="server" AutoLoad="false">
        <Proxy>
            <ext:AjaxProxy Url="/Data/GetUser/" />
        </Proxy>
        <Model>
            <ext:Model ID="Model1" runat="Server" IDProperty="ASPNETUserID" Root="data">
                <Fields>
                    <ext:ModelField Name="ClientID" />
                    <ext:ModelField Name="ASPNETUserID" />
                    <ext:ModelField Name="FirstName" />
                    <ext:ModelField Name="MiddleName" />
                    <ext:ModelField Name="LastName" />
                    <ext:ModelField Name="Suffix" />
                    <ext:ModelField Name="Email" />
                    <ext:ModelField Name="CreatedDate" Type="Date" />
                    <ext:ModelField Name="ModifiedDate" Type="Date" />
                    <ext:ModelField Name="IsActive" Type="Boolean" />
                </Fields>
            </ext:Model>
        </Model>
        <Listeners>
            <BeforeLoad Handler="#{UserDetailForm}.el.mask('Loading user...', 'x-mask-loading');" />
            <Exception Handler="#{UserDetailForm}.el.unmask();" />
            <Load Fn="userLoaded" />
        </Listeners>
    </ext:Store>

    <ext:Store ID="dsUserLocations" runat="server" ShowWarningOnFailure="true" AutoLoad="false">
        <%-- <Proxy>
            <ext:AjaxProxy Url="/Data/UserLocationsGet/" />
        </Proxy>
        <Writer>
            <ext:JsonWriter Encode="true" Root="data"/>
        </Writer>
        <%--<UpdateProxy>
            <ext:HttpWriteProxy Url="/Data/UserLocationsSet/" />
        </UpdateProxy>--%>
       <%-- <Reader>
            <ext:JsonReader Root="data">
                <Fields>
                    <ext:RecordField Name="UserId" />
                    <ext:RecordField Name="LocationId" />
                    <ext:RecordField Name="LocationName" />
                    <ext:RecordField Name="Selected" />
                </Fields>
            </ext:JsonReader>
        </Reader> --%>

        <Proxy>
             <ext:AjaxProxy Url="/Data/UserLocationsGet/">
                 <Reader>
                    <ext:JsonReader Root="Data">                          
                     </ext:JsonReader>
                 </Reader>                 
             </ext:AjaxProxy>
         </Proxy>
        <Proxy>
            <ext:AjaxProxy Url="/Data/UserLocationsSet/">
                <Writer>
                   <ext:JsonWriter Root="Data">
                    </ext:JsonWriter>
                 </Writer>
            </ext:AjaxProxy>
        </Proxy>            
          <Model>
             <ext:Model ID="Model2" runat="server">
                 <Fields>
                     <ext:ModelField Name="UserId" />
                     <ext:ModelField Name="LocationId" />
                     <ext:ModelField Name="LocationName" />
                     <ext:ModelField Name="Selected" />
                 </Fields>
             </ext:Model>
         </Model>
    </ext:Store>

    <ext:Store ID="dsUserRoles" runat="server" ShowWarningOnFailure="true" AutoLoad="false">
        <%-- <Proxy>
            <ext:HttpProxy Url="/Data/UserRolesGet/" />
        </Proxy>
        <UpdateProxy>
            <ext:HttpWriteProxy Url="/Data/UserRolesSet/" />
        </UpdateProxy>
        <Reader>
            <ext:JsonReader Root="data">
                <Fields>
                    <ext:RecordField Name="UserId" />
                    <ext:RecordField Name="RoleName" />
                    <ext:RecordField Name="Selected" />
                </Fields>
            </ext:JsonReader>
        </Reader>  --%>

        <Proxy>
            <ext:AjaxProxy Url="/Data/UserRolesGet/">
                <Reader>
                    <ext:JsonReader Root="Data">                          
                    </ext:JsonReader>
                </Reader>  
            </ext:AjaxProxy>
        </Proxy>
         <Proxy>
            <ext:AjaxProxy Url="/Data/UserRoleSet/">
                <Writer>
                   <ext:JsonWriter Root="Data">
                    </ext:JsonWriter>
                 </Writer>
            </ext:AjaxProxy>
        </Proxy>
        <Model>
             <ext:Model ID="Model3" runat="server">
                 <Fields>
                     <ext:ModelField Name="UserId" />
                     <ext:ModelField Name="RoleName" />
                     <ext:ModelField Name="Selected" />                     
                 </Fields>
             </ext:Model>
         </Model>   
    </ext:Store>

    <ext:ViewPort ID="ViewPort1" runat="server" Layout="Fit">
        <Items>
            <ext:FormPanel ID="UserDetailForm" runat="server" BodyStyle="background-color:#FFFFFF;padding:7px;" ButtonAlign="Right"
                 TitleCollapse="true" Url="/Data/SaveUser/" >
                <Items>                  
                       
                           
                                <ext:Panel ID="UserDetailPanel" runat="server" BodyStyle="padding: 7px;" Border="false" Layout="Form" ColumnWidth="0.5">
                                    <Items>
                                        <ext:TextField ID="IsNewUser" runat="server" Hidden="true" />
                                        <ext:TextField ID="ClientName" runat="server" Hidden="true" />
                                        <ext:TextField ID="ClientID" DataIndex="ClientID" runat="server" Hidden="true" />
                                        <ext:TextField ID="ASPNETUserID" DataIndex="ASPNETUserID" runat="server" Hidden="true" />
                                        <ext:TextField ID="FirstName" DataIndex="FirstName" runat="server" FieldLabel="First Name" MaxLength="50" AnchorHorizontal="100%" />
                                        <ext:TextField ID="MiddleName" DataIndex="MiddleName" runat="server" FieldLabel="Middle Name" MaxLength="50" AnchorHorizontal="100%" />
                                        <ext:TextField ID="LastName" DataIndex="LastName" runat="server" FieldLabel="Last Name" MaxLength="50" AnchorHorizontal="100%" />
                                        <ext:TextField ID="Suffix" DataIndex="Suffix" runat="server" FieldLabel="Suffix" MaxLength="50" AnchorHorizontal="100%" />
                                        <ext:TextField ID="Email" DataIndex="Email" runat="server" FieldLabel="Email" MaxLength="256" AnchorHorizontal="100%" />
                                        <ext:Checkbox ID="IsActive" DataIndex="IsActive" runat="server" FieldLabel="Active" AnchorHorizontal="100%" />
                                        <ext:GridPanel ID="UserRolesGridPanel" runat="server" StoreID="dsUserRoles" StripeRows="true" Height="168" Title="User Roles" AutoExpandColumn="RoleName" >
                                            <ColumnModel ID="ColumnModel1" runat="server">
                                                <Columns>
                                                    <ext:Column ID="Column1" runat="server" ColumnID="RoleName" DataIndex="RoleName" Header="Role" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                    <ext:CheckColumn ID="CheckColumn1" runat="server" DataIndex="Selected" Header="Select" Align="Center" Width="60" Editable="true" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                </Columns>
                                            </ColumnModel>
                                            <SelectionModel>
                                                <ext:RowSelectionModel />
                                            </SelectionModel>
                                        </ext:GridPanel>
                                    </Items>
                                </ext:Panel>
                           
                                <ext:Panel ID="UsersLocationsPanel" runat="server" BodyStyle="padding: 7px;" Border="false" Layout="Form">
                                    <Items>
                                        <ext:GridPanel ID="UsersLocationsGridPanel" runat="server" StoreID="dsUserLocations" StripeRows="true" Title="User Locations" Height="323" AutoExpandColumn="LocationName" >
                                            <ColumnModel ID="UserLocationsColumnModel" runat="server">
                                                <Columns>                                                   
                                                    <ext:Column ID="Column2" runat="server" ColumnID="LocationName" DataIndex="LocationName" Header="Location" Width="150" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                    <ext:CheckColumn ID="CheckColumn2" runat="server" DataIndex="Selected" Header="Select" Align="Center" Width="60" Editable="true" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                </Columns>
                                            </ColumnModel>
                                            <SelectionModel>
                                                <ext:RowSelectionModel />
                                            </SelectionModel>
                                        </ext:GridPanel>
                                    </Items>
                                </ext:Panel>
                            
                       
                   
                </Items>
                <Buttons>
                    <ext:Button ID="SaveButton" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                            <Click Handler="#{UserDetailForm}.form.submit({waitMsg:'Saving...', params:{id: getUserID()}, success: successHandler, failure: failureHandler});" />
                        </Listeners>
                    </ext:Button>
                    <ext:Button ID="CancelButton" runat="server" Icon="Cancel" Text="Cancel">
                        <Listeners>
                            <Click Handler="parent.window.UserDetailWindow.hide();" />
                        </Listeners>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:ViewPort></body>
</html>
