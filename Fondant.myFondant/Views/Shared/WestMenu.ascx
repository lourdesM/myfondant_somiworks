﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<%@Import Namespace="Fondant.myFondant.Web.Code" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="Fondant.myFondant.Web.Controllers" %>
 <script type="text/javascript" src="/Scripts/myFondant.js"></script>


<script type="text/javascript">
    var loadPage = function (tabPanel, node) {
        var tab = tabPanel.getItem(node.id);

        if (!tab) {
            tab = tabPanel.add({
                id: node.id,
                title: node.text,
                closable: true,
                autoLoad: {
                    showMask: true,
                    url: node.attributes.href,
                    mode: "iframe",
                    maskMsg: "Loading " + node.attributes.href + "..."
                },
                listeners: {
                    update: {
                        fn: function (tab, cfg) {
                            cfg.iframe.setHeight(cfg.iframe.getSize().height - 20);
                        },
                        scope: this,
                        single: true
                    }
                }
            });
        }

        tabPanel.setActiveTab(tab);
    }
    </script>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)    
    {
       
      
        if (this.ViewData["AccountNumber"].ToString().Contains("90100") || this.ViewData["AccountNumber"].ToString().Contains("50200"))
        {
            acdnWest.Items.Add(CreateProductsMenu("RC0"));
        }

        acdnWest.Items.Add(CreateProductsMenu("R35"));

        
        if (this.ViewData["AccountNumber"].ToString().Contains("90100"))
        {
            acdnWest.Items.Add(CreateProductsMenu("RS3"));
        }

        // SBE Corp. VIP Forms Menu
        if (this.ViewData["AccountNumber"].ToString().Contains("50200"))
        {
            acdnWest.Items.Add(CreateSBECorpVIPFormsMenu());
        }

        // DryBar GL Upload Sytem
        if (this.ViewData["AccountNumber"].ToString().Contains("70100") && (ViewData["AccountingRoleForDryBar"].ToString().Contains("Accounting") || ViewData["AccountingAdminRoleForDryBar"].ToString().Contains("Accounting Admin")))
        {
            acdnWest.Items.Add(CreateGLUploadSystemMenu());
        }
                
        // MyFondant Menu       
        if (this.ViewData["UserRole"].ToString().Contains("Corporate IT") || this.ViewData["UserRole"].ToString().Contains("Corporate Administrator"))
        {
            acdnWest.Items.Add(CreateMyFondantMenu());
        }
    }

    private Ext.Net.MenuPanel CreateGLUploadSystemMenu()
    {
        // GL Upload System Folder
        Ext.Net.MenuPanel mupnlAdmin = new Ext.Net.MenuPanel();
        mupnlAdmin.Title = "GL Upload System";
        mupnlAdmin.Icon = Icon.FolderWrench;
        mupnlAdmin.Border = false;
        mupnlAdmin.SaveSelection = false;
        mupnlAdmin.Cls = "background-menu";
        mupnlAdmin.Menu.Listeners.Click.Handler = "addTab(#{tpMain}, menuItem.text, menuItem.url);";
        
        // GL Upload Page
        Ext.Net.MenuItem muiUM = new Ext.Net.MenuItem();
        muiUM.Text = "Retail GL Upload Summary";
        muiUM.Icon = Icon.ApplicationGet;
        Ext.Net.ConfigItem ciurlUMUser = new ConfigItem();
        ciurlUMUser.Name = "url";
        ciurlUMUser.Value = "/GLUploadSystem/GLUploadPage?sessionId=" + Request.Cookies["SessionId"].Value;
        ciurlUMUser.Mode = ParameterMode.Value;
        muiUM.CustomConfig.Add(ciurlUMUser);

        mupnlAdmin.Menu.Items.Add(muiUM);

        // GL Detail Report
        Ext.Net.MenuItem muiUM1 = new Ext.Net.MenuItem();
        muiUM1.Text = "Retail GL Detail Report";
        muiUM1.Icon = Icon.FolderPageWhite;
        Ext.Net.ConfigItem ciurlUMUser1 = new ConfigItem();
        ciurlUMUser1.Name = "url";
        ciurlUMUser1.Value = "/GLUploadSystem/GLDetailReport?sessionId=" + Request.Cookies["SessionId"].Value;
        ciurlUMUser1.Mode = ParameterMode.Value;
        muiUM1.CustomConfig.Add(ciurlUMUser1);
     
        mupnlAdmin.Menu.Items.Add(muiUM1);

        // Franchise GL Detail Report
        Ext.Net.MenuItem muiUM2 = new Ext.Net.MenuItem();
        muiUM2.Text = "Franchise Sales Report";
        muiUM2.Icon = Icon.FolderPageWhite;
        Ext.Net.ConfigItem ciurlUMUser2 = new ConfigItem();
        ciurlUMUser2.Name = "url";
        ciurlUMUser2.Value = "/GLUploadSystem/FranchiseSalesReport?sessionId=" + Request.Cookies["SessionId"].Value;
        ciurlUMUser2.Mode = ParameterMode.Value;
        muiUM2.CustomConfig.Add(ciurlUMUser2);

        mupnlAdmin.Menu.Items.Add(muiUM2);


                        
        return mupnlAdmin;
        
    }
    
    private TreePanel CreateProductsMenu(string Cls)
    {
        TreePanel tree = new TreePanel();
        tree.Cls = Cls;
       
        tree.RootVisible = false;
        tree.Lines = false;
        tree.Border = false;
        tree.AutoScroll = true;
        tree.EnableColumnResize = true;
       
          tree.Listeners.ItemClick.Handler = "addTab(#{tpMain}, record.data.text, record.data.qtitle);";
          
        switch (Cls)
        {
            case "RC0":
                GetRC0(tree.Root);
                break;
            case "R35":
                GetR35(tree.Root);
                break;
            case "RS3":
                GetRS3(tree.Root);
                break;
            default:
                break;
        }
        
        return tree;
    }
    private class UserInSSRSReports
    {
        public int? UserId { get; set; }
        public int SSRS_ReportsId { get; set; }
        public string ReportName { get; set; }
        public int Selected { get; set; }
    }
    private Ext.Net.NodeCollection GetR35(Ext.Net.NodeCollection treeReportsCollection)
    {
        if (treeReportsCollection == null)
        {
            treeReportsCollection = new Ext.Net.NodeCollection(); 
        }

        Ext.Net.Node root = new Ext.Net.Node();
        root.NodeID = "root";
        root.Text = "Reports";

        String pstrReportPath = "/" + this.ViewData["AccountNumber"];
        List<UserInSSRSReports> SSRSReports = new List<UserInSSRSReports>();
        DataController datacontroller = new DataController();
        
        Fondant.myFondant.Web.ReportingServices.CatalogItem[] cis = null;
        try
        {
            Fondant.myFondant.Web.ReportingServices.ReportingService2005 rs = new Fondant.myFondant.Web.ReportingServices.ReportingService2005();
            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
            
            cis = rs.ListChildren(pstrReportPath, false);

            if (this.ViewData["UserRoleForSSRSReport"].ToString().Contains("Run only specified SSRS Reports"))
            {
                var SSRSReports1 = datacontroller.UserInSSRSReportsGetSelected(this.ViewData["ASPNetUserId"].ToString());
                if (SSRSReports1.Count > 0)
                {
                    foreach (var i in SSRSReports1)
                    {
                        UserInSSRSReports value = new UserInSSRSReports();
                        value.UserId = i.UserId;
                        value.SSRS_ReportsId = i.SSRS_ReportsId;
                        value.ReportName = i.ReportName;
                        value.Selected = i.Selected;
                        SSRSReports.Add(value);
                    }
                }

                if (SSRSReports.Count > 0)
                {
                    foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem ci in cis)
                    {
                        if (ci.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Folder && (!ci.Hidden))
                        {
                            if (SSRSReports.Any(p => p.ReportName == ci.Name))
                            {
                                Ext.Net.Node treeFolder = new Ext.Net.Node();
                                treeFolder.Icon = Icon.FolderPageWhite;
                                treeFolder.Text = ci.Name;

                                Fondant.myFondant.Web.ReportingServices.CatalogItem[] cisreports = rs.ListChildren(ci.Path, false);
                                foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem cireports in cisreports)
                                {
                                    if (cireports.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Report && (!cireports.Hidden))
                                    {
                                        Ext.Net.Node treeReport = new Ext.Net.Node(); //.Net.TreeNode(); 
                                        treeReport.Icon = Icon.Report;
                                        treeReport.Text = cireports.Name;
                                        treeReport.Qtitle = "/WebForms/ReportViewer.aspx?rsurl=" + ToAbsoluteUrl(cireports.Path);//"http://fondant02:8081/R35ReportServer/Pages/ReportViewer.aspx?" + ToAbsoluteUrl(cireports.Path) + "&rs:Command=Render"; //added

                                        Ext.Net.ConfigItem cirsurl = new ConfigItem();
                                        cirsurl.Name = "rsurl";
                                        cirsurl.Value = "/WebForms/ReportViewer.aspx?rsurl=" + ToAbsoluteUrl(cireports.Path);//cireports.Path;//"http://fondant02:8081/R35ReportServer/Pages/ReportViewer.aspx?" + ToAbsoluteUrl(cireports.Path) + "&rs:Command=Render";//** cireports.Path;

                                        cirsurl.Mode = ParameterMode.Value;
                                        treeReport.CustomAttributes.Add(cirsurl);

                                        Ext.Net.ConfigItem ciurl = new ConfigItem();
                                        ciurl.Name = "url";
                                        ciurl.Value = "/WebForms/ReportViewer.aspx";
                                        ciurl.Mode = ParameterMode.Value;
                                        treeReport.CustomAttributes.Add(ciurl);

                                        treeFolder.Children.Add(treeReport);
                                    }
                                }

                                if (cisreports.Count() >= 1)
                                {

                                    root.Children.Add(treeFolder);
                                }
                            }
                        }

                    }
                }
                else
                {
                    Ext.Net.Node treeCatch = new Ext.Net.Node();
                    treeCatch.Icon = Icon.Information;
                    treeCatch.Text = "No reports found.";
                    root.Children.Add(treeCatch);
                }
            }
            else
            {
                foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem ci in cis)
                {
                    if (ci.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Folder && (!ci.Hidden))
                    {
                        Ext.Net.Node treeFolder = new Ext.Net.Node();
                        treeFolder.Icon = Icon.FolderPageWhite;
                        treeFolder.Text = ci.Name;

                        Fondant.myFondant.Web.ReportingServices.CatalogItem[] cisreports = rs.ListChildren(ci.Path, false);
                        foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem cireports in cisreports)
                        {
                            if (cireports.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Report && (!cireports.Hidden))
                            {
                                Ext.Net.Node treeReport = new Ext.Net.Node();
                                treeReport.Icon = Icon.Report;
                                treeReport.Text = cireports.Name;
                                treeReport.Qtitle = "WebForms/ReportViewer.aspx?rsurl=" + ToAbsoluteUrl(cireports.Path);//"http://fondant02:8081/R35ReportServer/Pages/ReportViewer.aspx?" + ToAbsoluteUrl(cireports.Path) + "&rs:Command=Render"; //added

                                Ext.Net.ConfigItem cirsurl = new ConfigItem();
                                cirsurl.Name = "rsurl";
                                cirsurl.Value = "WebForms/ReportViewer.aspx?rsurl=" + ToAbsoluteUrl(cireports.Path);//cireports.Path;//"http://fondant02:8081/R35ReportServer/Pages/ReportViewer.aspx?" + ToAbsoluteUrl(cireports.Path) + "&rs:Command=Render";//** cireports.Path;

                                cirsurl.Mode = ParameterMode.Value;
                                treeReport.CustomAttributes.Add(cirsurl);

                                Ext.Net.ConfigItem ciurl = new ConfigItem();
                                ciurl.Name = "url";
                                ciurl.Value = "/WebForms/ReportViewer.aspx";
                                ciurl.Mode = ParameterMode.Value;
                                treeReport.CustomAttributes.Add(ciurl);

                                treeFolder.Children.Add(treeReport);
                            }
                        }

                        if (cisreports.Count() >= 1)
                        {

                            root.Children.Add(treeFolder);
                        }
                    }
                }
            }
        }
        catch
        {
            Ext.Net.Node treeCatch = new Ext.Net.Node();
            treeCatch.Icon = Icon.Information;
            treeCatch.Text = "No reports found.";
            root.Children.Add(treeCatch);
        } 
       
        treeReportsCollection.Add(root);
        
       
        return treeReportsCollection;
    }

    private Ext.Net.Node GetR35Analysis(Ext.Net.Node treeNode)
    {
        Ext.Net.ConfigItem ciurlUM = new ConfigItem();
        ciurlUM.Name = "url";
        ciurlUM.Value = "/Advanced/AdhocGrid";
        ciurlUM.Mode = ParameterMode.Value;
        
        // Advanced Folder
        Ext.Net.Node tnAdvanced = new Ext.Net.Node();
        tnAdvanced.Text = "Advanced Analysis";
        tnAdvanced.Icon = Icon.FolderLightbulb;

       
        if(this.ViewData["AccountNumber"].ToString().Contains("50300"))
        {
            // AdhocGrid Sales Mix
            Ext.Net.Node tnAdhoc50300 = new Ext.Net.Node();
            tnAdhoc50300.Text = "Sales Mix Analysis";
            tnAdhoc50300.Icon = Icon.DatabaseTable;

            Ext.Net.ConfigItem cicubeUM50300 = new ConfigItem();
            cicubeUM50300.Name = "rsurl";
            cicubeUM50300.Value = "CUST Sales Mix";
            cicubeUM50300.Mode = ParameterMode.Value;
            tnAdhoc50300.CustomAttributes.Add(cicubeUM50300);
            tnAdhoc50300.CustomAttributes.Add(ciurlUM);

            tnAdvanced.Children.Add(tnAdhoc50300);
        }

       
        if(this.ViewData["AccountNumber"].ToString().Contains("50000"))
        {
           
            // AdhocGrid Employee
            Ext.Net.Node tnAdhoc = new Ext.Net.Node();
            tnAdhoc.Text = "Employee Sales Analysis";
            tnAdhoc.Icon = Icon.DatabaseTable;

            Ext.Net.ConfigItem cicubeUM = new ConfigItem();
            cicubeUM.Name = "rsurl";
            cicubeUM.Value = "CUST Employee Sales";
            cicubeUM.Mode = ParameterMode.Value;
            tnAdhoc.CustomAttributes.Add(cicubeUM);
            tnAdhoc.CustomAttributes.Add(ciurlUM);

            tnAdvanced.Children.Add(tnAdhoc);

            // AdhocGrid Product Mix
            Ext.Net.Node tnAdhocPM = new Ext.Net.Node();
            tnAdhocPM.Text = "Product Mix Analysis";
            tnAdhocPM.Icon = Icon.DatabaseTable;

            Ext.Net.ConfigItem cicubePMUM = new ConfigItem();
            cicubePMUM.Name = "rsurl";
            cicubePMUM.Value = "CUST Product Mix";
            cicubePMUM.Mode = ParameterMode.Value;
            tnAdhocPM.CustomAttributes.Add(cicubePMUM);
            tnAdhocPM.CustomAttributes.Add(ciurlUM);

            tnAdvanced.Children.Add(tnAdhocPM);

            // AdhocGrid Labor
            Ext.Net.Node tnAdhocL = new Ext.Net.Node();
            tnAdhocL.Text = "Labor Analysis";
            tnAdhocL.Icon = Icon.DatabaseTable;

            Ext.Net.ConfigItem cicubeLUM = new ConfigItem();
            cicubeLUM.Name = "rsurl";
            cicubeLUM.Value = "CUST Labor";
            cicubeLUM.Mode = ParameterMode.Value;
            tnAdhocL.CustomAttributes.Add(cicubeLUM);
            tnAdhocL.CustomAttributes.Add(ciurlUM);

            tnAdvanced.Children.Add(tnAdhocL);
        }

        treeNode.Children.Add(tnAdvanced);
        
        return treeNode;
    }
    
    private Ext.Net.NodeCollection GetRS3(Ext.Net.NodeCollection treeCollection)
    {
        if (treeCollection == null)
        {
            treeCollection = new Ext.Net.NodeCollection();
        }

        Ext.Net.Node root = new Ext.Net.Node();
        root.NodeID = "root";
        
       
        // Item Node: Schedules Employees
        Ext.Net.Node treeSchedEmployees = new Ext.Net.Node();
        treeSchedEmployees.Text = "Manage Employee Info";
        treeSchedEmployees.Icon = Icon.Group;
        treeSchedEmployees.Qtitle = "https://demo.myfondant.com/employees.aspx/"; //added

        Ext.Net.ConfigItem ciSchedEmployees = new ConfigItem();
        ciSchedEmployees.Name = "url";
        ciSchedEmployees.Value = "https://demo.myfondant.com/employees.aspx/"; //added
        ciSchedEmployees.Mode = ParameterMode.Value;
        treeSchedEmployees.CustomAttributes.Add(ciSchedEmployees);

        // Item Node: Schedule
        Ext.Net.Node treeSched = new Ext.Net.Node();
        treeSched.Text = "Schedule Employees";
        treeSched.Icon = Icon.Clock;
        treeSched.Qtitle = "https://demo.myfondant.com/mainschedule.aspx/";
        
        Ext.Net.ConfigItem ciSched = new ConfigItem();
        ciSched.Name = "url";
        ciSched.Value = "https://demo.myfondant.com/mainschedule.aspx/";
        ciSched.Mode = ParameterMode.Value;
        treeSched.CustomAttributes.Add(ciSched);

        // Folder Node: Tasks
        Ext.Net.Node tnTasks = new Ext.Net.Node();
        tnTasks.Text = "Tasks";
        tnTasks.Icon = Icon.FolderEdit;
        tnTasks.Children.Add(treeSchedEmployees);
        tnTasks.Children.Add(treeSched);
            
        // Folder: Schedules
        root.Children.Add(tnTasks);
        treeCollection.Add(root);
        return treeCollection;
    }

    private Ext.Net.NodeCollection GetRC0(Ext.Net.NodeCollection treeCollection) //Ready Comply
    {
        if (treeCollection == null)
        {
            treeCollection = new Ext.Net.NodeCollection();
        }

        // Create the root
        Ext.Net.Node root = new Ext.Net.Node();
        root.NodeID = "root";
        
        // Folder Node: Tasks
        Ext.Net.Node tnTasks = new Ext.Net.Node();
        tnTasks.Text = "Tasks";
        tnTasks.Icon = Icon.FolderEdit;

       //SBE
        if (this.ViewData["AccountNumber"].ToString().Contains("50200")) 
        {
            // Break Compliance Site
            Ext.Net.Node tnBreak50200 = new Ext.Net.Node();
            tnBreak50200.Text = "Manage Break Compliance Waivers";
            tnBreak50200.Qtitle = "https://custom.myfondant.com/50200/breakcompliance/"; 
            tnBreak50200.Icon = Icon.ClockRed;

            Ext.Net.ConfigItem ciSite50200 = new ConfigItem();
            ciSite50200.Name = "url";
            ciSite50200.Value = "https://custom.myfondant.com/50200/breakcompliance/";
            ciSite50200.Mode = ParameterMode.Value;
            tnBreak50200.CustomAttributes.Add(ciSite50200);

            tnTasks.Children.Add(tnBreak50200);
        }
        else
        {

            // Item Node: Break Compliance Waiver
            Ext.Net.Node tnBreakWaiver = new Ext.Net.Node();
            tnBreakWaiver.Text = "Manage Break Compliance Waivers";
            tnBreakWaiver.Icon = Icon.ClockRed;
            tnBreakWaiver.Qtitle = "https://demo.myfondant.com/employeewaivers.aspx/"; 

            Ext.Net.ConfigItem ciBreakWaiverSite = new ConfigItem();
            ciBreakWaiverSite.Name = "url";
            ciBreakWaiverSite.Value = "https://demo.myfondant.com/employeewaivers.aspx/";
            ciBreakWaiverSite.Mode = ParameterMode.Value;
            tnBreakWaiver.CustomAttributes.Add(ciBreakWaiverSite);

            tnTasks.Children.Add(tnBreakWaiver);

            // Item Node: Break Rules
            Ext.Net.Node tnBreakRules = new Ext.Net.Node();
            tnBreakRules.Text = "Manage Location Break Rules";
            tnBreakRules.Icon = Icon.ClockEdit;
            tnBreakRules.Qtitle = "https://demo.myfondant.com/locationbreakrules.aspx/"; 
            
            Ext.Net.ConfigItem ciBreakRulesSite = new ConfigItem();
            ciBreakRulesSite.Name = "url";
            ciBreakRulesSite.Value = "https://demo.myfondant.com/locationbreakrules.aspx/";
            ciBreakRulesSite.Mode = ParameterMode.Value;
            tnBreakRules.CustomAttributes.Add(ciBreakRulesSite);

            tnTasks.Children.Add(tnBreakRules);

            // Item Node: Alert Maintenance
            Ext.Net.Node tnAlert = new Ext.Net.Node();
            tnAlert.Text = "Manage Employee Alerts";
            tnAlert.Icon = Icon.ClockError;
            tnAlert.Qtitle = "https://demo.myfondant.com/employeealertmaintenance.aspx/"; 

            Ext.Net.ConfigItem ciAlertSite = new ConfigItem();
            ciAlertSite.Name = "url";
            ciAlertSite.Value = "https://demo.myfondant.com/employeealertmaintenance.aspx/";
            ciAlertSite.Mode = ParameterMode.Value;
            tnAlert.CustomAttributes.Add(ciAlertSite);

            tnTasks.Children.Add(tnAlert);
        }

        // Item Node: California Meal Period Helpful Links
        Ext.Net.Node tnDoc1 = new Ext.Net.Node();
        tnDoc1.Text = "California Meal Period Helpful Links";
        tnDoc1.Qtitle = "/Resources/READYC0MPLYdocs/California_Meal_Period_Helpful_Links.pdf"; 
        tnDoc1.Icon = Icon.PageWhiteAcrobat;

        Ext.Net.ConfigItem ciDoc1Site = new ConfigItem();
        ciDoc1Site.Name = "url";
        ciDoc1Site.Value = "/Resources/READYC0MPLYdocs/California_Meal_Period_Helpful_Links.pdf";
        ciDoc1Site.Mode = ParameterMode.Value;
        tnDoc1.CustomAttributes.Add(ciDoc1Site);
        
        // Item Node: California Meal Period Information
        Ext.Net.Node tnDoc2 = new Ext.Net.Node();
        tnDoc2.Text = "California Meal Period Information";
        tnDoc2.Qtitle = "/Resources/READYC0MPLYdocs/California_Meal_Period_Information.pdf"; 
        tnDoc2.Icon = Icon.PageWhiteAcrobat;

        Ext.Net.ConfigItem ciDoc2Site = new ConfigItem();
        ciDoc2Site.Name = "url";
        ciDoc2Site.Value = "/Resources/READYC0MPLYdocs/California_Meal_Period_Information.pdf";
        //ciDoc2Site.Value = "/Resources/50200.jpg";
        ciDoc2Site.Mode = ParameterMode.Value;
        tnDoc2.CustomAttributes.Add(ciDoc2Site);

        // Item Node: Sample Break Compliance Form
        Ext.Net.Node tnDoc3 = new Ext.Net.Node();
        tnDoc3.Text = "Sample Break Compliance Form";
        tnDoc3.Qtitle = "/Resources/READYC0MPLYdocs/Sample_Break_Compliance_Form.pdf"; 
        tnDoc3.Icon = Icon.PageWhiteAcrobat;

        Ext.Net.ConfigItem ciDoc3Site = new ConfigItem();
        ciDoc3Site.Name = "url";
        ciDoc3Site.Value = "/Resources/READYC0MPLYdocs/Sample_Break_Compliance_Form.pdf";
        ciDoc3Site.Mode = ParameterMode.Value;
        tnDoc3.CustomAttributes.Add(ciDoc3Site);

        // Item Node: Sample Break Compliance Form 10 Hours
        Ext.Net.Node tnDoc4 = new Ext.Net.Node();
        tnDoc4.Text = "Sample Break Compliance Form 10 Hours";
        tnDoc4.Qtitle = "/Resources/READYC0MPLYdocs/Sample_Break_Compliance_Form_10_Hours.pdf"; 
        tnDoc4.Icon = Icon.PageWhiteAcrobat;

        Ext.Net.ConfigItem ciDoc4Site = new ConfigItem();
        ciDoc4Site.Name = "url";
        ciDoc4Site.Value = "/Resources/READYC0MPLYdocs/Sample_Break_Compliance_Form_10_Hours.pdf";
        ciDoc4Site.Mode = ParameterMode.Value;
        tnDoc4.CustomAttributes.Add(ciDoc4Site);

        // Folder Node: Compliance Library
        Ext.Net.Node tnComplianceLib = new Ext.Net.Node();
        tnComplianceLib.Text = "Compliance Library";
        tnComplianceLib.Icon = Icon.FolderBookmark;

        tnComplianceLib.Children.Add(tnDoc1);
        tnComplianceLib.Children.Add(tnDoc2);
        tnComplianceLib.Children.Add(tnDoc3);
        tnComplianceLib.Children.Add(tnDoc4);

        root.Children.Add(tnTasks);
        root.Children.Add(tnComplianceLib);
        treeCollection.Add(root);
        return treeCollection;
    }

    private Ext.Net.MenuPanel CreateMyFondantMenu()
    {
        // Administration Folder
        Ext.Net.MenuPanel mupnlAdmin = new Ext.Net.MenuPanel();
        mupnlAdmin.Title = "Administration";
        mupnlAdmin.Icon = Icon.FolderWrench;
        mupnlAdmin.Border = false;
        mupnlAdmin.SaveSelection = false;
        mupnlAdmin.Cls = "background-menu";
        mupnlAdmin.Menu.Listeners.Click.Handler = "addTab(#{tpMain}, menuItem.text, menuItem.url);"; 
        //task: this part goes to admin/index, but it is redirecting (found out because it returns code 302). This feature is currently not in use. 
        
        // User Maintenance
        Ext.Net.MenuItem muiUM = new Ext.Net.MenuItem();
        muiUM.Text = "Users";
        muiUM.Icon = Icon.Group;
        Ext.Net.ConfigItem ciurlUMUser = new ConfigItem();
        ciurlUMUser.Name = "url";
        ciurlUMUser.Value = "/Administration/Index?sessionId=" + Request.Cookies["SessionId"].Value; 
        ciurlUMUser.Mode = ParameterMode.Value;
        muiUM.CustomConfig.Add(ciurlUMUser);

        mupnlAdmin.Menu.Items.Add(muiUM);
        
       
        return mupnlAdmin;
    }

    public string ToAbsoluteUrl(string relativeUrl)
    {
       relativeUrl = relativeUrl.Replace("/", "%2f");
       relativeUrl = relativeUrl.Replace(" ", "+");
       return relativeUrl;                    
    }
    
    private Ext.Net.MenuPanel CreateSBECorpVIPFormsMenu()
    {
        // Referrals
        Ext.Net.MenuItem muiReferrals = new Ext.Net.MenuItem();
        muiReferrals.Text = "Referrals";
        muiReferrals.Icon = Icon.BookAddresses;      
        
        Ext.Net.ConfigItem ciurlReferrals = new ConfigItem();
        ciurlReferrals.Name = "url";
        ciurlReferrals.Value = "https://custom.myfondant.com/50200/multiapps/referrals.aspx/";
        ciurlReferrals.Mode = ParameterMode.Value;
        muiReferrals.CustomConfig.Add(ciurlReferrals);

        // Referral Locations
        Ext.Net.MenuItem muiReferralLocations = new Ext.Net.MenuItem();
        muiReferralLocations.Text = "Referral Locations";
        muiReferralLocations.Icon = Icon.Map;   

        Ext.Net.ConfigItem ciurlReferralLocations = new ConfigItem();
        ciurlReferralLocations.Name = "url";
        ciurlReferralLocations.Value = "https://custom.myfondant.com/50200/multiapps/referrallocations.aspx/";
        ciurlReferralLocations.Mode = ParameterMode.Value;
        muiReferralLocations.CustomConfig.Add(ciurlReferralLocations);

        // Corp. VIP Forms Folder
        Ext.Net.MenuPanel mupnlSBECorpVIP = new Ext.Net.MenuPanel();
        mupnlSBECorpVIP.Title = "Corp. VIP Forms";
        mupnlSBECorpVIP.Icon = Icon.FolderStar;
        mupnlSBECorpVIP.Border = false;
        mupnlSBECorpVIP.SaveSelection = false;
        mupnlSBECorpVIP.Cls = "background-menu";
        mupnlSBECorpVIP.Menu.Listeners.Click.Handler = "addTab(#{tpMain}, menuItem.text, menuItem.url);";
        mupnlSBECorpVIP.Menu.Items.Add(muiReferrals);
        mupnlSBECorpVIP.Menu.Items.Add(muiReferralLocations);

        return mupnlSBECorpVIP;
    }
</script>

<ext:Panel Layout="AccordionLayout" ID="acdnWest" runat="server" Animate="false">
</ext:Panel>


