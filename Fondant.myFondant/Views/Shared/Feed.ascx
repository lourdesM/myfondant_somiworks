﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<script runat="server">
    private string m_strFeedUrl = string.Empty;
    private bool m_boolUseZipCode = true;
    private string m_strZipCode = string.Empty;

    public string LoadFeedUrl
    {
        get { return m_strFeedUrl; }
        set { m_strFeedUrl = value; }
    }

    public bool UseZipCode
    {
        get { return m_boolUseZipCode; }
        set { m_boolUseZipCode = value; }
    }

    public string ZipCode
    {
        get { return m_strZipCode; }
        set { m_strZipCode = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!X.IsAjaxRequest)
        {
            try
            {
                string strDataFile;
                strDataFile = Server.UrlDecode(ConfigurationManager.AppSettings[m_strFeedUrl]);
                if (m_boolUseZipCode)
                {
                    if (m_strZipCode != string.Empty)
                    {
                        strDataFile = strDataFile + m_strZipCode;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                
                XmlDataSourceFeed.DataFile = strDataFile;

                DataListFeed.DataSourceID = "XmlDataSourceFeed";
                DataListFeed.DataBind();
            }
            catch
            {
                this.DataListFeed.Enabled = false;
                this.DataListFeed.Visible = false;

                this.LabelFeed.Enabled = true;
                this.LabelFeed.Visible = true;
                this.LabelFeed.Text = "Feed is currently unavailable in your selected area.";
            }
        }
    }

    [DirectMethod(ShowMask = true)]
    public void ChangeLocation(string zipCode)
    {
        string strDataFile;
        strDataFile = Server.UrlDecode(ConfigurationManager.AppSettings[m_strFeedUrl]);
        if (m_boolUseZipCode)
        {
            strDataFile = strDataFile + zipCode;
        }
        XmlDataSourceFeed.DataFile = strDataFile;
        XmlDataSourceFeed.DataBind();

        DataListFeed.DataSourceID = "XmlDataSourceFeed";
        DataListFeed.DataBind();
        DataListFeed.Update();
    }
</script>

<asp:XmlDataSource ID="XmlDataSourceFeed" runat="server" XPath="rss/channel/item [position()<=5]" CacheDuration="600" EnableCaching="true" CacheExpirationPolicy="Absolute" />
<asp:DataList ID="DataListFeed" runat="server">
     <ItemTemplate>
         <div><a href="<%#XPath("link")%>" target="_blank"><strong><%#XPath("title")%></strong></a></div>
         <div><i><%#XPath("pubDate")%></i></div>
         <div><%#XPath("description")%></div>
         <div><br /></div>
     </ItemTemplate>
 </asp:DataList>
<asp:Label ID="LabelFeed" runat="server" />
