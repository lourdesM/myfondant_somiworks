﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@Import Namespace="System.Net" %>
<%@Import Namespace="System.Configuration" %>
<%@Import Namespace="System.Security.Principal" %>
<%@Import Namespace="System.Web.Security" %>
<%@Import Namespace="Microsoft.ReportingServices" %>
<%@Import Namespace="Microsoft.Reporting.WebForms" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %>

<script runat="server">
    IReportServerCredentials crc = new CustomReportCredentials();

    protected void Page_Init(object sender, EventArgs e)
    {
        ReportViewer1.ServerReport.ReportServerCredentials = crc;
        ReportViewer1.ServerReport.Timeout = 420;
    }
    protected void Page_Load(object sender, EventArgs e)    
    {
        if (!IsPostBack)
        {
            try
            {

                ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["RSServer"]);
                while (this.ReportViewer1.ServerReport.IsDrillthroughReport)
                {
                    this.ReportViewer1.PerformBack();
                }
                ReportViewer1.ServerReport.ReportPath = Request.QueryString["rsurl"];

                // pass the user name to the report
                ReportParameterInfoCollection paramCollection = ReportViewer1.ServerReport.GetParameters();
                foreach (ReportParameterInfo paramInfo in paramCollection)
                {
                    if (paramInfo.Name.ToLower() == "username")
                    {
                        List<ReportParameter> paramList = new List<ReportParameter>();
                        paramList.Add(new ReportParameter("UserName", this.ViewState["UserName"].ToString()/*Profile.UserName*/, false));
                        ReportViewer1.ServerReport.SetParameters(paramList);
                        break;
                    }
                }

                ReportViewer1.ServerReport.Refresh();
            }
            catch (Exception ex)
            {
                ReportViewer1.Visible = false;
                //Response.Write("<p style='font-family:verdana,arial,helvetica;font-size:xx-small;padding:30px;'>An error was encountered while processing your report.  We apologize for the inconvenience.  Please notify your READY NUMB3R5 account manager at 1-877-2-Fondant.</p>");
                Response.Write(ex.Message);
            }
        }
    }
</script>

<html>
<head>
<title></title>
<style type="text/css">html,body, form {height:100%; vertical-align: bottom; margin: 0; padding: 0; border: none; overflow: hidden;}</style>
</head>
<body>
    <form runat="server">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" SizeToReportContent="True" ProcessingMode="Remote" ShowCredentialPrompts="False" 
            InternalBorderStyle="NotSet" EnableTheming="False" Font-Size="XX-Small" Height="100%" EnableViewState="true"
            Width="100%" BorderStyle="None" BackColor="Transparent" ShowDocumentMapButton="false" ShowRefreshButton="false" ShowBackButton="true" 
            ZoomMode="Percent" ZoomPercent="100" >
        </rsweb:ReportViewer>
    </form>
</body>
</html>