﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        this.txtClient.Text = Profile.ClientLongName;
        this.txtUserName.Text = Profile.UserName;
        //this.txtClient.Text = "SBE Entertainment";
        //this.txtUserName.Text = this.ViewData["UserName"].ToString();
    }    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript">
        var getMsg = function (action) {
            if (action.failureType == "client") {
                return "Please check fields.";
            }
            
            var msg = '';
            
            if (action.result && action.result.errors.length > 0) {
                msg = action.result.errors[0].msg;
            } else if(action.response) {
                msg = action.response.responseText;
            }

            return msg;
        };

        var failureHandler = function (form, action) {
            Ext.MessageBox.show({
                title: 'Failure',
                msg: getMsg(action),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        };
        
        var success = function (form, action) {
            eval(action.result.script);  
        };
    </script>

</head>

<body>

    <% X.Msg.Alert("Alert", "Inline MessageBox"); %>
    
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

    <% this.txtEmail.DataBind(); %>
    
    <ext:FormPanel 
        ID="FormPanel1" 
        runat="server" 
        Url="/Home/SendProblem/"
        Border="false" 
        BodyStyle="padding:10px;" 
        LabelAlign="Top"
        ButtonAlign="Center" MonitorResize="true">
        <Items>
            <ext:TextField ID="txtClient" runat="server" FieldLabel="Company" ReadOnly="true" AnchorHorizontal="47.5%" />
            <ext:Container ID="ContainerUser" runat="server" Layout="Column" Height="50">
                <Items>
                    <ext:Container ID="Container1" runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".5">
                        <Items>
                            <ext:TextField ID="txtUserName" runat="server" FieldLabel="Username" AllowBlank="false" AnchorHorizontal="95%" ReadOnly="true" />
                        </Items>
                    </ext:Container>
                    <ext:Container ID="Container2" runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".5">
                        <Items>
                            <ext:TextField ID="txtEmail" runat="server" FieldLabel="Email" Text='<%# this.ViewData["Email"] %>' AllowBlank="false" AnchorHorizontal="95%" ReadOnly="true" />
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Container>
            <ext:Container ID="ContainerComment" runat="server" LabelAlign="Top" Layout="Form">
                <Items>
                    <ext:HtmlEditor ID="txtComments" runat="server" FieldLabel="Comments" Height="200" AllowBlank="false" AnchorHorizontal="97.5%" />
                </Items>
            </ext:Container>
        </Items>
        <Buttons>
            <ext:Button ID="Button1" runat="server" Text="Send" Icon="EmailStart">
                <Listeners>
                    <Click Handler="#{FormPanel1}.form.submit({ waitMsg:'Sending...', success: success });" />
                </Listeners>
            </ext:Button>
        </Buttons>
    </ext:FormPanel>
</body>
</html>
