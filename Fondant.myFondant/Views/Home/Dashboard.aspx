﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@Import Namespace="System.Net" %>
<%@Import Namespace="System.Security.Principal" %>
<%@Import Namespace="System.Web.Security" %>
<%@Import Namespace="Microsoft.ReportingServices" %>
<%@Import Namespace="Microsoft.Reporting.WebForms" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)    
    {
        if (!IsPostBack)
        {
            //String strClientNo = Profile.ClientAccountNumber;
            String strClientNo = this.ViewData["AccountNumber"].ToString();
            if (DashboardExist(strClientNo))
            {
                try
                {
                    IReportServerCredentials crc = new CustomReportCredentials();
                    ReportViewer1.ServerReport.ReportServerCredentials = crc;

                    ReportViewer1.Enabled = true;
                    ReportViewer1.Visible = true;
                    ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["RSServer"]);
                    while (this.ReportViewer1.ServerReport.IsDrillthroughReport)
                    {
                        this.ReportViewer1.PerformBack();
                    }
                    ReportViewer1.ServerReport.ReportPath = "/" + strClientNo + "/Dashboard";

                    // pass the user name to the report
                    //ReportParameterInfoCollection paramCollection = ReportViewer1.ServerReport.GetParameters();
                    //foreach (ReportParameterInfo paramInfo in paramCollection)
                    //{
                    //    if (paramInfo.Name.ToLower() == "username")
                    //    {
                    //        List<ReportParameter> paramList = new List<ReportParameter>();
                    //        paramList.Add(new ReportParameter("UserName", Profile.UserName, false));
                    //        ReportViewer1.ServerReport.SetParameters(paramList);
                    //        break;
                    //    }
                    //}

                    ReportViewer1.ServerReport.Refresh();
                }
                catch (Exception ex)
                {
                    ReportViewer1.Visible = false;
                    Response.Write("<p style='font-family:verdana,arial,helvetica;font-size:xx-small;padding:30px;'>An error was encountered while processing your dashboard.  We apologize for the inconvenience.  Please notify your READY NUMB3R5 account manager at 1-877-2-Fondant.</p>");
                }
            }
            else
            {
                this.lblMsg.Text = "A dashboard has not yet been defined.  Please contact your myFondant account representative at 1-(877)2-Fondant.";
                this.lblMsg.Visible = true;
            }
        }
    }

    private Boolean DashboardExist(String ClientNumber)
    {
        Boolean blnReturn = false;

        Fondant.myFondant.Web.ReportingServices.CatalogItem[] cisItems = null;

        Fondant.myFondant.Web.ReportingServices.SearchCondition condition = new Fondant.myFondant.Web.ReportingServices.SearchCondition();
        condition.Condition = Fondant.myFondant.Web.ReportingServices.ConditionEnum.Equals;
        condition.ConditionSpecified = true;
        condition.Name = "Name";
        condition.Value = "Dashboard";

        Fondant.myFondant.Web.ReportingServices.SearchCondition[] conditions = new Fondant.myFondant.Web.ReportingServices.SearchCondition[1];
        conditions[0] = condition;

        try
        {
            Fondant.myFondant.Web.ReportingServices.ReportingService2005 rs = new Fondant.myFondant.Web.ReportingServices.ReportingService2005();
            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
           // CustomRS2005Credentials rs = new CustomRS2005Credentials();
           // rs.LogonUser(Profile.UserName, null, null); //original
            //rs.LogonUser(this.ViewData["UserName"].ToString(), null, null); //not reaching this part. This may be AccountNumber not Username
            cisItems = rs.FindItems("/" + ClientNumber, Fondant.myFondant.Web.ReportingServices.BooleanOperatorEnum.And, conditions);

            if (cisItems != null)
            {
                foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem ci in cisItems)
                {
                    blnReturn = true;
                    break;
                }
            }
        }

        catch
        {
            blnReturn = false; 
        }

        return blnReturn;
    }
</script>

<html>
<head>
<title></title>
<style type="text/css">html,body,form {height:100%; vertical-align: bottom; margin: 0; padding: 0; border: none; overflow: hidden;}</style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label runat="server" ID="lblMsg" Visible="false" Font-Size="XX-Small" Font-Names="verdana"></asp:Label>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" SizeToReportContent="True" ProcessingMode="Remote" ShowCredentialPrompts="False" 
            InternalBorderStyle="NotSet" EnableTheming="False" Font-Size="XX-Small" Height="100%"
            Width="100%" BorderStyle="None" BackColor="Transparent" ShowDocumentMapButton="false" ShowToolBar="false" 
            ZoomMode="Percent" ZoomPercent="100" Enabled="false" Visible="false" >
        </rsweb:ReportViewer>
    </form>
</body>
</html>