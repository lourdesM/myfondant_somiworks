﻿//Ext.ns('myFondant');

//myFondant = {
//    addTab: function (config) {
//        var tabPanel = Ext.getCmp('tpMain');
//        var tab = tabPanel.getComponent(config.id);
//        debugger;
//        alert('test1');
//        if (Ext.isEmpty(config.url, false)) {
//            return;
//        }


//        if (!tab) {
//            tab = tabPanel.add({
//                id: config.id,
//                title: config.title,
//                iconCls: config.icon || 'icon-applicationdouble',
//                closable: true,
//                layout: 'fit',
////                autoLoad: {
////                    showMask: true,
////                    url: config.url,
////                    iconCls: config.icon || 'icon-applicationdouble',
////                    mode: 'iframe',
////                    noCache: true,
////                    maskMsg: "Loading '" + config.title + "'...",
////                    scripts: true,
////                    passParentSize: config.passParentSize,
////                    params: { rsurl: config.rsurl }
////                }
//                loader: {
//                    url: config.url,
//                    //src: tabValue,
//                    renderer: "frame",
//                    loadMask: {
//                        showMask: true,
//                        msg: "Loading " + config.title + "..."
//                    }
//                }
//            });

//            tabPanel.setActiveTab(tab);
//        } else {
//            tabPanel.setActiveTab(tab);
//            Ext.get(tab.tabEl).frame();
//        }
//    }
//};

var addTab = function (tabPanel, tabTitle, tabValue) {
    var id = tabTitle == 'GL Detail Report' ? tabTitle + "" + Math.random() : tabTitle, //Allow to open identical tab only if it's for GL Detail Report
        text = tabTitle,
                tab = tabPanel.getComponent(text);
    //tabPanel.frame = false;

    if (!tab && tabValue != "") {
        tab = tabPanel.add({
            id: id,
            title: text,
            closable: true,
            //                    loader: {
            //                        //url: 'localhost:62248/Resources/READYC0MPLYdocs/Sample_Break_Compliance_Form_10_Hours.pdf',
            //                        url: tabValue,
            //                       // autoload: true,
            //                       // src: tabValue,
            //                        renderer: "frame",
            //                        loadMask: {
            //                            showMask: true,
            //                            msg: "Loading " + tabTitle + "..."
            //                        }
            //                    }
            xtype: 'component',
            autoEl: {
                tag: 'iframe',
                src: tabValue
            }


        });
    }
    tabPanel.setActiveTab(tab);
    
};
