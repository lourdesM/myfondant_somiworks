﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<ext:Window 
    ID="winAbout" 
    runat="server" 
    Title="About myFondant" 
    Icon="Information" 
    Modal="true"
    Height="225" 
    Width="350"
    Hidden="true">
    <Content>
        <ext:TabPanel ID="tpHelp" runat="server" Border="false">
            <Items>
                <ext:Panel ID="tabInfo" runat="server" Title="Info" BodyStyle="padding:5px;background-color:#fff;">
                    <Content>
                        <div>Fondant Systems</div>
                        <div>46 Smith Alley</div>
                        <div>Suite 220</div>
                        <div>Pasadena, CA 91103</div>
                        <br />
                        <div>1-(877)-2-Fondant</div>
                        <br />
                        <div>myFondant by <a href="http://www.fondantsystems.com/" target="_blank">Fondant Systems</a>.</div>
                    </Content>
                </ext:Panel>
            </Items>
        </ext:TabPanel>
    </Content>
    <BottomBar>
        <ext:Toolbar ID="Toolbar1" runat="server">
            <Items>
                <ext:ToolbarFill ID="fill2" runat="server" />
                <ext:ToolbarTextItem ID="txtCopyright" runat="server" Text="Copyright 2010 Fondant Systems<br />" />
                <ext:ToolbarSpacer ID="spacer2" runat="server" />
            </Items>
        </ext:Toolbar>
    </BottomBar>
</ext:Window>