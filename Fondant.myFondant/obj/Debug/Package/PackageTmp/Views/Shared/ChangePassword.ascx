<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<%--  <script type="text/javascript" src="/Scripts/myFondant.js"></script>--%>
  <ext:XScript ID="XScript1" runat="server">
     <script type="text/javascript">
        Ext.Ajax.timeout = 120000;
        Ext.net.DirectEvent.timeout = 120000;
         function changePassword() {
         if(#{NewPassword}.getValue()==#{confirmPassword}.getValue()){
             Ext.Ajax.request
                                        ({
                                            url: '/Account/ChangePassword2/',
                                            method: 'POST',
                                            params: {
                                                OldPassword: #{OldPassword}.getValue(),
                                                NewPassword: #{confirmPassword}.getValue(),
                                                
                                            },
                                            success: function (response) {
                                                var params = Ext.JSON.decode(response.responseText);
                                                                  
                                                if (params.extraParamsResponse.messageSuccess == "Success") {
                                                
                                                    Ext.Msg.alert("Submit", "Password has been changed successfully!");
                                                }
                                                else {
                                                    Ext.Msg.alert("Submit", params.extraParamsResponse.messageFailed);
                                                }
                                                 parent.closewinChangePassword();
                                            },
                                            failure: function (response) {

                                                Ext.Msg.alert("Submit", "Operation failed due to an Internal Error!");
                                                  parent.closewinChangePassword();
                                            }

                                        }); 
          }
          else{
           Ext.Msg.alert("Submit", "Password does not match!");
          }
        }

        
     </script>
     </ext:XScript>

<ext:Window 
    ID="winChangePassword" 
    runat="server" 
    Title="Change Password" 
    Icon="LockKey" 
    Modal="true"
    Height="225" 
    Width="300" Resizable="false"
    Hidden="true">
    <Content>
        <ext:FormPanel ID="fpChgPwd" runat="server" Padding="10" ButtonAlign="Right" Frame="false" Layout="Form">
            <Items>
                <ext:TextField ID="OldPassword" runat="server" FieldLabel="Current Password" AnchorHorizontal="100%" MaxLength="30" InputType="Password" AllowBlank="false" BlankText="Current password is required."  />
                <ext:TextField ID="NewPassword" runat="server" FieldLabel="New Password" AnchorHorizontal="100%" MaxLength="30" InputType="Password" AllowBlank="false" BlankText="New password is required." />
                <ext:TextField ID="confirmPassword" runat="server" FieldLabel="Confirm Password" AnchorHorizontal="100%" MaxLength="30" InputType="Password" AllowBlank="false" BlankText="Confirm password is required." Note="Password must meet the minimum required length of 7 characters including 1 non-alpha." NoteCls="font-size:xx-small;" />
            </Items>
            <Buttons>
             <ext:Button ID="btnChgPwdSave" runat="server" icon="Disk" Text="Save"  >
                 <Listeners>
                      <Click Handler="changePassword()" />
                 </Listeners>                                        
             </ext:Button>
               <%-- <ext:Button ID="btnChgPwdSave" runat="server" Icon="Disk" Text="Save">
                    <DirectEvents>
                        <Click 
                            Url="/Account/ChangePassword/" 
                            FormID="frmChgPwd"
                            Timeout="60000"
                            CleanRequest="true" 
                            Method="POST"
                            Before="Ext.Msg.wait('Verifying New Password...', 'Save');"
                            Failure="Ext.Msg.show({
                               title:   'Save',
                               msg:     result.errorMessage,
                               buttons: Ext.Msg.OK,
                               icon:    Ext.MessageBox.ERROR
                            });"
                            Success="Ext.Msg.show({
                               title:   'Save',
                               msg:     'New password saved.',
                               buttons: Ext.Msg.OK,
                               icon:    Ext.MessageBox.INFO,
                               fn:      btnChgPwdCancel.fireEvent('click')
                            });">
                            <EventMask MinDelay="500" />
                        </Click>
                    </DirectEvents>
                </ext:Button>--%>
                <ext:Button ID="btnChgPwdCancel" runat="server" Icon="Cancel" Text="Cancel">
                    <Listeners>
                       <%-- <Click Handler="parent.window.winChangePassword.hide();" />--%>
                       <Click Handler="parent.closewinChangePassword()" />
                    </Listeners>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </Content>
</ext:Window>
