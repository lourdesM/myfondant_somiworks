﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="ForgotPassword.ascx" TagName="ForgotPassword" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>myFondant Client Log In</title>
    <link href="../../Content/Site.css" rel="stylesheet" type="text/css" />
    <ext:ResourcePlaceHolder ID="ResourcePlaceHolder1" runat="server" Mode="Script" />
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                this.ResourceManagerClientLogIn.RegisterIcon(Icon.Information);
            }
        }
    </script>
    <script language="JavaScript" src="https://seal.networksolutions.com/siteseal/javascript/siteseal.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        Ext.onReady(function() {
            if (top.location.href !== window.location.href) {
                parent.location.href = "http://www.myfondant.com/";
            };
            Username.focus(false, 1000);
        });
    </script>
</head>

<body>
    <ext:ResourceManager ID="ResourceManagerClientLogIn" runat="server" />

    <div class="page">
        <div id="header"></div>
        <div id="main">
            <ext:Panel ID="NonAuthPanel110" runat="server" Frame="false" Border="false" Height="460">
                <Content>
                    <ColumnModel Layout="ColumnLayout" ID="NonAuthColumnLayout1" runat="server" FitHeight="true" >
                        <Columns>
                            <ext:LayoutColumn ColumnWidth="0.30">
                            <ext:Panel ID="NonAuthPanel120" runat="server" Border="false">
                                <Content>
                                <RowLayout ID="NonAuthRowLayout110" runat="server">
                                    <Rows>
                                        <ext:LayoutRow RowHeight="0.6">
                                            <ext:Panel ID="NonAuthPanel130" runat="server" Frame="true" Title="Client Login" BodyStyle="padding:10px;" StyleSpec="padding: 0 10px 0 0 ;" Icon="Lock">
                                                <Items>
                                                <%-- JVA removed from below: BodyBorder="false" --%>
                                                    <ext:FormPanel ID="FormPanel1" 
                                                        runat="server" 
                                                        FormID="form1"
                                                        Border="false"
                                                        BodyStyle="background:transparent;"
                                                        LabelWidth="90">
                                                        <Items>
                                                            <ext:TextField 
                                                                ID="Username" 
                                                                runat="server" 
                                                                FieldLabel="Username" 
                                                                AnchorHorizontal="100%"
                                                                TabIndex="0"
                                                                />
                                                            <ext:TextField 
                                                                ID="Password" 
                                                                runat="server" 
                                                                InputType="Password" 
                                                                FieldLabel="Password" 
                                                                AnchorHorizontal="100%"
                                                                />
                                                            <ext:Checkbox ID="RememberMe" runat="server" InputValue="true" FieldLabel="Remember Me" Note="On a public computer, please keep the checkbox clear for security." NoteCls="font-size:.9em;" />
                                                        </Items>
                                                        <Buttons>
                                                            <ext:Button ID="btnLogin" runat="server" Text="Login" Icon="Accept">
                                                                <DirectEvents>
                                                                    <Click
                                                                        Url="/Account/ClientLogInNow/" 
                                                                        Timeout="60000"
                                                                        FormID="form1"
                                                                        CleanRequest="true" 
                                                                        Before="Ext.Msg.wait('Verifying...', 'Login');"
                                                                        Failure="Ext.Msg.show({
                                                                           title:   'Login Error',
                                                                           msg:     result.errorMessage,
                                                                           buttons: Ext.Msg.OK,
                                                                           icon:    Ext.MessageBox.ERROR
                                                                        });">
                                                                        <EventMask MinDelay="500" />
                                                                        <ExtraParams>
                                                                            <ext:Parameter Name="ReturnUrl" Value="Ext.urlDecode(String(document.location).split('?')[1]).r || '/'" Mode="Raw" />
                                                                        </ExtraParams>
                                                                    </Click>
                                                                </DirectEvents>
                                                            </ext:Button>
                                                        </Buttons>
                                                    </ext:FormPanel>
                                                </Items>
                                                <Content>
                                                    <uc1:ForgotPassword ID="ucForgotPassword" runat="server" />
                                                </Content>
                                            </ext:Panel>
                                        </ext:LayoutRow>
                                        <ext:LayoutRow RowHeight="0.4">
                                            <ext:Panel runat="server" ID="NonAuthPanel201" Border="false" Frame="false" BodyStyle="padding:10px;background-color: white;" StyleSpec="padding: 30px 0 0 0 ;" >
                                                <Content>
                                                    <div align="center">
						                                <!--
						                                SiteSeal Html Builder Code:
						                                Shows the logo at URL https://seal.networksolutions.com/images/basicsqblue.gif
						                                Logo type is  ("NETSP")
						                                //-->
						                                <script language="JavaScript" type="text/javascript">SiteSeal("https://seal.networksolutions.com/images/basicsqblue.gif", "NETSP", "none");</script>
                                                </Content>
                                            </ext:Panel>
                                        </ext:LayoutRow>
                                    </Rows>
                                </RowLayout>
                                </Content>
                                </ext:Panel>
                            </ext:LayoutColumn>
                            <ext:LayoutColumn ColumnWidth="0.70">
                                <ext:Panel ID="Panel1" runat="server" Border="false">
                                    <Content>
                                        <RowLayout ID="RowLayout1" runat="server">
                                            <Rows>
                                                <ext:LayoutRow RowHeight="0.8">
                                                    <ext:Panel ID="NonAuthPanel103" runat="server" BodyStyle="background-color: white;padding: 7px;" Frame="true">
                                                        <Content>
                                                            <div style="text-align:center;"><img height="375" alt="" width="500" border="0" src="../../Resources/MyFondant.jpg" style="margin:0px 0px 0px 0px;" /></div>
                                                        </Content>
                                                    </ext:Panel>
                                                </ext:LayoutRow>
                                                <ext:LayoutRow RowHeight="0.2">
                                                    <ext:Panel ID="NonAuthPanel104" runat="server" BodyStyle="background-color: white;padding: 7px;" Frame="true">
                                                        <Content>
                                                            <h5>Important Reminder:</h5>
                                                            <div>Fondant Systems technical support staff will <b>never</b> ask for your login information.</div>
                        						            <div></div>
                                                            <h5>New Users:</h5>
                                                            <div>Please&#160;check your user name and temporary password from your email account or contact your organization's&#160;Fondant Systems&#160;administrator for further instructions. Unauthorized access is prohibited.</div>
                                                        </Content>
                                                    </ext:Panel>
                                                </ext:LayoutRow>
                                            </Rows>
                                        </RowLayout>
                                    </Content>
                                </ext:Panel>
                            </ext:LayoutColumn>
                        </Columns>
                    </ColumnModel>
                 </Content>
            </ext:Panel>
            
            <div id="footer">
                Copyright 2010 Fondant Systems
            </div>
        </div>

    </div>
    <ext:KeyMap ID="KeyMap1" runat="server" Target="={Ext.isGecko ? Ext.getDoc() : Ext.getBody()}">
     <Binding>
<%--  JVA - changed keybinding and removed stopevent <ext:KeyBinding StopEvent="true">--%>
        <ext:KeyBinding Handler="#{btnLogin}.fireEvent('click');">
            <Keys>
                <ext:Key Code="ENTER" />
                <ext:Key Code="RETURN" />
            </Keys>
<%--            <Listeners>
                <Event Handler="#{btnLogin}.fireEvent('click');" />
            </Listeners>--%>
        </ext:KeyBinding>
     </Binding>
    </ext:KeyMap>
</body>
</html>