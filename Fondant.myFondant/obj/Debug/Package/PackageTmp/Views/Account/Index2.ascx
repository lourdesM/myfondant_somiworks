﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Assembly="System.Web.ApplicationServices" Namespace="System.Web.Security" TagPrefix="web" %>
<%@Import Namespace="Fondant.myFondant.Web.Code" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script runat="server">    
        protected void Page_Load(object sender, EventArgs e)    
        {
            if (!X.IsAjaxRequest)
            {
                this.ResourceManagerAdmin.RegisterIcon(Icon.Information); 
                //this.Store1.Parameters.Add(new Ext.Net.Parameter { Name = "ClientId", Value = Profile.ClientId.ToString(), Mode = ParameterMode.Raw }); //original
                this.Store1.Parameters.Add(new Ext.Net.Parameter { Name = "ClientId", Value = this.ViewData["ClientID"].ToString(), Mode = ParameterMode.Raw }); //modified
            }
            ////added
            //Fondant.myFondant.Web.Controllers.DataController datacontroller = new Fondant.myFondant.Web.Controllers.DataController();
            //this.Store1.DataSource = datacontroller.GetUsers(Convert.ToInt32(this.ViewData["ClientID"])).Data;
            ////end of added
        }

        [DirectMethod(ShowMask = true)]
        public void ResetPassword(string userName)
        {
            string strPwd = string.Empty;
            string strMsg = string.Empty;
            try
            {
                if (userName == string.Empty || userName == "")
                {
                    strMsg = "Please select a username to reset password.";
                }
                else
                {
                    // find out first if User's PasswordFormat in DB is 1, if so, update to 2 before calling ResetPassword
                    
                    strPwd = Membership.Provider.ResetPassword(userName, string.Empty);
                    strMsg = "New password was sent to " + userName + ".";

                    myFondantUtilities myFUtils = new myFondantUtilities();
                    string strEmailMsg = myFUtils.CreateResetPasswordMessage(strPwd);
                    //MembershipUser mUser = Membership.GetUser(userName); //original
                    string mUser = this.ViewData["email"].ToString(); //modified
                    //if (mUser.Email == string.Empty || mUser.Email == "") //original
                    if (mUser == string.Empty || mUser == "")
                    {
                        strMsg = "No email was found under the username provided.  Please contact your company administrator.";
                    }
                    else
                    {
                        //myFUtils.SendEmail(mUser.Email, "Your myFondant Password", strEmailMsg); //original
                        myFUtils.SendEmail(mUser, "Your myFondant Password", strEmailMsg); //modified
                    }
                }
            }
            catch (Exception ex)
            {
                strMsg = string.Concat("The password could not be reset at this time.  ", ex.Message);
            }
            finally
            {
                Notification.Show(new NotificationConfig
                {
                    Title = "Reset Password",
                    Icon = Icon.Information,
                    AlignCfg = new NotificationAlignConfig
                    {
                        ElementAnchor = AnchorPoint.BottomRight,
                        TargetAnchor = AnchorPoint.BottomRight,
                        OffsetX = -50,
                        OffsetY = -50
                    },
                    ShowFx = new Frame { Color = "#FCD856", Count = 1, Options = new FxConfig { Duration = 5 } },
                    HideFx = new SwitchOff(),
                    Html = strMsg
                });
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        var fullName = function (value, metadata, record, rowIndex, colIndex, store) {
            var suffix = record.data.Suffix || '';
            var middlename = record.data.MiddleName || '';
            return '<b>' + record.data.LastName + ' ' + suffix + ', ' + record.data.FirstName + ' ' + middlename + '</b>';
        }

        var commandHandler = function (cmd, record) {
            switch (cmd) {
                case "edit":
                    var winUDW = UserDetailWindow;
                    winUDW.autoLoad.params.UserId = record.id;
                    winUDW.autoLoad.params.IsNewUser = false;
                    winUDW.setTitle('Edit ' + record.data.LastName);
                    winUDW.show();
                    break;
                case "delete":
                    Ext.Msg.confirm('Alert', 'Delete Record?', function (btn) {
                        if (btn == "yes") {
                            dsCustomers.remove(record);
                            dsCustomers.save();
                        }
                    });
                    break;
                case "new":
                    var winNUW = NewUserWindow;
                    winNUW.setTitle('New User');
                    winNUW.show();
                    break;
                case "resetPwd":
                    Ext.Msg.confirm('Reset Password', 'Reset and email ' + record.data.UserName + ' their new password?', function (btn) {
                        if (btn == "yes") {
                            Ext.net.DirectMethods.ResetPassword(record.data.UserName);
                        }
                    });
                    break;
            }
        }

        function nextUserDetail(userId) {
            var win = UserDetailWindow;
            win.autoLoad.params.UserId = userId;
            win.autoLoad.params.IsNewUser = true;
            win.setTitle('New User');
            win.show();
        }
    </script>
</head>
<body>
    <ext:ResourceManager ID="ResourceManagerAdmin" runat="server" />

    <ext:TextField ID="txtUserId" runat="server" Hidden="true"/>

    <ext:Store ID="Store1" runat="server" UseIdConfirmation="true">
        <Proxy>
            <ext:AjaxProxy Url="/Data/GetUsers/">
                <Reader>
                    <ext:JsonReader IDProperty="UserId" Root="data" TotalProperty="totalCount">
                    </ext:JsonReader>
                </Reader>              
            </ext:AjaxProxy>
        </Proxy>
        <Model>
            <ext:Model ID="Model1" runat="server">
                <Fields>
                    <ext:ModelField Name="ClientId" />
                    <ext:ModelField Name="UserId" />
                    <ext:ModelField Name="FirstName" />
                    <ext:ModelField Name="MiddleName" />
                    <ext:ModelField Name="LastName" />
                    <ext:ModelField Name="Suffix" />
                    <ext:ModelField Name="UserName" />
                    <ext:ModelField Name="LoweredEmail" />
                    <ext:ModelField Name="CreateDate" Type="Date" />
                    <ext:ModelField Name="LastLoginDate" Type="Date" />
                    <ext:ModelField Name="IsActive" Type="Boolean" />
                </Fields>
            </ext:Model>
        </Model>    
        <Sorters>
            <ext:DataSorter Property="LastName" Direction="ASC" />
        </Sorters>            
    </ext:Store>
    
    <ext:ViewPort ID="ViewPort1" runat="server" Layout="Fit">
        <Items>
            <ext:GridPanel ID="GridPanel1" runat="server" StoreID="Store1" Header="false" Border="false" StripeRows="true" AutoExpandColumn="fullName" TrackMouseOver="true">
                <ColumnModel ID="ColumnModel1" runat="server">
                    <Columns>
                        <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Width="25"></ext:RowNumbererColumn>
                        <ext:Column ID="Column1" ColumnID="UserId" Hidden="true" runat="server" DataIndex="UserId" />
                        <ext:Column ID="Column2" ColumnID="fullName" Header="Full Name" runat="server" DataIndex="LastName">
                            <Renderer Fn="fullName" />
                        </ext:Column>
                        <ext:Column ID="Column3" DataIndex="UserName" Header="User Name" runat="server" Width="125" />
                        <ext:Column ID="Column4" DataIndex="LoweredEmail" Header="Email" runat="server" Width="190" />
                        <ext:Column ID="Column5" DataIndex="CreateDate" Header="Created Date" runat="server" Width="120">
                            <Renderer Fn="Ext.util.Format.dateRenderer('m-d-Y g:i a')" />
                        </ext:Column>
                        <ext:Column ID="Column6" DataIndex="LastLoginDate" Header="Last Login Date" runat="server" Width="120">
                            <Renderer Fn="Ext.util.Format.dateRenderer('m-d-Y g:i a')" />
                        </ext:Column>
                        <ext:CheckColumn ID="CheckColumn1" DataIndex="IsActive" Header="Active" runat="server" Width="97" Sortable="false" Align="Center" Editable="false" />
                    </Columns>
                </ColumnModel>
                <TopBar>
                    <ext:Toolbar ID="Toolbar1" runat="server">
                        <Items>
                            <ext:Button ID="tbNew" runat="server" Text="New" Icon="UserAdd">
                                <Listeners>
                                    <Click Handler="commandHandler('new', '');"/>
                                </Listeners>
                            </ext:Button>
                            <ext:Button ID="tbEdit" runat="server" Text="Edit" Icon="UserEdit" Disabled="true">
                                <Listeners>
                                    <Click Handler="commandHandler('edit', #{GridPanel1}.getSelectionModel().getSelected());"/>
                                </Listeners>
                            </ext:Button>
                            <ext:Button ID="tbResetPwd" runat="server" Text="Reset Password" Icon="Key" Disabled="true">
                                <Listeners>
                                    <Click Handler="commandHandler('resetPwd', #{GridPanel1}.getSelectionModel().getSelected());" />
                                </Listeners>
                            </ext:Button>
                        </Items>
                    </ext:Toolbar>
                </TopBar>
                <SelectionModel>
                    <ext:RowSelectionModel ID="RowSelectionModel1" runat="server">
                        <Listeners>
                            <Select Handler="#{tbEdit}.enable();#{tbResetPwd}.enable();" />
                            <Deselect Handler="if (!#{GridPanel1}.hasSelection()) {#{tbEdit}.disable();#{tbResetPwd}.disable();}" />
                        </Listeners>
                    </ext:RowSelectionModel>
                </SelectionModel>
                <Listeners>
                    <ItemDblClick Handler="commandHandler('edit', #{GridPanel1}.getSelectionModel().getSelected());" />
                   <%-- <Command Fn="commandHandler" />--%>
                </Listeners>
                <%--<LoadMask ShowMask="true" />--%>
            </ext:GridPanel>
        </Items>
    </ext:ViewPort>

    <ext:Window
        ID="UserDetailWindow" 
        runat="server" 
        Icon="UserEdit" 
        Width="600" 
        Height="430" 
        Hidden="true" 
        Modal="true"
        Constrain="true">
        <Loader ID="Loader1" 
            runat="server"
            Url="/Administration/UserDetail/" 
            Mode="Frame"
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="false" 
            MaskMsg="Loading Edit User Form..." >
            <Params>
                <ext:Parameter Name="UserId" Value="" Mode="Value" />
                <ext:Parameter Name="IsNewUser" Value="" Mode="Value" />
            </Params>
        </Loader>
    </ext:Window>
    
    <ext:Window 
        ID="NewUserWindow" 
        runat="server" 
        Icon="UserAdd" 
        Width="370" 
        Height="230" 
        Hidden="true" 
        Modal="true"
        Constrain="true">
        <Loader ID="Loader2"
            runat="server"
            Url="/Administration/NewUser/" 
            Mode="Frame" 
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="true" 
            MaskMsg="Loading New User Form...">
        </Loader>
    </ext:Window>
</body>
</html>
