﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Linq" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    public const string SQL_CONNECTION_STRING = "server=54.149.142.14;database=RN_THEDRYBAR_SETUP;user id=sa; Password=H9mhbr2k";
    IList<StoreGLDetail> GLDetailData = new List<StoreGLDetail>();
    private class StoreGLDetail
    {
        public int Id { get; set; }
        public string Subsidiary { get; set; }
        public string ClosedOrderDate { get; set; }
        public string Posting_Period { get; set; }
        public string gl_currency { get; set; }
        public int gl_account { get; set; }
        public string gl_account_name { get; set; }
        public decimal DEBIT { get; set; }
        public decimal CREDIT { get; set; }
        public string Memo { get; set; }
        public string Class { get; set; }
        //public string Location { get; set; }
        public int LocationInternalID { get; set; }
        public string Status { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!X.IsAjaxRequest && !IsPostBack)
        {
            this.frmDateSearch.MinDate = DateTime.Now.AddDays(-15);
            this.frmDateSearch.MaxDate = DateTime.Now;

            this.toDateSearch.MinDate = DateTime.Now.AddDays(-15);
            this.toDateSearch.MaxDate = DateTime.Now;
            this.hdnSessionId.Value = Request.QueryString["sessionId"];
           
            Fondant.myFondant.Web.Controllers.GLDataController GLDataController = new Fondant.myFondant.Web.Controllers.GLDataController();
            if (System.Web.HttpContext.Current.Application["ClosedOrderDates_" + this.hdnSessionId.Value] == "")
            {
                this.toDateSearch.Text = this.frmDateSearch.Text = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            }
            else
            {
                this.toDateSearch.Text = this.frmDateSearch.Text = "";
            }

            //For All Status combobox
            cboAllStatus.Items.Add(new Ext.Net.ListItem("All Status"));
            cboAllStatus.Items.Add(new Ext.Net.ListItem("posted"));
            cboAllStatus.Items.Add(new Ext.Net.ListItem("unposted"));

            this.hdnClosedOrderDates.Value = System.Web.HttpContext.Current.Application["ClosedOrderDates_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["ClosedOrderDates_" + this.hdnSessionId.Value];
            this.hdnLocationIds.Value = System.Web.HttpContext.Current.Application["LocationIds_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["LocationIds_" + this.hdnSessionId.Value];
            this.hdnLocationInternalIDs.Value = System.Web.HttpContext.Current.Application["LocationInternalIDs_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["LocationInternalIDs_" + this.hdnSessionId.Value];
            this.hdnStatus.Value = System.Web.HttpContext.Current.Application["Status_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["Status_" + this.hdnSessionId.Value];
            this.hdnStartDate.Value = System.Web.HttpContext.Current.Application["StartDate_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["StartDate_" + this.hdnSessionId.Value];
            this.hdnEndDate.Value = System.Web.HttpContext.Current.Application["EndDate_" + this.hdnSessionId.Value] == null ? "" : System.Web.HttpContext.Current.Application["EndDate_" + this.hdnSessionId.Value];
            
            ArrayList arrLocationInternalId = new ArrayList();
            ArrayList arrLocationName = new ArrayList();
            ArrayList arrSubsidiary = new ArrayList();
            ArrayList arrCurrency = new ArrayList();
            //FOR GL Summary DataGrid            
           // if (this.hdnClosedOrderDates.Value.ToString() != "" && this.hdnLocationIds.Value.ToString() != "" && this.hdnStartDate.Value.ToString() != "" && this.hdnEndDate.Value.ToString() != "")
           // {
            DataSet temp = GLDataController.GetFranchiseSalesReader(this.hdnClosedOrderDates.Value.ToString(), this.hdnLocationIds.Value.ToString(), this.hdnLocationInternalIDs.Value.ToString(), this.hdnStatus.Value.ToString(), this.hdnStartDate.Value.ToString(), this.hdnEndDate.Value.ToString());
                foreach (DataRow dr in temp.Tables[0].Rows)
                {
                    StoreGLDetail GLData = new StoreGLDetail();
                    GLData.Id = !Convert.IsDBNull(dr["Id"]) ? Convert.ToInt32(dr["Id"]) : 0;
                    GLData.Subsidiary = dr["Subsidiary"].ToString();
                    GLData.ClosedOrderDate = dr["ClosedOrderDate"].ToString();
                    GLData.Posting_Period = dr["Posting_Period"].ToString();
                    GLData.gl_currency = dr["gl_currency"].ToString();
                    GLData.gl_account = !Convert.IsDBNull(dr["gl_account"]) ? Convert.ToInt32(dr["gl_account"]) : 0;
                    GLData.gl_account_name = dr["gl_account_name"].ToString();
                    GLData.DEBIT = !Convert.IsDBNull(dr["DEBIT"]) ? Convert.ToDecimal(dr["DEBIT"]) : 0;
                    GLData.CREDIT = !Convert.IsDBNull(dr["CREDIT"]) ? Convert.ToDecimal(dr["CREDIT"]) : 0;
                    GLData.Memo = dr["Memo"].ToString();
                    GLData.Class = dr["Class"].ToString();
                    //GLData.Location = dr["Location"].ToString();
                    GLData.LocationInternalID = !Convert.IsDBNull(dr["LocationInternalId"]) ? Convert.ToInt32(dr["LocationInternalId"]) : 0;
                    GLData.Status = dr["Status"].ToString();
                    GLData.LocationID = !Convert.IsDBNull(dr["LocationID"]) ? Convert.ToInt32(dr["LocationID"]) : 0;
                    GLData.LocationName = dr["LocationName"].ToString();
                    GLDetailData.Add(GLData);
                }
                this.strGLDetail.DataSource = GLDetailData;
                this.strGLDetail.DataBind();
            //}
          

            //Convert GLDetailDate to JSON format and store it to a hidden field. This is for the purpose of exporting the JE into a CSV file.
            if (GLDetailData.Count > 0)
            {
                this.btnToCSV.Enabled = true;
                System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
                string output = jss.Serialize(GLDetailData);
                this.JsonGLEntries.Value = output;
            }
            else
            {
                this.btnToCSV.Enabled = false;
            }

            //For Locations combobox
            DataSet dsLocations = GLDataController.GetAllLocations();
            cboLocation.Items.Add(new Ext.Net.ListItem("All Locations", "All Locations"));
            foreach (DataRow dr in dsLocations.Tables[0].Rows)
            {
                cboLocation.Items.Add(new Ext.Net.ListItem(dr["LocationName"].ToString(), dr["LocationID"]));
            }

            //DISABLES ADD NEW RECORD, EDIT, AND POST BUTTONS JE IF STATUS CONTAINS POSTED
            this.hdnEntries.Value = System.Web.HttpContext.Current.Application["Entries_" + this.hdnSessionId.Value];
            string[] Status = (this.hdnStatus.Value.ToString()).Split(',');
            string[] Entries = (this.hdnEntries.Value.ToString()).Split(',');
            string userRole = (string)System.Web.HttpContext.Current.Application["LoginUserRole_For_DryBar_" + this.hdnSessionId.Value];
            if (userRole == "Accounting Admin")
            {
                if (Status.Contains("'unposted'"))
                {
                    this.btnInsert.Disabled = false;
                    if (Entries.Contains("'unbalanced'"))
                    {
                        this.btnPostJE.Disabled = true;
                    }
                    else
                    {
                        this.btnPostJE.Disabled = false;
                    }
                  
                }
               
            }
            else
            {
                if (Status.Contains("'posted'"))
                {
                    this.btnPostJE.Disabled = true;
                }
                else
                {
                    if (Entries.Contains("'unbalanced'"))
                    {
                        this.btnPostJE.Disabled = true;
                    }
                    else
                    {
                        this.btnPostJE.Disabled = false;
                    }
                }
            }




            System.Web.HttpContext.Current.Application["JE_ClosedOrderDates_" + this.hdnSessionId.Value] = System.Web.HttpContext.Current.Application["ClosedOrderDates_" + this.hdnSessionId.Value];
            System.Web.HttpContext.Current.Application["JE_LocationIds_" + this.hdnSessionId.Value] = System.Web.HttpContext.Current.Application["LocationIds_" + this.hdnSessionId.Value];
            System.Web.HttpContext.Current.Application["JE_LocationInternalIDs_" + this.hdnSessionId.Value] = System.Web.HttpContext.Current.Application["LocationInternalIDs_" + this.hdnSessionId.Value];
            System.Web.HttpContext.Current.Application["JE_Status_" + this.hdnSessionId.Value] = System.Web.HttpContext.Current.Application["Status_" + this.hdnSessionId.Value];
            System.Web.HttpContext.Current.Application["JE_Entries_" + this.hdnSessionId.Value] = System.Web.HttpContext.Current.Application["Entries_" + this.hdnSessionId.Value];

            System.Web.HttpContext.Current.Application["ClosedOrderDates_" + this.hdnSessionId.Value] = "";
            System.Web.HttpContext.Current.Application["LocationIds_" + this.hdnSessionId.Value] = "";
            System.Web.HttpContext.Current.Application["LocationInternalIDs_" + this.hdnSessionId.Value] = "";
            System.Web.HttpContext.Current.Application["Status_" + this.hdnSessionId.Value] = "";
            System.Web.HttpContext.Current.Application["Entries_" + this.hdnSessionId.Value] = "";


            X.Call("HideLoadIcon");
        }
    }

   
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Franchise Sales Report</title>
    <script src="../../Scripts/jquery-1.11.1.min.js" type="text/javascript"></script>
     <style type="text/css">
     
        #loadingmessage, #loadingmessage1, #postingMessage
        {
            position:fixed;
            opacity:0.5;
            top:0px;
            left:0px;
            height:100%;
            width:100%;
            background-color:#ccc;
            z-index:1000;
            cursor:progress;
        }
        #spinner, #spinner1, #spinnerPosting
        {
            position:fixed;
            top:50%;
            left:0%;
            height:100%;
            width:100%;
            text-align:center;
            vertical-align:middle;
        
        }
    </style>
    <ext:XScript ID="XScript1" runat="server">
     <script type="text/javascript">
        //loadStore Timeout---
        Ext.Ajax.timeout = 1000000;//120000;
        Ext.net.DirectEvent.timeout = 1000000;// 120000;

        window.onload= function(){
            LoadIcon1();
        }

         function LoadIcon() {
             $("#loadingmessage").show();
         }

          function LoadIcon1() {
             $("#loadingmessage1").show();
         }

         function HideLoadIcon() {
             $("#loadingmessage").hide();
             $("#loadingmessage1").hide();
         }

      

        // if status is Posted, then font color should be green----
        var template = '<span style="color:{0};">{1}</span>';
        var fontColorChangeIfPosted= function(value){
                 return Ext.String.format(template, (value == 'posted') ? "green" : "black", value);
         }

       var successFilter = function (result) {
            var params= Ext.JSON.decode(result.responseText); 
             #{strGLDetail}.removeAll();   
             #{strGLDetail}.loadRawData(params.result); 
             Ext.getCmp('cboLocation').enable(); 
             Ext.getCmp('cboAllStatus').enable();
             Ext.getCmp('btnClearSearch').enable();
             HideLoadIcon();
         };

          var f = [];     
          var filterLocation=true;
          var filterStatus=true;     

       var applyFilter = function () {                
                var store = #{grdGLDetail}.getStore();
                var locationVal=#{cboLocation}.getDisplayValue();
                var entryVal=#{cboAllStatus}.getDisplayValue(); 

                if(locationVal=="All Locations" && entryVal=="All Status"){                   
                   #{strGLDetail}.clearFilter();                   
                }

                else if(locationVal != "All Locations" && entryVal != "All Status"){
                    filterLocation=true;
                    filterStatus=true; 
                    store.filterBy(getRecordFilter());
                }

                else if(locationVal =="All Locations" && entryVal != "All Status"){
                    filterLocation=false;
                    filterStatus=true;
                    store.filterBy(getRecordFilter());
                }

                else if(locationVal != "All Locations" && entryVal == "All Status"){
                      filterLocation=true;
                      filterStatus=false;
                      store.filterBy(getRecordFilter());
                }                                  
         };


          var getRecordFilter = function () {
                   f.push({
                        filter: function (record) {      
                                 return filterString( filterLocation ? #{cboLocation}.getDisplayValue() : "", "LocationName", record);
                        }
                    });
           
                    f.push({
                        filter: function (record) {          
                                return filterNumber(filterStatus ? #{cboAllStatus}.getDisplayValue() : "", "Status", record);
                        }
                    });

               /* f.push({
                    filter: function (record) {        
                                     
                        return filterDate(#{frmDateSearch}.getValue(), #{toDateSearch}.getValue(), "business_date", record);
                    }
                });*/
                
                var len = f.length;
                 
                return function (record) {
                    for (var i = 0; i < len; i++) {
                        if (!f[i].filter(record)) {
                            return false;
                        }
                    }
                    return true;
                };
            };


             var filterString = function (value, dataIndex, record) {
                var val = record.get(dataIndex);
                
                if (typeof val != "string") {
                    return value.length == 0;
                }
                
                return val.toLowerCase().indexOf(value.toLowerCase()) > -1;
            };
 
            var filterDate = function (value, value2, dataIndex, record) {
                var val = Ext.Date.clearTime(record.get(dataIndex), true).getTime();
 
                if (!Ext.isEmpty(value, false) && val < Ext.Date.clearTime(value, true).getTime()) {
                    return false;
                }
                if (!Ext.isEmpty(value2, false) && val > Ext.Date.clearTime(value2, true).getTime()) {
                    return false;
                }
                return true;
            };
 
            var filterNumber = function (value, dataIndex, record) {
                var val = record.get(dataIndex);                
 
                if (!Ext.isEmpty(value, false) && val != value) {
                    return false;
                }
                
                return true;
            };

            var clearFilter = function () {
            
                #{cboLocation}.reset();
                #{cboAllStatus}.reset();
                     
                #{strGLDetail}.clearFilter();
              
            }

            //FILTER DATA SUMMARY READER---
             var applyDateFilter= function (){
             try{
              clearFilter(null);
              Ext.getCmp('cboLocation').disable(); 
              Ext.getCmp('cboAllStatus').disable();
              Ext.getCmp('btnClearSearch').disable();
              var frmDate=#{frmDateSearch}.getValue();
              var toDate=#{toDateSearch}.getValue();

                     
                     if(frmDate!="" && toDate != "" && frmDate!=null && toDate != null){
                              LoadIcon();
                              Ext.Ajax.request 
                                                ({ 
                                                    url: '/GLData/FilterGLEntriesByDate/', 
                                                    method: 'POST', 
                                                    type:'Load',   
                                                    params: {
                                                        frmDate : frmDate, 
                                                        toDate: toDate,                                                        
                                                    },       
                                                    success: function(response) 
                                                    {                                     
                                                        successFilter(response);
                                                    }, 
                                                    failure: function(response) 
                                                    { 
                                              
                                                    }          
                                });   
                                }
                 }
                    catch (e) {
                        
                         Ext.Msg.alert("Submit", "Your session has timed out.");
                         window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                       
                    }
                        
             }


         // EXPORT GRID TO CSV, XCEL, XML
         var submitValue = function (grid, hiddenFormat, format) {
             var data=#{JsonGLEntries}.getValue();
             JSONToCSVConvertor(data, "JE Revenue", true);

         };

           function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
            var CSV = '';    
            //Set Report title in first row or line
    
            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";
        
                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {
            
                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);
        
                //append Label row with line break
                CSV += row + '\r\n';
            }
    
            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";
        
                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);
        
                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {        
                alert("Invalid data");
                return;
            }   
    
            //Generate a file name
            var fileName = "";//"MyReport_";
             //this will remove the blank-spaces from the title and replace it with an underscore
              fileName += ReportTitle.replace(/ /g,"_");  
             if(msieversion()){
                    var IEwindow = window.open();
                    IEwindow.document.write('sep=,\r\n' + CSV);
                    IEwindow.document.close();
                    IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
                    IEwindow.close();
                } 
                else
                {
                    //Initialize file format you want csv or xls
                    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
                    // Now the little tricky part.
                    // you can use either>> window.open(uri);
                    // but this will not work in some browsers
                    // or you will not get the correct file extension    
    
                    //this trick will generate a temp <a /> tag
                    var link = document.createElement("a");    
                    link.href = uri;
    
                    //set the visibility hidden so it will not effect on your web-layout
                    link.style = "visibility:hidden";
                    link.download = fileName + ".csv";
    
                    //this part will append the anchor tag and remove it after automatic click
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
             }
        }
         
         function msieversion() {
                  var ua = window.navigator.userAgent;
                  var msie = ua.indexOf("MSIE ");
                  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return true
                  {
                    return true;
                  } else { // If another browser,
                  return false;
                  }
                  return false;
        }
    
    var selectDefaultValues= function(){

        
             var cboLocationInternalID= Ext.getCmp('cboLocationInternalID');  
             Ext.getCmp('cboLocationInternalID').setValue(cboLocationInternalID.getStore().getAt(0));
             
             var cboLocationName= Ext.getCmp('cboLocationName');
             Ext.getCmp('cboLocationInternalID').setValue(cboLocationName.getStore().getAt(0));

             var cboSubsidiary= Ext.getCmp('cboSubsidiary');
             Ext.getCmp('cboLocationInternalID').setValue(cboSubsidiary.getStore().getAt(0));

             var cboCurrency= Ext.getCmp('cboCurrency');
             Ext.getCmp('cboLocationInternalID').setValue(cboSubsidiary.getStore().getAt(0));
             
    };
      var commandHandler = function (cmd, record) {
       var records = #{grdGLDetail}.selModel.getSelection();
            switch (cmd) {
                case "edit":
                   if(records.length > 0)
                   {
                         Ext.Ajax.request 
                                ({ 
                                    url: '/GLData/storeSelectedJE/', 
                                    method: 'POST',    
                                    params: {
                                        JE_Id : record.get('Id'), 
                                        isNewJE : 'false'                                        
                                    },       
                                    success: function(response) 
                                    {                                     
                                         var winUDW = Ext.getCmp('JournalEntryWindow');
                                        winUDW.getLoader().baseParams = { Id: record.get('Id'), IsNewJE: false };
                                        winUDW.setTitle('Edit Journal Entry'); //added
                                        winUDW.getLoader().load();
                                        winUDW.show();
                                    }, 
                                    failure: function(response) 
                                    { 
                                       alert('Error: ' + response);
                                    } 
         
                                });  
                    }
                    else{
                      alert('Please select an item!');
                    }                            
                    break;               
               case "new":
                 Ext.Ajax.request 
                                ({ 
                                    url: '/GLData/storeSelectedJE/', 
                                    method: 'POST',    
                                    params: {
                                        JE_Id : '', 
                                        isNewJE : 'true'                                        
                                    },       
                                    success: function(response) 
                                    {                                     
                                         var winNUW = Ext.getCmp('JournalEntryWindow');
                                            winNUW.setTitle('New Item');
                                            winNUW.getLoader().load();
                                            winNUW.show();
                                    }, 
                                    failure: function(response) 
                                    { 
                                       alert('Error: ' + response);
                                    }          
                                });                          
                    break;
            }
            
        }

        function reloadGridPanel() {
            var winUDW = Ext.getCmp('JournalEntryWindow');
            winUDW.hide();
           reloadGrdGLDetail();
        } 
        
        function reloadGrdGLDetail(){
        //UPDATE STATUS
         Ext.Ajax.request 
                                ({ 
                                    url: '/GLData/GetUpdatedEntriesJS/', 
                                    method: 'POST',    
                                    params: {
                                        ClosedOrderDates : #{hdnClosedOrderDates}.getValue(), 
                                        LocationIDs : #{hdnLocationIds}.getValue(),                                        
                                        LocationInternalIds: #{hdnLocationInternalIDs}.getValue(),
                                        Status : #{hdnStatus}.getValue(),
                                        StartDate: #{hdnStartDate}.getValue(),
                                        EndDate: #{hdnEndDate}.getValue()
                                    },       
                                    success: function(response) 
                                    {              
                                    var str=response.responseText;
                                    var n = str.indexOf("'unbalanced'");                       
                                         if(n == -1){
                                          Ext.getCmp('btnPostJE').enable();
                                          
                                         }
                                         else{
                                           Ext.getCmp('btnPostJE').disable();
                                         }
                                    }, 
                                     failure: function(response) 
                                     { 
                                          HideLoadIcon();
                                     } 
         
                                });

       //UPDATE GRID
                             Ext.Ajax.request 
                                ({ 
                                    url: '/GLData/GetGLDetailReaderJS/', 
                                    method: 'POST',    
                                    params: {
                                        ClosedOrderDates : #{hdnClosedOrderDates}.getValue(), 
                                        LocationIDs : #{hdnLocationIds}.getValue(),                                        
                                        LocationInternalIds: #{hdnLocationInternalIDs}.getValue(),
                                        Entries : #{hdnStatus}.getValue(),
                                        StartDate: #{hdnStartDate}.getValue(),
                                        EndDate: #{hdnEndDate}.getValue()
                                    },       
                                    success: function(response) 
                                    {                                     
                                         successFilter(response);
                                    }, 
                                     failure: function(response) 
                                     { 
                                          HideLoadIcon();
                                     } 
         
                                });
        }  

        //POST JE ENTRY-----
           var PostEntry={
           
                submitData: function (source) {
                      try {
                      LoadPostIcon();
                                var LocationInternalId= #{hdnLocationInternalIDs}.getValue();
                                var ClosedOrderDates= #{hdnClosedOrderDates}.getValue();
                                var frmDate=getFormattedDate= #{hdnStartDate}.getValue();
                                var toDate=getFormattedDate= #{hdnEndDate}.getValue();
                               
                                                Ext.Ajax.request 
                                                ({ 
                                                    url: '/GLData/PostGLEntry/', 
                                                    method: 'POST',    
                                                    params: {
                                                    ClosedOrderDates : ClosedOrderDates, 
                                                    LocationInternalIds : LocationInternalId,
                                                    StartDate : frmDate,
                                                    EndDate : toDate
                                                    },       
                                                    success: function(response) 
                                                    {          
                                                     var params= Ext.JSON.decode(response.responseText);  
                                                                              
                                                       if(params.extraParamsResponse.messageSuccess=="Success!"){
                                                            Ext.Msg.alert("Submit", "Operation was successful");
                                                       }
                                                       else{
                                                         Ext.Msg.alert("Submit", params.extraParamsResponse.messageSuccess);
                                                        }
                                                          HidePostIcon();
                                                          LoadIcon1();
                                                          reloadGrdGLDetail();
                                                    }, 
                                                    failure: function(response) 
                                                    { 
                                                       HidePostIcon();
                                                       Ext.Msg.alert("Submit", "Your session has timed out.");
                                                       window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                                              
                                                       // Ext.Msg.alert("Submit", "Operation failed due to an Internal Error!");
                                                        
                                                    } 
         
                                                }); 
                                        
                                
                    }
                    catch (e) {
                        
                         Ext.Msg.alert("Submit", "Your session has timed out.");
                         window.top.location.href = "https://works.somidata.com/Account/ClientLogIn"; 
                       
                    }
                 }
                   
              };

               //CHECK ITEM Status WHEN SELECT HANDLER IS TRIGGERED======
                    //This will disable or enable Edit button at the top right side of the grid depending on the Status.
                   
                    var checkStatus = function () {                  
                            var shouldNotEdit=false;               
                            source= App.grdGLDetail;
                        
                        if(source.selModel.hasSelection()){
                           var records = source.selModel.getSelection();
                            if(records.length > 0){
                                for (var i = 0; i < records.length; i++){
                
                                   if(records[i].data.Status == "posted"){// || records[i].data.Entries == "unbalanced"){
                                     shouldNotEdit=true;
                                   }
                                   
                                }
                             }   

                          if(shouldNotEdit){
                            
                              Ext.getCmp('btnEdit').disable();
                          }              
                          else{
                             
                               Ext.getCmp('btnEdit').enable();
                          }                         
                     
                      }
                      else{
                     
                         Ext.getCmp('btnEdit').disable();
                      }
                        
                     };  

           
         function LoadIcon() {
             $("#loadingmessage").show();
         }
          function LoadIcon1() {
             $("#loadingmessage1").show();
         }

         function HideLoadIcon() {
             $("#loadingmessage").hide();
              $("#loadingmessage1").hide();
         }

          function LoadPostIcon() {
             $("#postingMessage").show();
         }

         function HidePostIcon() {
             $("#postingMessage").hide();
         }
    </script>
    </ext:XScript>

</head>
<body>
   <form id="Form1" runat="server">
     
      <ext:ResourceManager ID="ResourceManager1" runat="server" Theme="Gray"  DirectMethodNamespace="Transactions" />
        <ext:Viewport ID="Viewport1" runat="server" Layout="FitLayout" >               
            <Items>
                <ext:GridPanel   
                    ID="grdGLDetail"                  
                    runat="server"
                    Region="Center"
                    ColumnWidth="1"
                    ColumnHeight="2"
                    Border="true"
                    Flex="2"
                    Height="590"
                    MarginSpec="0 5 0 5"
                    Frame="false">
                    <Store>
                        <ext:Store ID="strGLDetail" runat="server">
                            <Model>
                                <ext:Model ID="mdlGLDetail" runat="server" Name="JournalEntry" >
                                    <Fields>
                                        <ext:ModelField Name="Id" />
                                        <ext:ModelField Name="Subsidiary" />
                                        <ext:ModelField Name="ClosedOrderDate" />
                                        <ext:ModelField Name="Posting_Period"/>
                                        <ext:ModelField Name="gl_currency" />                                    
                                        <ext:ModelField Name="gl_account"  />
                                        <ext:ModelField Name="gl_account_name"  />
                                        <ext:ModelField Name="DEBIT"  />
                                        <ext:ModelField Name="CREDIT"  />
                                        <ext:ModelField Name="Memo"  />
                                        <ext:ModelField Name="Class"  />
                                        <%--<ext:ModelField Name="Location"  />--%>
                                        <ext:ModelField Name="LocationInternalID"  />
                                        <ext:ModelField Name="Status" />
                                        <ext:ModelField Name="LocationID" Type="Int" />
                                        <ext:ModelField Name="LocationName"  />
                                    </Fields>
                                </ext:Model>
                            </Model>                           
                        </ext:Store>
                    </Store>
                  
                    <TopBar>
                        <ext:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                         <ext:Label ID="Label1" runat="server" Text="SEARCH BY " MarginSpec="0 0 5 10"  StyleSpec="font-weight: bold" Hidden="true"></ext:Label> 
                                         <ext:DateField ID="frmDateSearch" Name="frmDate" runat="server" FieldLabel="From"  MarginSpec="0 0 5 10" LabelWidth="40" Width="180" Hidden="true">
                                         </ext:DateField>
                                         <ext:DateField ID="toDateSearch" Name="toDate" runat="server" FieldLabel="To"  MarginSpec="0 0 5 10" LabelWidth="40" Width="180" Hidden="true" >
                                         </ext:DateField>
                                         <ext:Button ID="btnSearch" runat="server"  Text="Search" MarginSpec="0 0 0 5" Icon="Magnifier" StyleSpec="background-color:#f9f9f9;background-image:none;" Hidden="true">                                             
                                            <Listeners>
                                                    <Click Handler="applyDateFilter()" />
                                            </Listeners>
                                         </ext:Button>  

                                           <ext:Label ID="Label2" runat="server" Text="FILTER BY " MarginSpec="0 0 5 10"  StyleSpec="font-weight: bold"></ext:Label> 
                                         <ext:SelectBox ID="cboLocation" Name="location" runat="server" FieldLabel="Location" EmptyText="All Locations"  MarginSpec="0 0 5 10" LabelWidth="50"  Width="200"  >
                                            <Listeners>
                                                    <Select Handler="applyFilter()" />
                                            </Listeners>
                                         </ext:SelectBox>
                                         <ext:SelectBox ID="cboAllStatus" Name="allStatus" runat="server" FieldLabel="Status" EmptyText="All Status" MarginSpec="0 0 5 10" LabelWidth="40"  Width="180"  >
                                         <Listeners>
                                                    <Select Handler="applyFilter()" />
                                            </Listeners>
                                         </ext:SelectBox>
                                         <ext:Hidden ID="hdnLocationInternalId" runat="server" Text="" />
                                      
                                         <ext:Button ID="btnClearSearch" runat="server"  Text="Clear Filter" MarginSpec="0 0 0 5" Icon="Cancel" StyleSpec="background-color:#f9f9f9;background-image:none;" >                                             
                                            <Listeners>
                                                    <Click Handler="clearFilter(null)"  />
                                            </Listeners>
                                         </ext:Button>  
                                          <ext:ToolbarFill ID="ToolbarFill1" runat="server" />
                                         <ext:Button ID="btnInsert" runat="server" Text="Add new record" Icon="Add" Disabled="true" Hidden="true">
                                                <Listeners>
                                                  <Click Handler="commandHandler('new', #{grdGLDetail}.getSelectionModel().getLastSelected());" />
                                                </Listeners>
                                        </ext:Button>
                                        <ext:Button ID="btnEdit" runat="server" Text="Edit" Icon="ApplicationEdit" Disabled="true"  Hidden="true">
                                                <Listeners>
                                                 <Click Handler="commandHandler('edit', #{grdGLDetail}.getSelectionModel().getLastSelected());" />
                              
                                                </Listeners>
                                        </ext:Button>
                                        <ext:Button ID="btnPostJE" runat="server" Text="Post JE" icon="ApplicationGet" Disabled="true" Hidden="true">
                                                 <Listeners>
                                                     <Click Handler="PostEntry.submitData();" />
                                                 </Listeners> 
                                            </ext:Button>
                        
                                        <ext:Button ID="btnToCSV" runat="server" Text="Export to CSV" Icon="PageAttach">
                                            <Listeners>
                                                <Click Handler="submitValue(#{grdGLDetail}, #{FormatType}, 'csv');" />
                                            </Listeners>
                                        </ext:Button>
                                </Items>
                        </ext:Toolbar>
                    </TopBar>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Width="30" />
                            <ext:Column ID="Column1" ColumnID="Id" runat="server" Text="Id" Flex="1" DataIndex="Id" Hidden="true" />
                            <ext:Column ID="Column2" ColumnID="LocationId" runat="server" Text="locationId" Flex="1" DataIndex="LocationID" Hidden="true" />
                            <ext:Column ID="Column3" ColumnID="LocationInternalId" runat="server" Text="Location Internal ID" Width="130" DataIndex="LocationInternalID" />
                            <ext:Column ID="Column4" ColumnID="LocationName" runat="server" Text="Location" Flex="1" DataIndex="LocationName" />
                            <ext:Column ID="Column5" ColumnID="Subsidiary" runat="server" Text="Subsidiary" Flex="1" DataIndex="Subsidiary" />
                            <ext:DateColumn ID="DateColumn1" ColumnID="ClosedOrderDate" runat="server" Text="Date" Flex="1"   DataIndex="ClosedOrderDate" />
                            <ext:Column ID="Column6" ColumnID="PostingPeriod" runat="server" Text="Posting Period" Flex="1" DataIndex="Posting_Period" />
                            <ext:Column ID="Column7" ColumnID="Currency" runat="server" Text="Currency" Flex="1" DataIndex="gl_currency" />
                            <ext:Column ID="Column8" ColumnID="AccountNo" runat="server" Text="Account No" Flex="1" DataIndex="gl_account" />
                            <ext:Column ID="Column9" ColumnID="AccountName" runat="server" Text="Account Name" Flex="1" DataIndex="gl_account_name" />
                            <ext:Column ID="Column10" ColumnID="Debit" runat="server" Text="Debit" Flex="1" DataIndex="DEBIT" />
                            <ext:Column ID="Column11" ColumnID="Credit" runat="server" Text="Credit" Flex="1" DataIndex="CREDIT" />
                            <ext:Column ID="Column12" ColumnID="Memo" runat="server" Text="Memo" Flex="1" DataIndex="Memo" />
                            <ext:Column ID="Column13" ColumnID="Class"  runat="server" Text="Class" Flex="1" DataIndex="Class" />
                            <ext:Column ColumnID="Status" ID="colStatus" runat="server" Text="Status" DataIndex="Status"   >
                              <Renderer Fn="fontColorChangeIfPosted" />
                            </ext:Column>                                                   
                        </Columns>
                    </ColumnModel>
                   <%-- <SelectionModel>
                                <ext:CheckboxSelectionModel ID="CheckboxSelectionModel1" runat="server" Mode="Multi" >                                   
                                    <Listeners>
                                    <SelectionChange Handler="selected.length && #{TransactionTpl}.overwrite(#{DetailPanel}.body, selected[0].data);" />
                                    </Listeners>
                                </ext:CheckboxSelectionModel>                             
                      </SelectionModel>--%>
                    <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" Mode="Multi" />
                    </SelectionModel>
                    <Listeners>
                       <SelectionChange Handler="checkStatus();selected.length && #{TransactionTpl}.overwrite(#{DetailPanel}.body, selected[0].data);" />
                   </Listeners>
                    
               
                      
                </ext:GridPanel>     
            </Items>   
                                
        </ext:Viewport>
         <ext:Hidden ID="JsonGLEntries" runat="server" />
         <ext:Hidden ID="hdnClosedOrderDates" runat="server" />
         <ext:Hidden ID="hdnLocationIds" runat="server" />
         <ext:Hidden ID="hdnLocationInternalIDs" runat="server" />
         <ext:Hidden ID="hdnEntries" runat="server" />
         <ext:Hidden ID="hdnStatus" runat="server" />
         <ext:Hidden ID="hdnStartDate" runat="server" />
         <ext:Hidden ID="hdnEndDate" runat="server" />
         <ext:Hidden ID="LoginUserRole_For_DryBar" runat="server" />
         <ext:Hidden ID="hdnSessionId" runat="server" />
        <div id="loadingmessage" style="display:none;" >
            <div id="spinner" style=" text-align:center">
            <label id = "Label3" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">Searching <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
        <div id="loadingmessage1" style="display:none;" >
            <div id="spinner1" style=" text-align:center">
            <label id = "Label4" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">loading <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
   
        <div id="postingMessage" style="display:none;" >
            <div id="spinnerPosting" style=" text-align:center">
            <label id = "Label7" style="color:#585858; margin-left:auto; margin-right:auto; font-weight:bolder; font-size:1.90em; font-family:Calibri">Posting <img src="../../Resources/ajax-loader.gif" alt="loadingmessage" /></label>
            </div>
        </div>
   </form>

    <ext:Window
        ID="JournalEntryWindow" 
        runat="server" 
        Icon="ApplicationEdit"
        Width="340" 
        Height="450" 
        Hidden="true" 
        Modal="true"
        Constrain="true">
        <Loader ID="Loader1" 
            runat="server"
            Url="/GLUploadSystem/JournalEntry" 
            Mode="Frame"
            TriggerEvent="show" 
            ReloadOnEvent="true" 
            ShowMask="false" 
            MaskMsg="Loading Journal Entry Form..." >
         <Params>
                <ext:Parameter Name="Id" Value="" Mode="Value" />
                <ext:Parameter Name="IsNewJE" Value="" Mode="Value" />
            </Params>
        </Loader>
    </ext:Window>


   
</body>
</html>