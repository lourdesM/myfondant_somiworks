﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="Ext.Net.Utilities" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ Register Src="Feed.ascx" TagName="Feed" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {                
                this.StoreLocations.Parameters.Add(new Ext.Net.Parameter { Name = "UserId", Value = this.ViewData["UserId"].ToString() /*Profile.UserId.ToString()*/, Mode=ParameterMode.Raw });
                this.ucFeedWeather.ZipCode = Profile.DefaultZipCode;
                this.ucFeedEvents.ZipCode = Profile.DefaultZipCode;

                //added for testing
             //**   this.StoreLocations.Parameters.Add(new Ext.Net.Parameter { Name = "UserId", Value = "50200", Mode = ParameterMode.Raw });
                //this.ucFeedWeather.ZipCode = "90810";
                //this.ucFeedEvents.ZipCode = "90810";
                //end of added
            }

           /* if (!X.IsAjaxRequest)
            {
                string text ="";

                this.ResourceManager1.RegisterClientScriptBlock("text", string.Format("var text=\"{0}\";", text));

                foreach (Portlet portlet in ControlUtils.FindControls<Portlet>(this.Page))
                {
                    if (!portlet.ID.Equals("nPortlet1"))
                    {
                        portlet.Html = "={text}";
                        portlet.BodyPadding = 5;
                    }
                }
            }*/
        }
    </script>
    <script language="javascript" type="text/javascript">
       /* *** var changeLocation = function(zip) {
            Ext.net.DirectMethods.ucFeedWeather.ChangeLocation(zip);
            Ext.net.DirectMethods.ucFeedEvents.ChangeLocation(zip);
        }*/
    </script>
</head>
<body>
      <ext:ResourceManager ID="ResourceManager1" runat="server" />
                    <ext:Panel ID="PanelHome" runat="server" Border="false" Layout="Fit">                                     
                       <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server" Flat="true" StyleSpec="margin: 3px 18px 0 0;">
                                    <Items>
                                        <ext:ToolbarFill />
                                        <ext:ComboBox ID="ComboBoxLocation" runat="server" Mode="Local" ForceSelection="true" Editable="false" FieldLabel="ChangeLocation" DisplayField="LocationName" ValueField="ZipCode" Width="300" SelectedIndex="0">
                                            <Store>
                                                <ext:Store runat="server" ID="StoreLocations" >
                                                    <Proxy>
                                                        <ext:AjaxProxy Url="/Data/GetUserLocations/" >
                                                            <Reader>
                                                            <ext:JsonReader Root="data" TotalProperty="TotalRecords"/>                                                         
                                                            </Reader>
                                                        </ext:AjaxProxy>
                                                    </Proxy>
                                                    <Model>
                                                        <ext:Model ID="Model1" runat="server">
                                                            <Fields>
                                                                <ext:ModelField Name="LocationName" /> 
                                                                <ext:ModelField Name="ZipCode" />
                                                            </Fields>
                                                        </ext:Model>                                                        
                                                    </Model>                                                    
                                                </ext:Store>
                                            </Store>
                                           <%-- *** <Listeners>
                                                <Select Handler="changeLocation(this.getValue());" />
                                            </Listeners>--%>
                                        </ext:ComboBox>
                                    </Items>
                                </ext:Toolbar>
                                </TopBar>
                        <Items>                     
                            <ext:Portal 
                                ID="Portal1" 
                                runat="server" 
                                Border="false" 
                                BodyStyle="background-color: transparent;">                        
                                <Items>                               
                                    <ext:PortalColumn ID="PortalColumn1" 
                                        runat="server" 
                                        ColumnWidth=".67">
                                        <Items>
                                            <ext:Portlet 
                                                ID="Portlet6" 
                                                runat="server"
                                                Title="Your Dashboard" 
                                                Height="300"
                                                BodyPadding="0" 
                                                Icon="ChartPie">
                                               <%-- <Loader ID="Loader1" runat="server" Url="/Home/Dashboard.aspx" Mode="Frame">  
                                                    <LoadMask ShowMask="true" />                                                    
                                                </Loader> --%>
                                            </ext:Portlet>
                                            <ext:Portlet ID="Portlet2" Title="Weather Near You" runat="server" Icon="Feed">
                                                <Content>
                                                    <uc1:Feed ID="ucFeedWeather" runat="server" LoadFeedUrl="FeedWeatherUrl" UseZipCode="true" />
                                                </Content>
                                            </ext:Portlet>
                                        </Items>
                                    </ext:PortalColumn>
                                    <ext:PortalColumn ID="PortalColumn2" 
                                        runat="server"
                                        ColumnWidth=".31" 
                                        StyleSpec="padding:10px 0 10px 0px">
                                        <Items>
                                             <ext:Portlet ID="Portlet5" Title="Your Alerts" runat="server" Icon="Bell" >
                                                 <Content>
                                                    <h2>Your alerts will show here.</h2>
                                                 </Content>
                                             </ext:Portlet>
                                             <ext:Portlet ID="Portlet4" Title="National Restaurant News" runat="server" Icon="Feed" >
                                                <Content>
                                                    <uc1:Feed ID="ucFeedNRA" runat="server" LoadFeedUrl="FeedNRAUrl" UseZipCode="false" />
                                                </Content>
                                             </ext:Portlet>
                                             <ext:Portlet ID="Portlet3" Title="Upcoming Events Near You" Icon="Feed" runat="server">
                                                 <Content>
                                                    <uc1:Feed ID="ucFeedEvents" runat="server" LoadFeedUrl="FeedEventsUrl" UseZipCode="true" />
                                                 </Content>
                                             </ext:Portlet>
                                        </Items>
                                    </ext:PortalColumn>                                   
                                </Items>
                            </ext:Portal>
                        </Items>
                    </ext:Panel>                                                 
</body>
</html>
