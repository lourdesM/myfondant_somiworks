﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<%@Import Namespace="Fondant.myFondant.Web.Code" %> <%--added--%>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script runat="server">
        private class UserDetail
        {
            public string ClientID { get; set; }
            public string ASPNETUserID { get; set; }
            public string FirstName{ get; set; }
            public string MiddleName{ get; set; }
            public string LastName{ get; set; }
            public string Suffix { get; set; }
            public string CreatedDate { get; set; }
            public string ModifiedDate { get; set; }
            public string IsActive { get; set; }
            public string Email{ get; set; }
            public string ClientName { get; set; }
           
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewData["UserId"] = System.Web.HttpContext.Current.Application["viewdata_UserId"] != "" ? System.Web.HttpContext.Current.Application["viewdata_UserId"] : ViewData["UserId"];
            ViewData["ASPNETUserId"] = System.Web.HttpContext.Current.Application["viewdata_ASPNETUserId"] != "" ? System.Web.HttpContext.Current.Application["viewdata_ASPNETUserId"] : ViewData["ASPNETUserId"];
            ViewData["IsNewUser"] = System.Web.HttpContext.Current.Application["viewdata_IsNewUser"] != "" ? System.Web.HttpContext.Current.Application["viewdata_IsNewUser"] : ViewData["IsNewUser"];
           // ViewData["LoginUserID"] = System.Web.HttpContext.Current.Application["LoginUserID"] != "" ? System.Web.HttpContext.Current.Application["LoginUserID"] : ViewData["LoginUserID"];
            ViewData["LoginUserID"] = System.Web.HttpContext.Current.Application["LoginUserID"] != "" ? System.Web.HttpContext.Current.Application["LoginUserID"] : ViewData["LoginUserID"];
            ViewData["LoginASPNETUserId"]=System.Web.HttpContext.Current.Application["LoginASPNETUserId"];
            ViewData["LoginUserRole"] = System.Web.HttpContext.Current.Application["LoginUserRole"];
            ViewData["LoginUserClientID"] = System.Web.HttpContext.Current.Application["LoginUserClientID"];
          
        
            if (ViewData["UserId"] != null && ViewData["UserId"] != "")
            {
                
                this.txtIsNewUser.Text = ViewData["IsNewUser"].ToString();
               
                this.txtUserId.Text = ViewData["UserId"].ToString(); //temporary
                this.txtASPNETUserID.Text = ViewData["ASPNETUserId"].ToString();
                
                Fondant.myFondant.Web.Controllers.DataController datacontroller = new Fondant.myFondant.Web.Controllers.DataController();

                //GETS THE LIST OF USER ROLES
                Object temp = datacontroller.UserRolesGet(ViewData["ASPNETUserId"].ToString()).Data;//ViewData["LoginASPNETUserId"].ToString()).Data;
                this.dsUserRoles.DataSource = temp;
                this.dsUserRoles.DataBind();

                //GETS THE LIST OF USER LOCATIONS                    
                temp = datacontroller.UserLocationsGet(ViewData["ASPNETUserId"].ToString()).Data;//ViewData["LoginASPNETUserId"].ToString()).Data;
                this.dsUserLocations.DataSource = temp;
                this.dsUserLocations.DataBind();

                //GETS THE LIST OF SSRS REPORTS
                temp = datacontroller.UserInSSRSReportsGet(ViewData["ASPNETUserId"].ToString()).Data;
                this.dsUserReports.DataSource = temp;
                this.dsUserReports.DataBind();

                temp = datacontroller.GetUser(ViewData["ASPNETUserId"].ToString()).Data;
                
               var jsonUserDetail = new JavaScriptSerializer().Serialize(temp);
                JavaScriptSerializer j = new JavaScriptSerializer();
                var userDetail = j.Deserialize<UserDetail[]>(jsonUserDetail);


                //  this.dsUser.DataSource = temp;
                // this.dsUser.DataBind();
                
                
               foreach (UserDetail i in userDetail)
               {
                   
                   this.txtClientID.Text = i.ClientID != null ? i.ClientID : "" ;
                   this.txtFirstName.Text = i.FirstName != null ? i.FirstName : "";
                   this.txtMiddleName.Text = i.MiddleName != null ? i.MiddleName : "";
                   this.txtLastName.Text = i.LastName != null ? i.LastName : "";
                   this.txtSuffix.Text = i.Suffix != null ? i.Suffix : "";
                   this.txtEmail.Text = i.Email != null ? i.Email : "";
                   this.txtIsActive.Text = i.IsActive != null ? i.IsActive : "";
                   this.txtClientName.Text = i.ClientName != null ? i.ClientName : "";
               } 
                
              
               
            }
            System.Web.HttpContext.Current.Application["viewdata_UserId"]="";
            System.Web.HttpContext.Current.Application["viewdata_IsNewUser"]="";
            System.Web.HttpContext.Current.Application["viewdata_ASPNETUserId"] = "";
        }

        protected void vwUserDetail(string command) 
        {
            Fondant.myFondant.Web.Controllers.DataController datacontroller = new Fondant.myFondant.Web.Controllers.DataController();
            Object temp = datacontroller.UserRolesGet(Profile.GetProfile("50200_admin").ASPNETUserId.ToString()).Data;
            this.dsUserRoles.DataSource = temp;
            this.dsUserRoles.DataBind();
        }
    </script>
   <ext:XScript ID="XScript2" runat="server">
    <script type="text/javascript">
     var _saveUserRolesIsSuccessful=true;
     var  _saveUserLocationsIsSuccessful=true;
     var _saveUserSSRSReportsIsSuccessful=true;
     var _form;
     var _action;


        var userLoaded = function (store, records) {
            if (records.length > 0) {
                Ext.getCmp('UserDetailForm').loadRecord(records[0]); //original

            }
            else {
                Ext.getCmp('UserDetailForm').reset();

            }

            Ext.getCmp('UserDetailForm').el.unmask();

        }
        var failureHandler = function (form, action) {
          
             var msg = '';
            if (action.failureType == "client" || (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Please check for required fields.";
            } else if (action.result && action.result.msg) {
                msg = action.result.msg;
                
            } else if (action.response) {
                var params = Ext.JSON.decode(action.response.responseText);
                msg = params.extraParams.msg;
                
            }
            alert("Failed! " + msg);
        }
      

        var successHandler = function (form, action) {
            _form=form;
            _action=action;
         
             saveUserRoles();
        }

        function saveUserRoles() {
         
                var records = #{UserRolesGridPanel}.getStore().getModifiedRecords(); //#{UserRolesGridPanel}.selModel.getSelection(); //#{grdGLUpload}.getRowsValues({selectedOnly:true});                    
              
                for (var i = 0; i < records.length; i++) {
                    Ext.Ajax.request({
                        url: '/Data/UserRolesSet1/',
                        method: 'POST',
                        params: {
                            UserId: #{txtASPNETUserID}.getValue(),//records[i].data.UserId,
                            RoleName: records[i].data.RoleName,
                            Selected: records[i].data.Selected

                        },
                        success: function (response) {
                            _saveUserRolesIsSuccessful=true;
                        
                        },
                        failure: function (response) {
                            _saveUserRolesIsSuccessful=false;
                            
                        }
                    });
                }
                saveUserLocations();
        }
        
        function saveUserLocations(){        
                 
                var recordsLoc = #{UsersLocationsGridPanel}.getStore().getModifiedRecords(); // #{UsersLocationsGridPanel}.getStore().getModifiedRecords()[0].data.LocationName #{UserRolesGridPanel}.selModel.getSelection(); //#{grdGLUpload}.getRowsValues({selectedOnly:true});                    
               
                for (var i = 0; i < recordsLoc.length; i++) {
                    Ext.Ajax.request({
                        url: '/Data/UserLocationsSet1/',
                        method: 'POST',
                        params: {
                            UserId: #{txtUserID}.getValue(), // recordsLoc[i].data.UserId,
                            LocationId: recordsLoc[i].data.LocationId,
                            LocationName: recordsLoc[i].data.LocationName,
                            Selected: recordsLoc[i].data.Selected

                        },
                        success: function (response) {
                        _saveUserLocationsIsSuccessful=true;
                        },
                        failure: function (response) {
                        _saveUserLocationsIsSuccessful=false;

                        }
                    });
                }
               
                  saveUserSSRSReports();
        }
   
        function saveUserSSRSReports(){
         
                var recordsLoc = #{SSRSReportsGridPanel}.getStore().getModifiedRecords(); 
               
                 for (var i = 0; i < recordsLoc.length; i++) {
                
                    Ext.Ajax.request({
                        url: '/Data/UserSSRSReportsSet/',
                        method: 'POST',
                        params: {
                            UserId: #{txtUserID}.getValue(), 
                            SSRS_ReportsId: recordsLoc[i].data.SSRS_ReportsId,
                            ReportName: recordsLoc[i].data.ReportName,
                            Selected: recordsLoc[i].data.Selected

                        },
                        success: function (response) {
                        _saveUserSSRSReportsIsSuccessful=true;
                        },
                        failure: function (response) {
                        _saveUserSSRSReportsIsSuccessful=false;

                        }
                    });
                }
               
                saveClose();
        }
        

        function saveClose() {
          if(_saveUserRolesIsSuccessful && _saveUserLocationsIsSuccessful){
           
              Ext.Msg.show({
                    title: 'Save',
                    msg: 'Save successful.',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    fn: reloadPage
                });
              
          }
          else{
                if(_saveUserLocationsIsSuccessful==false){
                    Ext.Msg.alert("Error", "An error occurred while saving the User Locations");
                 }

                 if(_saveUserRolesIsSuccessful==false){
                   Ext.Msg.alert("Error", "An error occurred while saving the User Roles");
                 }

                 if(_saveUserSSRSReportsIsSuccessful==false){
                   Ext.Msg.alert("Error", "An error occurred while saving the SSRS Reports");
                  }
          }
         
        }

        function reloadPage() {
              
                Ext.get('CancelButton').dom.click();
                parent.reloadGridPanel();
              
                /*customerChanged = true;
                 if (action.options.params.setNew) {

                        UserDetailForm.form.reset();
                        dsUserRoles.removeAll();
                        dsUserLocations.removeAll();
                        var rec = new dsUser.recordType();
                        rec.newRecord = true;
                        dsUser.add(rec);
                        //initUI(true);
                 }
                 else {
                        //initUI(false);
                 }*/
        }


        var getUserID = function () {
            return Ext.getStore('dsUser').getAt(0).get('ASPNETUserID');
        }

       
    </script>
    </ext:XScript>
  
     <script type="text/javascript">
         var pinEditors = function (btn, pressed) {
             var columnConfig = btn.column,
                column = columnConfig.column;

             if (pressed) {
                 column.pinOverComponent();
                 column.showComponent(columnConfig.record, true);
             } else {
                 column.unpinOverComponent();
                 column.hideComponent(true);
             }
         };
    </script>
    <style id="Style1" runat="server" type="text/css">
        .x-over-editor-grid tr.x-grid-row
        {
            height: 25px;
        }
    </style>   
</head>
<body>
  <ext:ResourceManager ID="ResourceManager1" runat="server" />
  <ext:ViewPort ID="ViewPort1" runat="server" Layout="Fit" Height="600" >
        <Items>

            <ext:FormPanel ID="UserDetailForm" runat="server" BodyStyle="background-color:#FFFFFF;padding:7px;" ButtonAlign="Right"
                 TitleCollapse="true" Url="/Data/SaveUser/" Height="600" StandardSubmit="false">
                <Items>                                          
          
                  <ext:FormPanel ID="FormPanel1" runat="server" Title="User Detail" Height="200" Width="575" >
                  <Items>
                     <ext:TextField ID="txtIsNewUser" runat="server" FieldLabel="IsNewUser" Hidden="true" />
                     <ext:TextField ID="txtUserId" runat="server" FieldLabel="UserId" Hidden="true" />
                     <ext:TextField ID="txtClientID" runat="server" FieldLabel="ClientID" Hidden="true" />   
                     <ext:TextField ID="txtClientName" runat="server" FieldLabel="ClientName" Hidden="true" />                       
                     <ext:TextField ID="txtASPNETUserID" runat="server" FieldLabel="ASPNetUserID"  Hidden="true"  />
                     <ext:TextField ID="txtFirstName" runat="server" FieldLabel="FirstName"   MarginSpec="5 5 5 5"  LabelWidth="100"  />
                     <ext:TextField ID="txtMiddleName" runat="server" FieldLabel="MiddleName"   MarginSpec="5 5 5 5"  LabelWidth="100"  />
                     <ext:TextField ID="txtLastName" runat="server" FieldLabel="LastName"   MarginSpec="5 5 5 5"  LabelWidth="100"  />
                     <ext:TextField ID="txtSuffix" runat="server" FieldLabel="Suffix"   MarginSpec="5 5 5 5"  LabelWidth="100"  />
                     <ext:TextField ID="txtEmail" runat="server" FieldLabel="Email"   MarginSpec="5 5 5 5"  LabelWidth="100"  />
                      <ext:ComboBox ID="txtIsActive" runat="server" FieldLabel="IsActive"   MarginSpec="5 5 5 5"  LabelWidth="100"   >
                          <Items>
                                 <ext:ListItem Text="true" Value="true" Mode="Raw" />
                                 <ext:ListItem Text="false" Value="false" Mode="Raw" />
                          </Items>
                      </ext:ComboBox>
                     
                  </Items>
                  </ext:FormPanel>
                    <ext:GridPanel ID="UsersLocationsGridPanel" runat="server" StripeRows="true" Title="User Locations" Height="100" AutoExpandColumn="LocationName" > <%--StoreID="dsUserLocations" --%>
                                              <Store>
                                              <ext:Store ID="dsUserLocations" runat="server" >
                                                  <Model>
                                                     <ext:Model ID="Model1" runat="server">
                                                         <Fields>
                                                             <ext:ModelField Name="UserId" />
                                                             <ext:ModelField Name="LocationId" />
                                                             <ext:ModelField Name="LocationName" />
                                                             <ext:ModelField Name="Selected" />
                                                         </Fields>
                                                     </ext:Model>
                                                 </Model>
                                              </ext:Store>
                                            </Store>
                                            <ColumnModel ID="ColumnModel3" runat="server" > 
                                                <Columns>     
                                                    <ext:RowNumbererColumn ID="RowNumbererColumn1" runat="server" Width="30" />                                              
                                                    <%-- <ext:Column ID="Column3" runat="server" ColumnID="UserId" DataIndex="UserId" Header="UserId" Width="150" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />--%>
                                                    <ext:Column ID="Column2" runat="server" Flex="1" ColumnID="LocationName" DataIndex="LocationName" Header="Location" Width="150" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                    <ext:CheckColumn ID="CheckColumn2" runat="server" Flex="1" DataIndex="Selected" Header="Select" Align="Center" Width="60" Editable="true" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                                </Columns>
                                            </ColumnModel>
                                           
                   </ext:GridPanel>
                  <ext:GridPanel ID="UserRolesGridPanel" runat="server" StripeRows="true" Height="168" Title="User Roles" AutoExpandColumn="RoleName" AutoScroll="true" >
                         <Store> 
                               <ext:Store ID="dsUserRoles" runat="server" > 
                                     <Model>
                                          <ext:Model ID="Model2" runat="server">
                                                     <Fields>
                                                         <ext:ModelField Name="UserId" />
                                                         <ext:ModelField Name="RoleName" />
                                                         <ext:ModelField Name="Selected" />                     
                                                     </Fields>
                                          </ext:Model>
                                     </Model>   
                               </ext:Store>
                          </Store>
                          <ColumnModel ID="ColumnModel2" runat="server">
                                   <Columns>
                                         <ext:RowNumbererColumn ID="RowNumbererColumn2" runat="server" Width="30" />
                                         <ext:Column ID="Column1" runat="server" ColumnID="RoleName" Flex="1" DataIndex="RoleName" Header="Role" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                         <ext:CheckColumn ID="CheckColumn1" runat="server" DataIndex="Selected" Flex="1" Header="Select" Align="Center" Width="60" Editable="true" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                   </Columns>
                         </ColumnModel>
                                           
                    </ext:GridPanel>
                  <ext:GridPanel ID="SSRSReportsGridPanel"  runat="server" StripeRows="true" Height="168" Title="SSRS Reports" AutoExpandColumn="ReportName" AutoScroll="true" >
                      <Store> 
                               <ext:Store ID="dsUserReports" runat="server" > 
                                     <Model>
                                          <ext:Model ID="Model3" runat="server">
                                                     <Fields>
                                                         <ext:ModelField Name="UserId" />
                                                         <ext:ModelField Name="SSRS_ReportsId" />
                                                         <ext:ModelField Name="ReportName" />
                                                         <ext:ModelField Name="Selected" />                     
                                                     </Fields>
                                          </ext:Model>
                                     </Model>   
                               </ext:Store>
                          </Store>
                          <ColumnModel ID="ColumnModel1" runat="server">
                                   <Columns>
                                         <ext:RowNumbererColumn ID="RowNumbererColumn3" runat="server" Width="30" />
                                         <ext:Column ID="Column3" runat="server" ColumnID="ReportName" Flex="1" DataIndex="ReportName" Header="Report" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                         <ext:CheckColumn ID="CheckColumn3" runat="server" DataIndex="Selected" Flex="1" Header="Select" Align="Center" Width="60" Editable="true" Hideable="false" Groupable="false" MenuDisabled="true" Resizable="false" Fixed="true" />
                                   </Columns>
                         </ColumnModel>
                     </ext:GridPanel>
                </Items>
                <Buttons>
                 <%--<ext:Button ID="Button2" runat="server"  Text="Test">
                        <Listeners>
                            <Click Handler="getGridRows()" />
                        </Listeners>
                    </ext:Button>--%>
                    <ext:Button ID="SaveButton" runat="server" Icon="Disk" Text="Save">
                        <Listeners>
                           <%--  <Click Handler="#{UserDetailForm}.form.submit({waitMsg:'Saving...', params:{id: getUserID(), IsNewUser: Ext.getStore('dsUser').getAt(0).get('IsNewUser'), ClientName: Ext.getStore('dsUser').getAt(0).get('ClientName'), ClientID: Ext.getStore('dsUser').getAt(0).get('ClientID'), ASPNETUserID: Ext.getStore('dsUser').getAt(0).get('ASPNETUserID'), FirstName: #{txtFirstName}.getValue(), MiddleName: #{txtMiddleName}.getValue(), LastName: #{txtLastName}.getValue(), Suffix: #{txtSuffix}.getValue(), Email: #{txtEmail}.getValue(), IsActive: #{txtIsActive}.getValue()}, success: successHandler, failure: failureHandler});" />--%>
                             <Click Handler="#{UserDetailForm}.form.submit({waitMsg:'Saving...', params:{id:#{txtASPNETUserID}.getValue(), IsNewUser: #{txtIsNewUser}.getValue(), ClientName: #{txtClientName}.getValue(), ClientID: #{txtClientID}.getValue(), ASPNETUserID: #{txtASPNETUserID}.getValue(), FirstName: #{txtFirstName}.getValue(), MiddleName: #{txtMiddleName}.getValue(), LastName: #{txtLastName}.getValue(), Suffix: #{txtSuffix}.getValue(), Email: #{txtEmail}.getValue(), IsActive: #{txtIsActive}.getValue()}, success: successHandler, failure: failureHandler});" /> <%--original--%>
                        </Listeners>
                    </ext:Button>
                    <ext:Button ID="CancelButton" runat="server" Icon="Cancel" Text="Cancel">
                        <Listeners>
                            <Click Handler="parentAutoLoadControl.hide();" />
                        </Listeners>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:ViewPort></body>
</html>
