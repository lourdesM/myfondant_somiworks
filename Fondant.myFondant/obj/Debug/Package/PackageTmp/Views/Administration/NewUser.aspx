﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<%@Import Namespace="Fondant.myFondant.Web.Code" %> <%--added--%>
<%@ Import Namespace="System.Web.Configuration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <script runat="server">    
        protected void Page_Load(object sender, EventArgs e)    
        {
            ViewData["LoginUserClientID"] = System.Web.HttpContext.Current.Application["LoginUserClientID"];
            this.ClientID.Value = ViewData["LoginUserClientID"];
        }
        [DirectMethod]
        public void LogUserInfo(string name, int count)
        {
            Fondant.myFondant.Web.Controllers.DataController data = new Fondant.myFondant.Web.Controllers.DataController();
            data.save_user();
            
        } //does not recognize app.direct
        
        protected void SaveClick(object sender, DirectEventArgs e)
        {
            int id = int.Parse(e.ExtraParams["recordId"]);

            Fondant.myFondant.Web.Controllers.DataController data = new Fondant.myFondant.Web.Controllers.DataController();
         
        }  
       
    </script>
    <script type="text/javascript">
        var failureHandler = function (form, action) {

            var msg = '';
            if (action.failureType == "client" || (action.result && action.result.errors && action.result.errors.length > 0)) {
                msg = "Please check for required fields.";
            } else if (action.result && action.result.msg) {
                msg = action.result.msg;
                
            } else if (action.response) {
                var params = Ext.JSON.decode(action.response.responseText);
                msg = params.extraParams.msg;
                
            }
            alert("Failed! " + msg);
           // bBarNextMsg.setStatus({ text: msg });
        }

        var successHandler = function (form, action) {
            
            if (action.result && action.result.extraParams && action.result.extraParams.newID) {
            
                parent.window.NewUserWindow.hideMode = "display";
                parent.nextUserDetail(action.result.extraParams.newID);
            }
        }

        function CancelButton_click() {
            Ext.get('CancelButton').dom.click();
        }

    </script>
</head>
<body>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    <ext:ViewPort ID="ViewPort1" runat="server" Layout="Fit">
        <Items>
            <ext:FormPanel ID="NewUserForm" runat="server" BodyStyle="background-color:#FFFFFF;padding:7px;" ButtonAlign="Right"
                 TitleCollapse="true" Url="/Data/SaveUser/" StandardSubmit="false" > <%--StandardSubmit="true" --%>
                <Items>
                    <ext:TextField ID="ClientID" DataIndex="ClientID" runat="server" Hidden="true" AnchorHorizontal="100%" />
                    <ext:TextField ID="UserName" DataIndex="UserName" runat="server" FieldLabel="Username" MaxLength="50" AllowBlank="false" BlankText="Username is required." AnchorHorizontal="100%" />
                    <ext:TextField ID="Password" DataIndex="Password" runat="server" FieldLabel="Password" MaxLength="50" InputType="Password" AllowBlank="false" BlankText="Password is required." AnchorHorizontal="100%" />
                    <ext:TextField ID="ConfirmPassword" DataIndex="ConfirmPassword" runat="server" FieldLabel="Confirm Password" MaxLength="50" InputType="Password" AllowBlank="false" BlankText="Confirm Password is required."
                         Note="Password must meet the minimum required length of 7 characters including 1 non-alpha." NoteCls="font-size:xx-small;" AnchorHorizontal="100%" />
                </Items>
                <BottomBar>
                    <ext:StatusBar ID="bBarNextMsg" runat="server" Flat="true" StyleSpec="padding-left: 7px; color: red;" />
                </BottomBar>
                <Buttons>
                    <ext:Button ID="NextButton" runat="server" Icon="NextGreen" Text="Next" > <%--for testing OnDirectClick="ButtonClick"--%>
                      <Listeners>
                            <Click Handler="#{NewUserForm}.form.submit({waitMsg:'Saving...', params:{setNew: true}, success: successHandler, failure: failureHandler});" />   
                      </Listeners>
                       
                    </ext:Button>
                    <ext:Button ID="CancelButton" runat="server" Icon="Cancel" Text="Cancel">
                        <Listeners>
                                <Click Handler="parentAutoLoadControl.hide();" />
                        </Listeners>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:ViewPort>
</body>
</html>
