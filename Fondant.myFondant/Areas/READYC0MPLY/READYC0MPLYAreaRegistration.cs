﻿using System.Web.Mvc;

namespace Fondant.myFondant.Web.Areas.READYC0MPLY
{
    public class READYC0MPLYAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "READYC0MPLY";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "READYC0MPLY_default",
                "READYC0MPLY/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
