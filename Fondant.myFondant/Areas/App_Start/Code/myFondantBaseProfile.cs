﻿using System;
using System.Web;
using System.Web.Profile;

namespace Fondant.myFondant.Web.Code
{
    public class myFondantBaseProfile : ProfileBase
    {
        //public string ClientAccountNumber { get; set; }
        //public int ClientId { get; set; }
        //public string ClientLongName { get; set; }
        //public string ClientShortName { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public int UserId { get; set; }
        //public string ASPNetUserId { get; set; }
        //public string FirstName { get; set; }
        //public bool IsActive { get; set; }
        //public string LastName { get; set; }
        //public string DefaultZipCode { get; set; }
        //public string Locations { get; set; }
        //public string MiddleName { get; set; }
        //public DateTime ModifiedDate { get; set; }
        //public string Suffix { get; set; }

        //added values for testing, remove if done
        public virtual int ClientId
        {
            get { return ((int)(this.GetPropertyValue("ClientId"))); }
            //get { return (2); }
            set { this.SetPropertyValue("ClientId", value); }
        }

        public string ClientAccountNumber
        {
            get { return ((string)(this.GetPropertyValue("ClientAccountNumber"))); }
            set { this.SetPropertyValue("ClientAccountNumber", value); }
            //get { return ("50200"); }
        }

        public virtual string ClientLongName
        {
            get { return ((string)(this.GetPropertyValue("ClientLongName"))); }
            set { this.SetPropertyValue("ClientLongName", value); }
            //get { return ("SBE Entertainment"); }
        }

        public string ClientShortName
        {
            get { return ((string)(this.GetPropertyValue("ClientShortName"))); }
            set { this.SetPropertyValue("ClientShortName", value); }
            //get { return ("SBE"); }
        }

        public virtual int UserId
        {
            get { return ((int)(this.GetPropertyValue("UserId"))); }
            set { this.SetPropertyValue("UserId", value); }
            //get { return (96); }
        }

        public string ASPNETUserId
        {
            get { return ((string)(this.GetPropertyValue("ASPNETUserID"))); }
            set { this.SetPropertyValue("ASPNETUserID", value); }
            //get { return ("59FE2D2A-9F93-401E-9279-0A9D13B461DB"); }
        }

        public string FirstName
        {
            get { return ((string)(this.GetPropertyValue("FirstName"))); }
            set { this.SetPropertyValue("FirstName", value); }
            //get { return ("SoMi Data"); }
        }

        public string MiddleName
        {
            get { return ((string)(this.GetPropertyValue("MiddleName"))); }
            set { this.SetPropertyValue("MiddleName", value); }
            //get { return (""); }
        }

        public string LastName
        {
            get { return ((string)(this.GetPropertyValue("LastName"))); }
            set { this.SetPropertyValue("LastName", value); }
            //get { return ("Administrator"); }
        }

        public string Suffix
        {
            get { return ((string)(this.GetPropertyValue("Suffix"))); }
            set { this.SetPropertyValue("Suffix", value); }
            //get { return (""); }
        }

        public string DefaultZipCode
        {
            get { return ((string)(this.GetPropertyValue("DefaultZipCode"))); }
            set { this.SetPropertyValue("DefaultZipCode", value); }
            //get { return ("90069"); }
        }

        public string Locations
        {
            get { return ((string)(this.GetPropertyValue("Locations"))); }
            set { this.SetPropertyValue("Locations", value); }
            //get { return ("Abbey"); }
        }

        public bool IsActive
        {
            get { return ((bool)(this.GetPropertyValue("IsActive"))); }
            set { this.SetPropertyValue("IsActive", value); }
            //get { return (true); }
        }

        public DateTime CreatedDate
        {
            get { return ((DateTime)(this.GetPropertyValue("CreatedDate"))); }
            set { this.SetPropertyValue("CreatedDate", value); }
            //get { return ((DateTime)((object)("2009-08-24 23:04:53.800"))); }
        }

        public DateTime ModifiedDate
        {
            get { return ((DateTime)(this.GetPropertyValue("ModifiedDate"))); }
            set { this.SetPropertyValue("ModifiedDate", value); }
            //get { return ((DateTime)((object)("2009-08-24 23:04:53.800"))); }
        }

        public virtual myFondantBaseProfile GetProfile(string username)
        {
            return ((myFondantBaseProfile)(ProfileBase.Create(username)));
            //return ((myFondantBaseProfile)(ProfileBase.Create("50200_admin")));
        }
    }
}
