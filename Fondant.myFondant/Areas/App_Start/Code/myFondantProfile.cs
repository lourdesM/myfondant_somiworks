﻿using System;
using System.Web;

namespace Fondant.myFondant.Web.Code
{
    public class myFondantProfile
    {
        private static myFondantBaseProfile Profile { get { return (myFondantBaseProfile)HttpContext.Current.Profile; } }
        //private static myFondantBaseProfile Profile { get { return (myFondantBaseProfile)System.Web.HttpContext.Current.Profile; } }

        public static string ClientAccountNumber() { return Profile.ClientAccountNumber; }
        public static int ClientId() { return Profile.ClientId; }
        public static string ClientLongName() { return Profile.ClientLongName; }
        public static string ClientShortName() { return Profile.ClientShortName; }
        public static DateTime CreatedDate() { return Profile.CreatedDate; }
        public static int UserId() { return Profile.UserId; }
        public static string ASPNETUserId() { return Profile.ASPNETUserId; }
        public static string FirstName() { return Profile.FirstName; }
        public static bool IsActive() { return Profile.IsActive; }
        public static string LastName() { return Profile.LastName; }
        public static string DefaultZipCode() { return Profile.DefaultZipCode; }
        public static string Locations() { return Profile.Locations; }
        public static string MiddleName() { return Profile.MiddleName; }
        public static DateTime ModifiedDate() { return Profile.ModifiedDate; }
        public static string Suffix() { return Profile.Suffix; }

    }
}
