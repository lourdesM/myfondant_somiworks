﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Xml;
using Microsoft.ReportingServices;
using Microsoft.Reporting.WebForms;
using Fondant.myFondant.Web.ReportingServices;
using System.Text;
using System.IO;
using System.Net.Security;

namespace Fondant.myFondant.Web.Code
{
    public class myFondantUtilities
    {
        public void SendEmail(string emailTo, string emailSubject, string emailMessage)
        {
            if (!string.IsNullOrEmpty(emailTo))
            {
                MailMessage message = new MailMessage();
                message.To.Add(emailTo);
                message.Subject = emailSubject;
                message.IsBodyHtml = true;
                message.Body = emailMessage;

                try
                {
                    SmtpClient client = new SmtpClient();
                    client.EnableSsl = true;
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    // ignore error for now
                }
                finally
                {
                    message.Dispose();
                }
            }
        }

        public string CreateNewUserMessage(string nameFirst, string nameUser, string passWord, string nameClient)
        {
            string returnMsg = "<p>Dear " + nameFirst;

            returnMsg = returnMsg + "<p>We are pleased to inform you that you have been added as a new user to the site SomiWorks for " + nameClient + ".";
            returnMsg = returnMsg + "  Please read the following information carefully and be sure to save this message in a safe location for future reference. </p>";

            returnMsg = returnMsg + "<p>Website: <a href='https://works.somidata.com'>https://works.somidata.com</a></p>";

            returnMsg = returnMsg + "<p>Username: " + nameUser + "</p>";

            returnMsg = returnMsg + "<p>Password: " + passWord + "</p>";

            returnMsg = returnMsg + "<p>Please take the opportunity to visit the website to review its content and take advantage of its many features.</p>";

            returnMsg = returnMsg + "<p>Thank you,</p>";

            returnMsg = returnMsg + "<p>SomiWorks for " + nameClient + "<br />1.888.621.2260</p>";

            return returnMsg;
        }

        public string CreateForgotPasswordMessage(string passWord)
        {
            string returnMsg = "<p>You have requested for your password to SomiWorks to be emailed to you.  Below is your current password.";
            returnMsg = returnMsg + "  Please be sure to save this message in a safe location for future reference. </p>";

            returnMsg = returnMsg + "<p>Password: " + passWord + "</p>";

            returnMsg = returnMsg + "<p>Website: <a href='https://works.somidata.com'>https://works.somidata.com</a></p>";

            returnMsg = returnMsg + "<p>Thank you,</p>";

            returnMsg = returnMsg + "<p>SomiWorks Team<br />1.888.621.2260</p>";

            return returnMsg;
        }

        public string CreateResetPasswordMessage(string username, string passWord)
        {
            string returnMsg = "<p>Your password to SomiWorks was reset by your company administrator.  Below is your new password.";
            returnMsg = returnMsg + "  You may change your password by logging into SomiWorks and clicking on Change Password under your user name menu. </p>";

            returnMsg = returnMsg + "<p>Password: " + passWord + "</p>";

            returnMsg = returnMsg + "<p>Website: <a href='https://works.somidata.com'>https://works.somidata.com</a></p>";

            returnMsg = returnMsg + "<p>Thank you,</p>";

            returnMsg = returnMsg + "<p>SomiWorks Team<br />1.888.621.2260</p>";

            return returnMsg;
        }
    }

    public class CustomReportCredentials : IReportServerCredentials
    {    // From: http://community.discountasp.net/default.aspx?f=14&m=15967
        public CustomReportCredentials()
        {
        }
        public WindowsIdentity ImpersonationUser
        {
            get
            {
                return null; // not implemented
            }
        }
        public ICredentials NetworkCredentials
        {
            get
            {
                return null; // not implemented
            }
        }
        public bool GetFormsCredentials(out Cookie authCookie, out string user, out string password, out string authority)
        {
            user = password = authority = null;
            HttpCookie cookie = HttpContext.Current.Request.Cookies[".FONDANT"];
            //HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies[".FONDANT"];
            if (cookie == null) HttpContext.Current.Response.Redirect("https://works.somidata.com");
            //if (cookie == null) System.Web.HttpContext.Current.Response.Redirect("https://works.somidata.com");

            Cookie netCookie = new Cookie(cookie.Name, cookie.Value);
            if (cookie.Domain == null)
                netCookie.Domain = HttpContext.Current.Request.ServerVariables["SERVER_NAME"].ToUpper();
                //netCookie.Domain = ConfigurationManager.AppSettings["RSServerName"];
            netCookie.Expires = cookie.Expires;
            netCookie.Path = cookie.Path;
            netCookie.Secure = cookie.Secure;
            authCookie = netCookie;
            return true;
        }
    }

    public class CustomRS2005Credentials : ReportingService2005
    {
        private Cookie m_authCookie;
        public Cookie AuthCookie
        {
            get
            {
                return m_authCookie;
            }
        }
        protected override WebRequest GetWebRequest(Uri uri)        
        {
            //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            //request.UseDefaultCredentials = false;
            //request.Credentials = new System.Net.NetworkCredential("jmitra_fondant", "H9mhbr2k", "whs");
            //request.UserAgent = "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
            //request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            //request.Headers["Accept-Language"] = "en-US,en;q=0.5";
            //request.Headers["Accept-Encoding"] = "gzip, deflate";
            //request.Referer = "http://fondant02:8081/r35reportserver";
            //request.KeepAlive = true;
            //request.Headers["Cache-Controls"] = "max-age=0";
            //
            //CredentialCache cc = new CredentialCache();
            //cc.Add( //start
            //new Uri("http://fondant02:8081/R35ReportServer/ReportService2005.asmx"),
            //"NTLM",
            //new NetworkCredential("50200", ""));
            //request.Credentials = cc; //end of added
            //request.Credentials = base.Credentials; //removed for testing
            //start
            //NetworkCredential nc = new NetworkCredential("50200_admin", "ST4RTR3#");
            //request.Credentials = nc;
            //string credentials = "50200_admin:ST4RTR3#";
            //request.Headers.Add("Authorization", "Basic "
            //    + Convert.ToBase64String(Encoding.Default.GetBytes(credentials)));
            //CredentialCache cache = new CredentialCache();
            //cache.Add(new Uri("http://fondant02:8081/R35ReportServer/ReportService2005.asmx"), "Basic", nc);
            //request.Credentials = cache;
            //request.PreAuthenticate = true;
            //end

            //start
            //// Create a new HttpWebRequest object for the specified resource.
            //WebRequest request = (WebRequest)WebRequest.Create(uri);
            //// Request mutual authentication.
            //request.AuthenticationLevel = AuthenticationLevel.MutualAuthRequested;
            //// Supply client credentials.
            //request.Credentials = CredentialCache.DefaultCredentials;
            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //// Determine whether mutual authentication was used.
            //Console.WriteLine("Is mutually authenticated? {0}", response.IsMutuallyAuthenticated);
            //// Read and display the response.
            //Stream streamResponse = response.GetResponseStream();
            //StreamReader streamRead = new StreamReader(streamResponse);
            //string responseString = streamRead.ReadToEnd();
            //Console.WriteLine(responseString);
            //// Close the stream objects.
            //streamResponse.Close();
            //streamRead.Close();
            //// Release the HttpWebResponse.
            //response.Close();
            //return request;
            //end
            //request.CookieContainer = new CookieContainer();
            ////m_authCookie = FormsAuthentication.GetAuthCookie("50200_admin", true);
            
            //if (m_authCookie != null)
            //    request.CookieContainer.Add(m_authCookie);
            //return request;
            //

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Credentials = base.Credentials;
            request.CookieContainer = new CookieContainer();
            if (m_authCookie != null)
                request.CookieContainer.Add(m_authCookie);
            return request;
        }
        protected override WebResponse GetWebResponse(WebRequest request)
        {
            //request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None; //added
            
            WebResponse response = base.GetWebResponse(request);
            string cookieName = response.Headers["RSAuthenticationHeader"]; //stops here
            if (cookieName != null)
            {
                HttpWebResponse webResponse = (HttpWebResponse)response;
                m_authCookie = webResponse.Cookies[cookieName];
            }
            return response;
        }
    }

    public interface IReportServerConnection2 : IReportServerCredentials
    {
        Uri ReportServerUrl { get; }
        int Timeout { get; }
    }

    [Serializable]
    public class MyConfigFileCredentials : IReportServerCredentials
    {
        public MyConfigFileCredentials()
        {
        }

        public WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                return new NetworkCredential(
                    ConfigurationManager.AppSettings["MyUserName"],
                    ConfigurationManager.AppSettings["MyPassword"]);
            }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;
            return true;
        }
    }
    public class ReportRolePermission
    {
        public XmlDocument Permissions(string ClientAccountNumber)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode xmlnode;
            XmlElement xmlRoot;

            // add the XML declaration section
            xmlnode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlnode);

            // add the root element
            xmlRoot = xmlDoc.CreateElement("permissions");
            xmlDoc.AppendChild(xmlRoot);

            // get list of roles
            List<string> RoleGroups = new List<string>();
            string[] strARoles = Roles.GetAllRoles();
            for (int i = 0; i < strARoles.Length; i++)
            {
                RoleGroups.Add(strARoles[i]);
            }
            RoleGroups.Sort();

            // Folders
            String pstrFolderPath = "/" + ClientAccountNumber; // + Profile.ClientAccountNumber; //commented out for testing
            //String pstrFolderPath = "/" + "50200"; // + Profile.ClientAccountNumber;
            CustomRS2005Credentials rsFolder = new CustomRS2005Credentials();
            rsFolder.LogonUser("r35admin", null, null);
            Fondant.myFondant.Web.ReportingServices.CatalogItem[] cis = null;
            try
            {
                cis = rsFolder.ListChildren(pstrFolderPath, false);
                foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem ci in cis)
                {
                    if (ci.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Folder && (!ci.Hidden))
                    {
                        // Folder - add to Group tree
                        //ci.Name;
                        //Icon.FolderPageWhite;

                        Fondant.myFondant.Web.ReportingServices.CatalogItem[] cisreports = rsFolder.ListChildren(pstrFolderPath + "/" + ci.Name, false);
                        foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem cireports in cisreports)
                        {
                            if (cireports.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Report && (!cireports.Hidden))
                            {
                                // Report - add to Folder tree
                                //cireports.Name;
                                //Icon.Report;
                                //Boolean bInherits;
                                //Policy[] rsPolicy = rs.GetPolicies(pstrReportPath + "/" + ci.Name, out bInherits);

                                XmlNode xmlPermission;
                                XmlElement xmlAccount;
                                XmlElement xmlFolder;
                                XmlElement xmlReport;

                                xmlPermission = xmlDoc.CreateElement("permission");
                                xmlRoot.AppendChild(xmlPermission);

                                xmlAccount = xmlDoc.CreateElement("account");
                                xmlAccount.AppendChild(xmlDoc.CreateTextNode(ClientAccountNumber));
                                xmlPermission.AppendChild(xmlAccount);

                                xmlFolder = xmlDoc.CreateElement("folder");
                                xmlFolder.AppendChild(xmlDoc.CreateTextNode(ci.Name));
                                xmlPermission.AppendChild(xmlFolder);

                                xmlReport = xmlDoc.CreateElement("report");
                                xmlReport.AppendChild(xmlDoc.CreateTextNode(cireports.Name));
                                xmlPermission.AppendChild(xmlReport);

                                Boolean bInherits;
                                Policy[] rsPolicy = rsFolder.GetPolicies(pstrFolderPath + "/" + ci.Name + "/" + cireports.Name, out bInherits);

                                // loop through each role group
                                foreach (string rgTemp in RoleGroups)
                                {
                                    XmlElement xmlRole;

                                    xmlRole = xmlDoc.CreateElement(rgTemp.Replace(' ', '_'));
                                    Boolean blnFound = false;
                                    foreach (Policy pol in rsPolicy)
                                    {
                                        if (pol.GroupUserName == rgTemp.ToString())
                                        {
                                            xmlRole.AppendChild(xmlDoc.CreateTextNode("true"));
                                            blnFound = true;
                                            break;
                                        }
                                    }
                                    if (blnFound == false)
                                    {
                                        xmlRole.AppendChild(xmlDoc.CreateTextNode("false"));
                                    }
                                    xmlPermission.AppendChild(xmlRole);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) //SoapException e)
            {
            }
            return xmlDoc;
        }

        public ReportRolePermission()
        {
        }

        public string Folder { get; set; }
        public string Report { get; set; }
        public string Role { get; set; }
        public bool IsPermitted { get; set; }

        public static List<ReportRolePermission> RolePermissions
        {
            get
            {
                // get list of roles
                List<string> RoleGroups = new List<string>();
                string[] strARoles = Roles.GetAllRoles();
                for (int i = 0; i < strARoles.Length; i++)
                {
                    RoleGroups.Add(strARoles[i]);
                }
                RoleGroups.Sort();

                List<ReportRolePermission> data = new List<ReportRolePermission>();

                // Folders
                String pstrFolderPath = "/50000"; // + Profile.ClientAccountNumber;
                CustomRS2005Credentials rsFolder = new CustomRS2005Credentials();
                rsFolder.LogonUser("r35admin", null, null);
                Fondant.myFondant.Web.ReportingServices.CatalogItem[] cis = null;
                try
                {
                    cis = rsFolder.ListChildren(pstrFolderPath, false);
                    foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem ci in cis)
                    {
                        if (ci.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Folder && (!ci.Hidden))
                        {
                            // Folder - add to Group tree
                            //ci.Name;
                            //Icon.FolderPageWhite;

                            Fondant.myFondant.Web.ReportingServices.CatalogItem[] cisreports = rsFolder.ListChildren(pstrFolderPath + "/" + ci.Name, false);
                            foreach (Fondant.myFondant.Web.ReportingServices.CatalogItem cireports in cisreports)
                            {
                                if (cireports.Type == Fondant.myFondant.Web.ReportingServices.ItemTypeEnum.Report && (!cireports.Hidden))
                                {
                                    // Report - add to Folder tree
                                    //cireports.Name;
                                    //Icon.Report;
                                    //Boolean bInherits;
                                    //Policy[] rsPolicy = rs.GetPolicies(pstrReportPath + "/" + ci.Name, out bInherits);

                                    Boolean bInherits;
                                    Policy[] rsPolicy = rsFolder.GetPolicies(pstrFolderPath + "/" + cireports.Name, out bInherits);

                                    // loop through each role group
                                    foreach (string rgTemp in RoleGroups)
                                    {
                                        ReportRolePermission rrpToAdd = new ReportRolePermission();
                                        rrpToAdd.Folder = ci.Name;
                                        rrpToAdd.Report = cireports.Name;
                                        rrpToAdd.Role = rgTemp.ToString();
                                        rrpToAdd.IsPermitted = false;
                                        foreach (Policy pol in rsPolicy)
                                        {
                                            if (pol.GroupUserName == rrpToAdd.Role)
                                            {
                                                rrpToAdd.IsPermitted = true;
                                                break;
                                            }
                                        }
                                        data.Add(rrpToAdd);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) //SoapException e)
                {
                }
                return data;
            }
        }
    }
}
