﻿using System.Web.Mvc;

namespace Fondant.myFondant.Web.Areas.READYNUMB3R5
{
    public class READYNUMB3R5AreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "READYNUMB3R5";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "READYNUMB3R5_default",
                "READYNUMB3R5/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
