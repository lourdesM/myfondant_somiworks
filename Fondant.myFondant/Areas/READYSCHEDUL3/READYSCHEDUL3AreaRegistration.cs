﻿using System.Web.Mvc;

namespace Fondant.myFondant.Web.Areas.READYSCHEDUL3
{
    public class READYSCHEDUL3AreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "READYSCHEDUL3";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "READYSCHEDUL3_default",
                "READYSCHEDUL3/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
