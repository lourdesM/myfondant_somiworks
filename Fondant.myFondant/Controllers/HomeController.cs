﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using Ext.Net;
using Ext.Net.MVC;
using Fondant.myFondant.Web.Code;
using Fondant.myFondant.Web.Models; //added for authentication
using System.Web.Routing;
using System.Net;
using System.Web.Services; //added for authentication

namespace Fondant.myFondant.Web.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }
            
            base.Initialize(requestContext);
        }


        public ActionResult ValidateUser()
        {
            return View();
        }

        [DirectMethod]
        public ActionResult Index()
        {
           
          ViewData["Username"] = base.HttpContext.User.Identity.Name;
          //  ViewData["Username"] = "50200_admin";
         //   ViewData["email"] = "oscar@somidata.com";
         //   ViewData["LocationID"] = 72;

           // if (MembershipService.ValidateUser("50200_admin", "ST4RTR3#"))
           // {
             //   FormsService.SignIn("50200_admin", true);
           // }
            return View();
        }
        
        //public ActionResult Dashboard() 
        //{
        //   Portlet p = X.GetCmp<Portlet>("Portlet6");
        //    p.LoadContent(Url.Action("Dashboard"));

        //    return this.Direct();
        //}

        public ActionResult Home()
        {
           // ViewData["Username"] = "50200_admin";
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        [Authorize(Roles = "Corporate IT")]
        public ActionResult AdHoc()
        {
            return View();
        }

        public ActionResult ReportAProblem()
        {
            MembershipUser mUser = Membership.GetUser(HttpContext.User.Identity.Name);
            //MembershipUser mUser = Membership.GetUser(System.Web.HttpContext.Current.User.Identity.Name); //might not return anything because user property has no value, only profile

            ViewData["Email"] = mUser.Email;
            return this.View();
        }

        public FormPanelResult SendProblem(string txtClient, string txtUserName, string txtEmail, string txtComments)
        {
            FormPanelResult result = new FormPanelResult();

            if (String.IsNullOrEmpty(txtComments))
            {
                result.Script = X.Msg.Alert("Send", "Comment not found.  Please enter a comment to report.").ToScript();
                return result;
            }

            String strMsgHeader = "Problem Date: " + DateTime.UtcNow.ToString() +
                "<br/>Client: " + txtClient +
                "<br/>Reported by: " + txtUserName +
                "<br/>User Email: " + txtEmail +
                "<br/>Problem:<br/>";

            // send email
            myFondantUtilities ruSendMail = new myFondantUtilities();
            ruSendMail.SendEmail(ConfigurationManager.AppSettings["SupportEmail"], "myFondant User Reported Problem", strMsgHeader + Uri.UnescapeDataString(txtComments));

            result.Script = X.Msg.Alert("Send", "Message sent.  Thank you for reporting the problem.").ToScript();
            return result;
        }

        public ActionResult Viewer()
        {
            return View();
        }
    }
}
