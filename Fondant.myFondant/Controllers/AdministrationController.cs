﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fondant.myFondant.Web.Controllers
{
    public class AdministrationController : Controller
    {
        //
        // GET: /Administration/

       // [Authorize(Roles = "Corporate IT")] //removed
        public ActionResult Index()
        {
            //this.ViewData["UserName"].ToString();
            return View();
            
        }


        // GET: /Administration/UserDetail

        public ActionResult UserDetail()
        {
            ViewData["UserId"] = HttpContext.Request["UserId"] ?? "";
            ViewData["IsNewUser"] = HttpContext.Request["IsNewUser"] ?? "false";
            ViewData["ASPNETUserId"] = HttpContext.Request["ASPNETUserId"] ?? "";
            //ViewData["UserId"] = System.Web.HttpContext.Current.Request["UserId"] ?? "";
            //ViewData["IsNewUser"] = System.Web.HttpContext.Current.Request["IsNewUser"] ?? "false";

            //added for testing
            //ViewData["UserId"] = "50200_admin";
            //ViewData["IsNewUser"] = "false";
            //end of added

            return this.View();
        }


        // GET: /Administration/NewUser

        public ActionResult NewUser()
        {
            return this.View();
        }
    }
}
