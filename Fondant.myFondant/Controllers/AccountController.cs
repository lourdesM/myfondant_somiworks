﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Ext.Net.MVC;
using Fondant.myFondant.Web.Models;
using System.Web.Profile;
using System.Web.Configuration;
using Fondant.myFondant.Web.Code;
namespace Fondant.myFondant.Web.Controllers
{

    [HandleError]
    public class AccountController : Controller
    {

       /* public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        // **************************************
        // URL: /Account/ClientLogIn
        // **************************************

        //added
        public ActionResult Index() 
        {
            return View();
        }
        public ActionResult UserDetail()
        {
            return View(); 
        }
        public ActionResult NewUser()
        {
            return View(); 
        }
        //end of added
        public ActionResult ClientLogIn()
        {
           // return RedirectToAction("Home", "Home"); //added
            return View(); //original
        }

        [HttpPost]
        public ActionResult ClientLogInNow(LogOnModel model, string returnUrl)
        {
            //model checks if the entered value for the field is valid or not
            //at this point we are sure that the model is passing the correct user details
            if (ModelState.IsValid)
            {
                MembershipService = new AccountMembershipService(Membership.Provider); //added
                if (MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    FormsService = new FormsAuthenticationService(); //added
                    
                    FormsService.SignIn(model.UserName, model.RememberMe);

                    ProfileBase profile = ProfileBase.Create(model.UserName);
                    //profile.SetPropertyValue("UserName", "50200_admin"); //added for testing
                    profile.Save();


                    //System.Web.HttpContext.Current.Profile.Initialize(model.UserName, true);
                    if (!String.IsNullOrEmpty(returnUrl))
                    {
                        //return Redirect("/Home/Home"); 
                        //return RedirectToAction("Home", "Home");
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                        //return RedirectToAction("ClientLogIn", "Account"); //for testing only
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The username or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, return the first error message
            //return View(model) -- no need for this;
            var errorList = (from item in ModelState.Values
                             from error in item.Errors
                             select error.ErrorMessage).ToList();
            return new AjaxResult { ErrorMessage = errorList.FirstOrDefault().ToString() };
        }

        // **************************************
        // URL: /Account/LogOut
        // **************************************

        public ActionResult LogOut()
        {
            FormsService.SignOut();

            return RedirectToAction("ClientLogIn", "Account");
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************
        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return new AjaxResult { Result = "Success" };
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            //ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            //return View(model);
            var errorList = (from item in ModelState.Values
                             from error in item.Errors
                             select error.ErrorMessage).ToList();
            return new AjaxResult { ErrorMessage = errorList.FirstOrDefault().ToString() };
        }*/
        public AccountController()
        {
        }
       



        [Authorize, HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (base.ModelState.IsValid)
            {
                if (this.MembershipService.ChangePassword(base.User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return new AjaxResult { Result = "Success" };
                }
                base.ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
            }
            List<string> source = (from item in base.ModelState.Values
                                   from error in item.Errors
                                   select error.ErrorMessage).ToList<string>();
            return new AjaxResult { ErrorMessage = source.FirstOrDefault<string>().ToString() };
        }

        public Ext.Net.DirectResponse ChangePassword2(string OldPassword, string NewPassword)
        {
            Ext.Net.DirectResponse response = new Ext.Net.DirectResponse();
            Ext.Net.ParameterCollection parameters = new Ext.Net.ParameterCollection();
            var userName = (string)System.Web.HttpContext.Current.Application["LoginUserName"];
            if (this.MembershipService.ChangePassword(userName, OldPassword, NewPassword))
            {
                parameters["messageSuccess"] = "Success";
                response.Success = true;
            }
            else
            {
                parameters["messageFailed"] = "Failed! The current password is incorrect or the new password is invalid.";
                response.Success = false;
            }
            response.ExtraParamsResponse = parameters.ToJson();

            return response;       
           
        }

        public Ext.Net.DirectResponse ForgotPassword(string userName)
        {
            Ext.Net.DirectResponse response = new Ext.Net.DirectResponse();
            Ext.Net.ParameterCollection parameters = new Ext.Net.ParameterCollection();

            string strPwd = string.Empty;
            string strMsg = string.Empty;
            try
            {
                if (userName == string.Empty || userName == "")
                {
                    parameters["messageFailed"] = "Please enter your username in the Username field to retrieve your password.";
                    response.Success = false;
                }
                else
                {

                    strPwd = Membership.Provider.GetPassword(userName, string.Empty);
                    strMsg = "Your password was sent to your registered email address.";

                    myFondantUtilities myFUtils = new myFondantUtilities();
                    string strEmailMsg = myFUtils.CreateForgotPasswordMessage(strPwd);
                    MembershipUser mUser = Membership.GetUser(userName);
                    if (mUser.Email == string.Empty || mUser.Email == "")
                    {
                        parameters["messageFailed"] = "No email was found under the username provided.  Please contact your company administrator.";
                        response.Success = false;
                    }
                    else
                    {
                        myFUtils.SendEmail(mUser.Email, "Your myFondant Password", strEmailMsg);
                        parameters["messageSuccess"] = "Success";
                        response.Success = true;
                    }
                }
               
            }
            catch (Exception ex)
            {
                parameters["messageFailed"] = "Your password could not be retrieved at this time.";
                response.Success = false;
              
            }
          


           
            /*var userName = WebConfigurationManager.AppSettings["LoginUserName"];
            if (this.MembershipService.ChangePassword(userName, OldPassword, NewPassword))
            {
                parameters["messageSuccess"] = "Success";
                response.Success = true;
            }
            else
            {
                parameters["messageFailed"] = "Failed! The current password is incorrect or the new password is invalid.";
                response.Success = false;
            }*/
            response.ExtraParamsResponse = parameters.ToJson();

            return response;

        }

       // [Authorize]
        public ActionResult ClientLogIn()
        {
            // return RedirectToAction("Home", "Home"); //added
            return View(); //original
        }
        [HttpPost]
        public ActionResult ClientLogInNow(LogOnModel model, string returnUrl)
        {
            System.Web.HttpContext.Current.Application["CurrentlyLoggedIn"] = "false";
            if (base.ModelState.IsValid)
            {
                if (this.MembershipService.ValidateUser(model.UserName, model.Password))
                {
                    this.FormsService.SignIn(model.UserName, model.RememberMe);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                       
                       return this.Redirect(returnUrl);
                    }

                    MembershipUser UserProfile= this.MembershipService.GetUserProfileByUsername(model.UserName);
                    
                   // System.Web.HttpContext.Current.Application["SessionId"] = Session["SessionId"];
                    System.Web.HttpContext.Current.Application["LoginUserName"] = UserProfile.UserName;
                    System.Web.HttpContext.Current.Application["LoginUserEmail"] = UserProfile.Email;

                    System.Web.HttpContext.Current.Application["CurrentlyLoggedIn"] = "true";
                   

                    return base.RedirectToAction("Index", "Home");
                }
                base.ModelState.AddModelError("", "The username or password provided is incorrect.");
            }
            List<string> source = (from item in base.ModelState.Values
                                   from error in item.Errors
                                   select error.ErrorMessage).ToList<string>();
            return new AjaxResult { ErrorMessage = source.FirstOrDefault<string>().ToString() };
        }

       

        protected override void Initialize(RequestContext requestContext)
        {
            if (this.FormsService == null)
            {
                this.FormsService = new FormsAuthenticationService();
            }
            if (this.MembershipService == null)
            {
                this.MembershipService = new AccountMembershipService();
            }
            base.Initialize(requestContext);
        }




        public ActionResult LogOut()
        {
            this.FormsService.SignOut();
            return base.RedirectToAction("ClientLogIn", "Account");
        }




        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
 


 

 

 

    }
}
