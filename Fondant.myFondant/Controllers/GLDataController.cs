﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Ext.Net;
using Ext.Net.MVC;
using Fondant.myFondant.Web.Models;
using Fondant.myFondant.Web.Code;
using SomiWorks.Controllers;
using SomiWorks.Models;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using SomiWorks.Class;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Net;
using System.IO;
using Fondant.myFondant.Web.Controllers;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Xml.Xsl;
using System.Xml.Serialization;
namespace Fondant.myFondant.Web.Controllers
{
    [HandleError]
    public class GLDataController : BaseDataController
    {
        public string SQL_CONNECTION_STRING = "server=54.149.142.14;database=RN_THEDRYBAR_SETUP;user id=sa; Password=H9mhbr2k";// System.Configuration.ConfigurationManager.ConnectionStrings["DataAccessQuickStart"].ConnectionString;
        
        string netSuiteURL = WebConfigurationManager.AppSettings["nsURL"];
        string netSuiteURL_InventoryMovement = WebConfigurationManager.AppSettings["nsURL_InventoryMovement"];
        
        string _SessionId = "";
        string netSuiteDATA = "";
        string netSuiteData_InventoryMovement = "";
        private const string LogFilePath = "~/InventoryMovementLogs";
        private DateTime _lastInventoryLog;
        private string _logInventoryFeedsToNetSuites;
        private class netSuiteResult
        {
            public string status { get; set; }
            public string message { get; set; }
            public string intergationId { get; set; }
        }

        public DataSet GetDataSummaryReader(string frmDate, string toDate, int locationId, string entry)
        {
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            string frmDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            string toDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            sqlStr = "SELECT gls.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Summary('" + frmDt + "', '" + toDt + "') gls INNER JOIN dbo.Locations_Mapping lm ON gls.LocationInternalId=lm.LocationInternalId" +
                   " INNER JOIN dbo.Locations l" +
                   " ON lm.LocationID=l.LocationID" +
                   " WHERE gls.LocationInternalId != 0" +
                   " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                   " ORDER BY ClosedOrderDate, gls.LocationInternalId";

            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLSummary = new DataSet();
            scom.Fill(dsGLSummary);
            adocn.Close();
            return dsGLSummary;
        }
        public DataSet GetFranchiseSalesReader(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Entries, string StartDate, string EndDate)
        {
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();

            if (ClosedOrderDates != "" && LocationInternalIds != "" && StartDate != "" && EndDate != "")
            {
                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Details('" + StartDate + "','" + EndDate + "') gld " +
                         " LEFT JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " LEFT JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0  AND " +
                         " gld.LocationInternalId IN (" + LocationInternalIds + ")" +
                         " AND gld.ClosedOrderDate IN (" + ClosedOrderDates + ")" +
                         " AND lm.LocationType LIKE 'Franchise'" +
                       // " AND gld.Status IN ("+Entries+")"+
                         " ORDER BY gld.ClosedOrderDate, gld.LocationInternalID";
            }
            else
            {
                string frmDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                string toDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");

                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM dbo.fxnGLAccountEntries_Details('" + frmDt + "','" + toDt + "') gld " +
                         " INNER JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " INNER JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0" +
                         " AND lm.LocationType LIKE 'Franchise'" +
                         " ORDER BY ClosedOrderDate, gld.LocationInternalID";
            }

            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLDetail = new DataSet();
            scom.Fill(dsGLDetail);
            adocn.Close();
            GetUpdatedEntries(ClosedOrderDates,LocationIDs,LocationInternalIds, Entries, StartDate, EndDate);
            return dsGLDetail;
        }
        public DataSet GetGLDetailReader(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Entries, string StartDate, string EndDate, string SessionId)
        {
            _SessionId = SessionId;
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();

            if (ClosedOrderDates != "" && LocationInternalIds != "" && StartDate != "" && EndDate != "")
            {
                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Details('" + StartDate + "','" + EndDate + "') gld " +
                         " LEFT JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " LEFT JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0  AND " +
                         " gld.LocationInternalId IN (" + LocationInternalIds + ")" +
                         " AND gld.ClosedOrderDate IN (" + ClosedOrderDates + ")" +
                         " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                       // " AND gld.Status IN ("+Entries+")"+
                         " ORDER BY gld.ClosedOrderDate, gld.LocationInternalID";
            }
            else
            {
                string frmDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                string toDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");

                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM dbo.fxnGLAccountEntries_Details('" + frmDt + "','" + toDt + "') gld " +
                         " INNER JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " INNER JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0" +
                         " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                         " ORDER BY ClosedOrderDate, gld.LocationInternalID";
            }

            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLDetail = new DataSet();
            scom.Fill(dsGLDetail);
            adocn.Close();
            GetUpdatedEntries(ClosedOrderDates,LocationIDs,LocationInternalIds, Entries, StartDate, EndDate);
            return dsGLDetail;
        }
        public void GetUpdatedEntries(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Status, string StartDate, string EndDate)
        {
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            if (ClosedOrderDates != "" && LocationInternalIds != "" && StartDate != "" && EndDate != "")
            {
                sqlStr = "SELECT gls.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Summary('" + StartDate + "', '" + EndDate + "') gls INNER JOIN dbo.Locations_Mapping lm ON gls.LocationInternalId=lm.LocationInternalId" +
                       " INNER JOIN dbo.Locations l" +
                       " ON lm.LocationID=l.LocationID" +
                       " WHERE gls.LocationInternalId != 0" +
                       " AND gls.ClosedOrderDate IN (" + ClosedOrderDates + ")" +
                       " AND gls.LocationInternalId IN (" + LocationInternalIds + ")" +
                       " AND l.LocationID IN (" + LocationIDs + ")" +
                       " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                       " ORDER BY ClosedOrderDate, gls.LocationInternalId";
            }
            else
            {
                string frmDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                string toDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                sqlStr = "SELECT gls.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Summary('" + frmDt + "', '" + toDt + "') gls INNER JOIN dbo.Locations_Mapping lm ON gls.LocationInternalId=lm.LocationInternalId" +
                       " INNER JOIN dbo.Locations l" +
                       " ON lm.LocationID=l.LocationID" +
                       " WHERE gls.LocationInternalId != 0" +
                       " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                       " ORDER BY ClosedOrderDate, gls.LocationInternalId";
            }
            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLSummary = new DataSet();
            scom.Fill(dsGLSummary);
            string entries = "";
            int i = 1;
            foreach (DataRow dr in dsGLSummary.Tables[0].Rows)
            {
                
                string result = (Convert.ToDecimal(dr["Debit_Total"]) == Convert.ToDecimal(dr["Credit_Total"])) ? "balanced" : "unbalanced";
                entries += "'" + result + "'";
                if (i < dsGLSummary.Tables[0].Rows.Count)
                {
                    entries += ",";
                }
                i++;
            }
            System.Web.HttpContext.Current.Application["Entries_" + _SessionId] = entries;
            adocn.Close();
        }
        
        public void GetGLDetailReaderJS(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Entries, string StartDate, string EndDate)
        {
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            var data = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();

            if (ClosedOrderDates != "" && LocationInternalIds != "" && StartDate != "" && EndDate != "")
            {
                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Details('" + StartDate + "','" + EndDate + "') gld " +
                         " LEFT JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " LEFT JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0  AND " +
                         " gld.LocationInternalId IN (" + LocationInternalIds + ")" +
                         " AND gld.ClosedOrderDate IN (" + ClosedOrderDates + ")" +
                         " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                    // " AND gld.Status IN ("+Entries+")"+
                         " ORDER BY gld.ClosedOrderDate, gld.LocationInternalID";
            }
            else
            {
                string frmDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                string toDt = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");

                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM dbo.fxnGLAccountEntries_Details('" + frmDt + "','" + toDt + "') gld " +
                         " INNER JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " INNER JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0" +
                         " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                         " ORDER BY ClosedOrderDate, gld.LocationInternalID";
            }

            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLDetail = new DataSet();
            scom.Fill(dsGLDetail);

            var paramResult = "[";

            ParameterCollection parameters = new ParameterCollection();

            foreach (DataRow dr in dsGLDetail.Tables[0].Rows)
            {
                parameters["Id"] = dr["Id"].ToString();
                parameters["Subsidiary"] = dr["Subsidiary"].ToString();
                parameters["ClosedOrderDate"] = dr["ClosedOrderDate"].ToString();
                parameters["Posting_Period"] = dr["Posting_Period"].ToString();
                parameters["gl_currency"] = dr["gl_currency"].ToString();
                parameters["gl_account"] = dr["gl_account"].ToString();
                parameters["gl_account_name"] = dr["gl_account_name"].ToString();
                parameters["DEBIT"] = dr["DEBIT"].ToString();
                parameters["CREDIT"] = dr["CREDIT"].ToString();
                parameters["Memo"] = dr["Memo"].ToString();
                parameters["Class"] = dr["Class"].ToString();
                // parameters["Location"] = dr["Location"].ToString();
                parameters["LocationInternalID"] = dr["LocationInternalID"].ToString();
                parameters["Status"] = dr["Status"].ToString();
                parameters["LocationID"] = dr["LocationID"].ToString();
                parameters["LocationName"] = dr["LocationName"].ToString();

                paramResult += parameters.ToJson() + ",";

            }

            if (paramResult.Length > 1)
            {
                data = paramResult.Substring(0, paramResult.Length - 1) + "]";
            }
            else
            {
                data = "";
            };
            adocn.Close();
           
            new DirectResponse { Result = data }.Return();
        }

        public string GetUpdatedEntriesJS(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Status, string StartDate, string EndDate)
        {
            
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            string frmDt = StartDate;// DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            string toDt = EndDate;//DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
            sqlStr = "SELECT gls.*, l.LocationID, l.LocationName FROM fxnGLAccountEntries_Summary('" + frmDt + "', '" + toDt + "') gls INNER JOIN dbo.Locations_Mapping lm ON gls.LocationInternalId=lm.LocationInternalId" +
                   " INNER JOIN dbo.Locations l" +
                   " ON lm.LocationID=l.LocationID" +
                   " WHERE gls.LocationInternalId != 0" +
                   " AND gls.ClosedOrderDate IN (" + ClosedOrderDates + ")" +
                   " AND gls.LocationInternalId IN (" + LocationInternalIds + ")" +
                   " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                  // " AND gls.Status IN (" + Entries + ")" +
                  // " AND l.LocationID IN (" + LocationIDs + ")" +
                   " ORDER BY ClosedOrderDate, gls.LocationInternalId";

            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsGLSummary = new DataSet();
            scom.Fill(dsGLSummary);
            string entries = "";
            int i = 1;
            foreach (DataRow dr in dsGLSummary.Tables[0].Rows)
            {

                string result = (Convert.ToDecimal(dr["Debit_Total"]) == Convert.ToDecimal(dr["Credit_Total"])) ? "balanced" : "unbalanced";
                entries += "'" + result + "'";
                if (i < dsGLSummary.Tables[0].Rows.Count)
                {
                    entries += ",";
                }
                i++;
            }
            
            adocn.Close();
            return entries;
        }
        public void FilterGLSummaryByDate(string frmDate, string toDate)
        {
            
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            var data = "";

            if (frmDate != "" && toDate != "")
            {
                try
                {
                    frmDate = Regex.Replace(frmDate, "[^0-9a-zA-Z -]+", "");
                    toDate = Regex.Replace(toDate, "[^0-9a-zA-Z -]+", "");
                    string[] frmDate1 = frmDate.Split('T');
                    string[] toDate1 = toDate.Split('T');
                    string frmDt = Convert.ToDateTime(frmDate1[0]).ToString("MM/dd/yyyy");
                    string toDt = Convert.ToDateTime(toDate1[0]).ToString("MM/dd/yyyy");

                    sqlStr = "SELECT gls.*, l.LocationID, l.LocationName FROM dbo.fxnGLAccountEntries_Summary('" + frmDt + "', '" + toDt + "') gls INNER JOIN dbo.Locations_Mapping lm ON gls.LocationInternalId=lm.LocationInternalId" +
                   " INNER JOIN dbo.Locations l" +
                   " ON lm.LocationID=l.LocationID" +
                   " WHERE gls.LocationInternalId != 0 " +
                   " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                   " ORDER BY gls.ClosedOrderDate, gls.LocationInternalId";

                    SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
                    scom.SelectCommand.CommandTimeout = 0;
                    DataSet dsGLSummary = new DataSet();
                    scom.Fill(dsGLSummary);


                    var paramResult = "[";

                    ParameterCollection parameters = new ParameterCollection();
                    int i = 1;
                    foreach (DataRow dr in dsGLSummary.Tables[0].Rows)
                    {
                       //  decimal debit_total =  !String.IsNullOrEmpty(dr["Debit_Total"].ToString()) ? Convert.ToDecimal(dr["Debit_Total"]) : 0;
                      //decimal credit_total = dr["Credit_Total"] != null ? Convert.ToDecimal(dr["Credit_Total"]) : 0;
     
                        parameters["Subsidiary"] = dr["Subsidiary"].ToString();
                        parameters["ClosedOrderDate"] = dr["ClosedOrderDate"].ToString();
                        parameters["Posting_Period"] = dr["Posting_Period"].ToString();
                        parameters["Debit_Total"] = dr["Debit_Total"].ToString();
                        parameters["Credit_Total"] = dr["Credit_Total"].ToString();
                        parameters["LocationInternalId"] =  dr["LocationInternalId"].ToString();
                        parameters["Status"] = dr["Status"].ToString();
                        parameters["LocationID"] = dr["LocationID"].ToString();
                        parameters["LocationName"] = dr["LocationName"].ToString();
                        parameters["Entries"] = (Convert.ToDecimal(dr["Debit_Total"]) == Convert.ToDecimal(dr["Credit_Total"])) ? "balanced" : "unbalanced";
                        parameters["OutOfBalanceAmount"] = (Convert.ToDecimal(dr["Debit_Total"]) > Convert.ToDecimal(dr["Credit_Total"])) ? (Convert.ToDecimal(dr["Debit_Total"]) - Convert.ToDecimal(dr["Credit_Total"])).ToString() : (Convert.ToDecimal(dr["Credit_Total"]) - Convert.ToDecimal(dr["Debit_Total"])).ToString();
                        paramResult += parameters.ToJson() + ",";
                        i++;
                    }

                   

                    if (paramResult.Length > 1)
                    {
                        data = paramResult.Substring(0, paramResult.Length - 1) + "]";
                    }
                    else
                    {
                        data = "";
                    };
                }
                catch (InvalidOperationException e)
                {
                    data = e.Message;
                }
            }
            adocn.Close();
            new DirectResponse { Result = data }.Return();
        }

        public void FilterGLEntriesByDate(string frmDate, string toDate)
        {
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            var data = "";

            if (frmDate != "" && toDate != "")
            {
                try
                {
                    frmDate = Regex.Replace(frmDate, "[^0-9a-zA-Z -]+", "");
                    toDate = Regex.Replace(toDate, "[^0-9a-zA-Z -]+", "");
                    string[] frmDate1 = frmDate.Split('T');
                    string[] toDate1 = toDate.Split('T');
                    string frmDt = Convert.ToDateTime(frmDate1[0]).ToString("MM/dd/yyyy");
                    string toDt = Convert.ToDateTime(toDate1[0]).ToString("MM/dd/yyyy");

                    sqlStr =  "SELECT gld.*, l.LocationID, l.LocationName FROM dbo.fxnGLAccountEntries_Details('" + frmDt + "','" + toDt + "') gld " +
                         " INNER JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId" +
                         " INNER JOIN dbo.Locations l" +
                         " ON lm.LocationID=l.LocationID" +
                         " WHERE gld.LocationInternalId !=0" +
                         " AND (lm.LocationType IS NULL OR lm.LocationType LIKE 'Retail')" +
                         " ORDER BY ClosedOrderDate, gld.LocationInternalID";

                    SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
                    scom.SelectCommand.CommandTimeout = 0;
                    DataSet dsGLSummary = new DataSet();
                    scom.Fill(dsGLSummary);

                    var paramResult = "[";

                    ParameterCollection parameters = new ParameterCollection();

                    foreach (DataRow dr in dsGLSummary.Tables[0].Rows)
                    {
                        parameters["Id"] = dr["Id"].ToString();
                        parameters["Subsidiary"] = dr["Subsidiary"].ToString();
                        parameters["ClosedOrderDate"] = dr["ClosedOrderDate"].ToString();
                        parameters["Posting_Period"] = dr["Posting_Period"].ToString();
                        parameters["gl_currency"] = dr["gl_currency"].ToString();
                        parameters["gl_account"] = dr["gl_account"].ToString();
                        parameters["gl_account_name"] = dr["gl_account_name"].ToString();
                        parameters["DEBIT"] = dr["DEBIT"].ToString();
                        parameters["CREDIT"] = dr["CREDIT"].ToString();
                        parameters["Memo"] = dr["Memo"].ToString();
                        parameters["Class"] = dr["Class"].ToString();
                        // parameters["Location"] = dr["Location"].ToString();
                        parameters["LocationInternalID"] = dr["LocationInternalID"].ToString();
                        parameters["Status"] = dr["Status"].ToString();
                        parameters["LocationID"] = dr["LocationID"].ToString();
                        parameters["LocationName"] = dr["LocationName"].ToString();

                        paramResult += parameters.ToJson() + ",";

                    }

                    if (paramResult.Length > 1)
                    {
                        data = paramResult.Substring(0, paramResult.Length - 1) + "]";
                    }
                    else
                    {
                        data = "";
                    };
                }
                catch (InvalidOperationException e)
                {
                    data = e.Message;
                }
            }

            adocn.Close();
            new DirectResponse { Result = data }.Return();
        }

        public DirectResponse PostGLEntry(string ClosedOrderDates, string LocationInternalIds, string StartDate, string EndDate)
        {
            _SessionId = Request.QueryString["sessionId"];
            DirectResponse response = new DirectResponse();
            ParameterCollection parameters = new ParameterCollection();
            var result = "";
            var errorMsg = "";

            try
            {
              //  if (ClosedOrderDates != "" && ClosedOrderDates != null && LocationInternalIds != "" && LocationInternalIds != null && StartDate != "" && EndDate != "")
              //  {
                    SqlConnection adocn = new SqlConnection();
                    adocn.ConnectionString = SQL_CONNECTION_STRING;
                    adocn.Open();
                    string strSQL;
                    if (ClosedOrderDates != "" && ClosedOrderDates != null && LocationInternalIds != "" && LocationInternalIds != null && StartDate != "" && EndDate != "")
                    {
                        strSQL = "SELECT DISTINCT CONVERT (DATE, ClosedOrderDate) 'ClosedOrderDate',  LocationInternalId FROM fxnGLAccountEntries('" + StartDate + "', '" + EndDate + "') WHERE LocationInternalId IN (" + LocationInternalIds + ") AND CONVERT (DATE, ClosedOrderDate) IN (" + ClosedOrderDates + ")  AND IsPosted='false'  AND LocationInternalId<>'0'";
                    }
                    else if (ClosedOrderDates == "" && LocationInternalIds == "" && StartDate == "" && EndDate == "")
                    {
                        StartDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                        EndDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                        strSQL = "SELECT DISTINCT CONVERT (DATE, ClosedOrderDate) 'ClosedOrderDate',  LocationInternalId FROM fxnGLAccountEntries('" + StartDate + "', '" + EndDate + "') WHERE  IsPosted='false'  AND LocationInternalId<>'0'";
                    }
                    else
                    {
                        strSQL = "";
                    }
                    if (strSQL != "")
                    {
                        SqlDataAdapter distinctScom = new SqlDataAdapter(strSQL, adocn);
                        distinctScom.SelectCommand.CommandTimeout = 0;
                        DataSet dsDisctinctGLDetails = new DataSet();
                        distinctScom.Fill(dsDisctinctGLDetails);


                        if (dsDisctinctGLDetails.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsDisctinctGLDetails.Tables[0].Rows)
                            {

                                netSuiteDATA = "{";
                                netSuiteDATA += "\"dateOfUpdate\": \"" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "\", ";
                                netSuiteDATA += "\"customerId\": \"" + dr["LocationInternalID"] + "\", ";
                                netSuiteDATA += "\"jeDetail\": [";


                                var sqlStr = "SELECT gla.Id, gla.Subsidiary, CONVERT (DATE, gla.ClosedOrderDate) 'ClosedOrderDate', gla.Posting_Period, gla.gl_currency, gla.gl_account, gla.gl_account_name, gla.DEBIT, gla.CREDIT, gla.Memo, gla.Class, gla.Department, gla.Location, gla.LocationInternalID,   l.LocationID, l.LocationName FROM fxnGLAccountEntries('" + StartDate + "', '" + EndDate + "') gla" +
                                                               " INNER JOIN dbo.Locations_Mapping lm ON gla.LocationInternalId=lm.LocationInternalId" +
                                                               " INNER JOIN dbo.Locations l" +
                                                               " ON lm.LocationID=l.LocationID" +
                                                               " WHERE gla.LocationInternalId IN (" + dr["LocationInternalID"] + ")" +
                                                               " AND CONVERT (DATE, gla.ClosedOrderDate) IN ('" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "')" +
                                                               " ORDER BY gla.LocationInternalId, CONVERT (DATE, gla.ClosedOrderDate)";
                                SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
                                scom.SelectCommand.CommandTimeout = 0;
                                DataSet dsGLDetails = new DataSet();
                                scom.Fill(dsGLDetails);

                                if (dsGLDetails.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dr1 in dsGLDetails.Tables[0].Rows)
                                    {

                                        if (Convert.ToDouble(dr1["DEBIT"]) <= 0.00 && Convert.ToDouble(dr1["CREDIT"]) <= 0.00)
                                        {
                                        }
                                        else
                                        {
                                            netSuiteDATA += "{";
                                            netSuiteDATA += "\"accountId\": \"" + dr1["gl_account"] + "\", ";
                                            netSuiteDATA += "\"debitAmount\": \"" + dr1["DEBIT"] + "\", ";
                                            netSuiteDATA += "\"creditAmount\": \"" + dr1["CREDIT"] + "\", ";
                                            netSuiteDATA += "\"memoText\": \"" + dr1["Memo"] + "\", ";
                                            netSuiteDATA += "}, ";
                                        }
                                    }
                                    netSuiteDATA += "]}";
                                    result = postGLEntriesToNetSuitesPerLocationInternalId(dsGLDetails, dr["LocationInternalID"].ToString());

                                    if (result != "Success")
                                    {
                                        errorMsg = result;
                                    }
                                }
                            }

                            if (errorMsg == "")
                            {
                                parameters["messageSuccess"] = "Success!";
                                response.Success = true;
                            }
                            else
                            {
                                parameters["messageSuccess"] = errorMsg;
                                response.Success = false;
                            }
                        }
                        else
                        {
                            parameters["messageSuccess"] = "Unable to process request. These items have already been posted!";
                            response.Success = false;
                        }
                    }
                    else
                    {
                        parameters["messageFailed"] = "Failed! Invalid Data.";
                        response.Success = false;
                    }
                    adocn.Close();
                // }
                // else
                //{
                //    parameters["messageFailed"] = "Failed! Invalid Customer Internal ID.";
                //    response.Success = false;
                // }

            }
            catch (SqlException e)
            {
                parameters["messageFailed"] = e.Message;
                response.Success = false;
            }
            response.ExtraParamsResponse = parameters.ToJson();

            return response;
        }

        //This task is being called from a VB Script and windows schedule in Daily Basis for Nightly Inventory Movement
        public void InventoryMovement()
        {
            try
            {
                var StartDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                var EndDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy");
                var result = "";
                var logInventoryFeedsToNetSuites = "";
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();

                SqlDataAdapter distinctScom = new SqlDataAdapter("SELECT DISTINCT LocationID, ClosedOrderDate, UpdateType FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') ORDER BY ClosedOrderDate, LocationID", adocn);
                distinctScom.SelectCommand.CommandTimeout = 0;
                DataSet dsDisctinctInventoryMovement = new DataSet();
                distinctScom.Fill(dsDisctinctInventoryMovement);

                if (dsDisctinctInventoryMovement.Tables[0].Rows.Count > 0)
                {
                    logInventoryFeedsToNetSuites += "EXECUTED ON " + DateTime.Now.ToString() + "\r\n  ";
                    logInventoryFeedsToNetSuites += "Inventory Movement : " + StartDate + " - " + EndDate + "\r\n \r\n  ";
                    foreach (DataRow dr in dsDisctinctInventoryMovement.Tables[0].Rows)
                    {
                        netSuiteData_InventoryMovement = "{";
                        netSuiteData_InventoryMovement += "\"dateOfUpdate\": \"" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "\", ";
                        netSuiteData_InventoryMovement += "\"customerId\": \"" + dr["LocationID"] + "\", ";
                        netSuiteData_InventoryMovement += "\"updateType\": \"" + dr["UpdateType"] + "\", ";
                        netSuiteData_InventoryMovement += "\"itemDetail\": [";

                        var sqlStr = "SELECT * FROM fxnInventoryMovements('" + StartDate + "', '" + EndDate + "') WHERE LocationID='" + dr["LocationID"] + "' AND ClosedOrderDate='" + Convert.ToDateTime(dr["ClosedOrderDate"]).ToString("MM/dd/yyyy") + "'";
                        SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
                        scom.SelectCommand.CommandTimeout = 0;
                        DataSet dsItemDetails = new DataSet();
                        scom.Fill(dsItemDetails);

                        if (dsItemDetails.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr1 in dsItemDetails.Tables[0].Rows)
                            {

                                netSuiteData_InventoryMovement += "{";
                                netSuiteData_InventoryMovement += "\"qtySoldOrConsumed\": \"" + dr1["Quantity"] + "\", ";
                                netSuiteData_InventoryMovement += "\"skuId\": \"" + dr1["SKU"] + "\", ";
                                netSuiteData_InventoryMovement += "}, ";

                            }
                            netSuiteData_InventoryMovement += "]}";
                        }


                        result = postInventoriesToNetStuites(dsItemDetails);
                        if (result != "Failed")
                        {
                            result = "Success: (IntegrationID : " + result + ") :";
                        }
                        else
                        {
                            result = "Failed: ";
                        }

                        logInventoryFeedsToNetSuites += "STATUS: "+ result + " \r\n \r\n " + netSuiteData_InventoryMovement + " \r\n \r\n ";

                    }
                }

                adocn.Close();
                _logInventoryFeedsToNetSuites = logInventoryFeedsToNetSuites;
                

            }
            catch (Exception e)
            {
                _logInventoryFeedsToNetSuites += "EXECUTED ON " + DateTime.Now.ToString() + "\r\n  ";
                _logInventoryFeedsToNetSuites += "FAILED TO EXECUTE INVENTORY MOVEMENT!";
                
            }

            InitializeInventoryMovementToNetSuitesLog();
        }

        private void InitializeInventoryMovementToNetSuitesLog()
        {
            var path = GetLogFilePath();
            path = Path.Combine(path, "InventoryMovementLogs_" + DateTime.Now.ToString("yyyyMMdd-HHMMss") + ".txt");
            System.IO.File.AppendAllText(path, _logInventoryFeedsToNetSuites);
            if (System.IO.File.Exists(path))
            {
                try
                {
                    _lastInventoryLog = DateTime.Parse(System.IO.File.ReadAllText(path));
                }
                catch { }
            }

        }
        public string GetLogFilePath()
        {
            var path = System.Web.Hosting.HostingEnvironment.MapPath(LogFilePath);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path = Path.Combine(path, "InventoryMovementLogs");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
        private string postGLEntriesToNetSuitesPerLocationInternalId(DataSet dsGLDetails, string locationInternalId)
        {
            var storeResponse = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(netSuiteURL);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "*/*";

                String requestParams = "nlauth_account=" + WebConfigurationManager.AppSettings["nsAccount"] + ", nlauth_email=" + WebConfigurationManager.AppSettings["nsEmail"] + ", nlauth_signature=" + WebConfigurationManager.AppSettings["nsSignature"] + ", nlauth_role=" + WebConfigurationManager.AppSettings["nsRole"];
                request.Headers.Add("Authorization: NLAuth " + requestParams);

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = netSuiteDATA;
                    // string json = "{\"dateOfUpdate\": \"04/08/2016\", \"customerId\" : \"525\", \"jeDetail\" : [{\"accountId\": \"40270\",\"debitAmount\": \"500.00\",\"creditAmount\": \"\", \"memoText\": \"\",},{\"accountId\": \"40270\",\"debitAmount\": \"\",\"creditAmount\": \"500\", \"memoText\": \"\",},]}";

                    var serializer = new JavaScriptSerializer();

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (Stream streamReader = httpResponse.GetResponseStream())
                    {
                        using (StreamReader r = new StreamReader(streamReader))
                        {
                            var result = r.ReadToEnd();
                            string status = "";
                            string integrationId = "";
                            JavaScriptSerializer j = new JavaScriptSerializer();
                            netSuiteResult a = (netSuiteResult)j.Deserialize(result, typeof(netSuiteResult));
                            if (a.status == "Success")
                            {
                                status= "Success";
                                integrationId = a.intergationId;
                               
                            }
                            else
                            {
                                status = "Failed on LocationInternalId: " + locationInternalId + " ; Message: " + a.message;
                                integrationId = "";
                            }

                            storeResponse = storeDataToGLPostedTable(dsGLDetails, integrationId, status);
                            return storeResponse == "Success" ? status : "Failed due to an Internal DB Error.";
                        }
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        Console.WriteLine(text);
                        return "Failed";
                    }
                }
            }

        }

        private string postInventoriesToNetStuites(DataSet dsItemDetails)
        {
            
            try
            {
                var storeResponse = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(netSuiteURL_InventoryMovement);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "*/*";

                String requestParams = "nlauth_account=" + WebConfigurationManager.AppSettings["nsAccount"] + ", nlauth_email=" + WebConfigurationManager.AppSettings["nsEmail"] + ", nlauth_signature=" + WebConfigurationManager.AppSettings["nsSignature"] + ", nlauth_role=" + WebConfigurationManager.AppSettings["nsRole"];
                request.Headers.Add("Authorization: NLAuth " + requestParams);

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = netSuiteData_InventoryMovement;
                    
                    var serializer = new JavaScriptSerializer();

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)request.GetResponse();
                    using (Stream streamReader = httpResponse.GetResponseStream())
                    {
                        using (StreamReader r = new StreamReader(streamReader))
                        {
                            var result = r.ReadToEnd();
                            string status = "";
                            
                            JavaScriptSerializer j = new JavaScriptSerializer();
                            netSuiteResult a = (netSuiteResult)j.Deserialize(result, typeof(netSuiteResult));
                            if (a.status == "Success")
                            {

                                status = a.intergationId;
                            }
                            else
                            {
                                status = "Failed";
                            }

                            storeResponse = storeDataToInventoryMovementPostedTable(dsItemDetails, status);
                            return status;
                        }
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        Console.WriteLine(text);
                        return "Failed";
                    }
                }
            }
        }

        private string storeDataToGLPostedTable(DataSet dsGLDetails, string intergrationId, string status)
        {
            try
            {
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();
                foreach (DataRow dr in dsGLDetails.Tables[0].Rows)
                {
                    /*string sql = "INSERT INTO [dbo].[GLPostedEntries] (GLAccountEntries_Id, Posted_date, Posted_status, Posted_userId, Posted_IntegrationId) " +
                                                     "VALUES (@GLAccountEntries_Id, @Posted_date, @Posted_status, @Posted_userId, @Posted_IntegrationId )";
                    SqlCommand command = new SqlCommand(sql, adocn);
                    command.Parameters.AddWithValue("@GLAccountEntries_Id", dr["Id"]);
                    command.Parameters.AddWithValue("@Posted_date", DateTime.Today);
                    command.Parameters.AddWithValue("@Posted_status", status);
                    command.Parameters.AddWithValue("@Posted_userId", Convert.ToInt32(WebConfigurationManager.AppSettings["LoginUserID"]));
                    command.Parameters.AddWithValue("@Posted_IntegrationId", intergrationId);*/

                    string sql = "INSERT INTO [dbo].[GLPosted] (Subsidiary, ClosedOrderDate, Posting_Period, gl_currency, gl_account, gl_account_name, Debit, Credit, Memo, Class, Department, LocationInternalId, Posted_date, Posted_status, Posted_userId, Posted_IntegrationId, GLAccountEntries_Id) " +
                                            "VALUES (@Subsidiary, @ClosedOrderDate, @Posting_Period, @gl_currency, @gl_account, @gl_account_name, @Debit, @Credit, @Memo, @Class, @Department, @LocationInternalId, @Posted_date, @Posted_status, @Posted_userId, @Posted_IntegrationId, @GLAccountEntries_Id)";

                     SqlCommand command = new SqlCommand(sql, adocn);
                    command.Parameters.AddWithValue("@Subsidiary", dr["Subsidiary"]);
                    command.Parameters.AddWithValue("@ClosedOrderDate", dr["ClosedOrderDate"]);
                    command.Parameters.AddWithValue("@Posting_Period", dr["Posting_Period"]);
                    command.Parameters.AddWithValue("@gl_currency", dr["gl_currency"]);
                    command.Parameters.AddWithValue("@gl_account", dr["gl_account"]);
                    command.Parameters.AddWithValue("@gl_account_name", dr["gl_account_name"]);
                    command.Parameters.AddWithValue("@Debit", dr["DEBIT"]);
                    command.Parameters.AddWithValue("@Credit", dr["CREDIT"]);
                    command.Parameters.AddWithValue("@Memo", dr["Memo"]);
                    command.Parameters.AddWithValue("@Class", dr["Class"]);
                    command.Parameters.AddWithValue("@Department", dr["Department"]);
                    command.Parameters.AddWithValue("@LocationInternalId", Convert.ToInt32(dr["LocationInternalID"]));
                    command.Parameters.AddWithValue("@Posted_date", DateTime.Today);
                    command.Parameters.AddWithValue("@Posted_status", status);
                    command.Parameters.AddWithValue("@Posted_userId", Convert.ToInt32(System.Web.HttpContext.Current.Application["LoginUserID_"+ _SessionId]));
                   // command.Parameters.AddWithValue("@Posted_userId", Convert.ToInt32(WebConfigurationManager.AppSettings["LoginUserID"]));
                    command.Parameters.AddWithValue("@Posted_IntegrationId", intergrationId);
                    command.Parameters.AddWithValue("@GLAccountEntries_Id", dr["Id"]);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();

                    if (intergrationId != "")
                    {
                         string sql1 = "UPDATE [dbo].[GLAccountEntries] SET IsPosted='true' WHERE Id=@GLAccountEntries_Id";
                         SqlCommand command1 = new SqlCommand(sql1, adocn);
                         command1.Parameters.AddWithValue("@GLAccountEntries_Id", dr["Id"]);
                         command1.CommandType = CommandType.Text;
                         command1.ExecuteNonQuery();
                    }

                }
                adocn.Close();
                return "Success";
            }
            catch (SqlException e)
            {
                return "Failed";
            }
        }
        private string  storeDataToInventoryMovementPostedTable(DataSet dsItemDetails, string status){
            try
            {
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();
                foreach (DataRow dr in dsItemDetails.Tables[0].Rows)
                {
                    string sql = "INSERT INTO [dbo].[InventoryMovementPosted] (LocationID, Quantity, ItemName, ClosedOrderDate, SKU, UpdateType, Posted_date, Posted_status) " +
                                                      "VALUES (@LocationID, @Quantity, @ItemName, @ClosedOrderDate, @SKU, @UpdateType, @Posted_date, @Posted_status)";
                    SqlCommand command = new SqlCommand(sql, adocn);
                    command.Parameters.AddWithValue("@LocationID", dr["LocationID"]);
                    command.Parameters.AddWithValue("@Quantity", dr["Quantity"]);
                    command.Parameters.AddWithValue("@ItemName", dr["ItemName"]);
                    command.Parameters.AddWithValue("@ClosedOrderDate", dr["ClosedOrderDate"]);
                    command.Parameters.AddWithValue("@SKU", dr["SKU"]);
                    command.Parameters.AddWithValue("@UpdateType", dr["UpdateType"]);                    
                    command.Parameters.AddWithValue("@Posted_date", DateTime.Today);
                    command.Parameters.AddWithValue("@Posted_status", status);
                    
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();

                }
                adocn.Close();
                return "Success";
            }
            catch (SqlException e)
            {
                return "Failed";
            }
        }
        
        public void storeSelectedItems(string ClosedOrderDates, string LocationIDs, string LocationInternalIds, string Entries, string Status, string StartDate, string EndDate)
        {
            _SessionId = Request.QueryString["sessionId"];
            System.Web.HttpContext.Current.Application["ClosedOrderDates_"+_SessionId] = ClosedOrderDates;
            System.Web.HttpContext.Current.Application["LocationIds_" + _SessionId] = LocationIDs;
            System.Web.HttpContext.Current.Application["LocationInternalIDs_" + _SessionId] = LocationInternalIds;
            System.Web.HttpContext.Current.Application["Entries_" + _SessionId] = Entries;
            System.Web.HttpContext.Current.Application["Status_" + _SessionId] = Status;
            System.Web.HttpContext.Current.Application["StartDate_" + _SessionId] = StartDate;
            System.Web.HttpContext.Current.Application["EndDate_" + _SessionId] = EndDate;
        }

        public DataSet GetAllLocations()
        {
            SqlConnection adocn = new SqlConnection();
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();
            SqlDataAdapter scom = new SqlDataAdapter("SELECT LocationID, LocationName FROM Locations where Deleted=0 ORDER BY LocationName", adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsLocations = new DataSet();
            scom.Fill(dsLocations);
            adocn.Close();
            return dsLocations;
        }


        ///EDIT JOURNAL ENTRY
        ///
        public void storeSelectedJE(string JE_Id, string isNewJE)
        {
            _SessionId = Request.QueryString["sessionId"];
            System.Web.HttpContext.Current.Application["JE_Id_" + _SessionId] = JE_Id;
            System.Web.HttpContext.Current.Application["isNewJE_" + _SessionId] = isNewJE;

            
        }
        public DataSet GetJEDetailReader(string JE_Id){
            SqlConnection adocn = new SqlConnection();
            string sqlStr = "";
            adocn.ConnectionString = SQL_CONNECTION_STRING;
            adocn.Open();

            if (JE_Id != "")
            {
                sqlStr = "SELECT gld.*, l.LocationID, l.LocationName FROM dbo.GLAccountEntries gld  " +
                          "LEFT JOIN dbo.Locations_Mapping lm ON gld.LocationInternalId=lm.LocationInternalId " +
                          "LEFT JOIN dbo.Locations l " +
                          "ON lm.LocationID=l.LocationID " +
                          "WHERE gld.Id=" + JE_Id;
            }
           
            SqlDataAdapter scom = new SqlDataAdapter(sqlStr, adocn);
            scom.SelectCommand.CommandTimeout = 0;
            DataSet dsJEDetail = new DataSet();
            scom.Fill(dsJEDetail);
            adocn.Close();
            return dsJEDetail;
        }

        public string saveModifiedJE(string JE_Id, string Subsidiary, string ClosedOrderDate, string Posting_Period, string gl_currency, string gl_account, string gl_account_name, string debit, string credit, string Memo, string Class, string LocationInternalId)
        {
            _SessionId = Request.QueryString["sessionId"];
            try
            {
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();
                string sql = "UPDATE [dbo].[GLAccountEntries] SET Subsidiary=@Subsidiary, ClosedOrderDate=@ClosedOrderDate, Posting_Period=@Posting_Period, gl_currency=@gl_currency, gl_account= @gl_account, gl_account_name=@gl_account_name, DEBIT= @debit, CREDIT=@credit, Memo=@Memo, Class=@Class, LocationInternalId=@LocationInternalId, ModifiedDate=@ModifiedDate, ModifiedBy=@ModifiedBy  WHERE Id=@JE_Id";
                SqlCommand command = new SqlCommand(sql, adocn);
                command.Parameters.AddWithValue("@JE_Id", JE_Id);
                command.Parameters.AddWithValue("@Subsidiary", Subsidiary);
                command.Parameters.AddWithValue("@ClosedOrderDate", ClosedOrderDate);
                command.Parameters.AddWithValue("@Posting_Period", Posting_Period);
                command.Parameters.AddWithValue("@gl_currency", gl_currency);
                command.Parameters.AddWithValue("@gl_account", gl_account);
                command.Parameters.AddWithValue("@gl_account_name", gl_account_name);
                command.Parameters.AddWithValue("@debit", debit);
                command.Parameters.AddWithValue("@credit", credit);
                command.Parameters.AddWithValue("@Memo", Memo);
                command.Parameters.AddWithValue("@Class", Class);
                command.Parameters.AddWithValue("@LocationInternalId", LocationInternalId);
                command.Parameters.AddWithValue("@ModifiedDate", DateTime.Today);
                command.Parameters.AddWithValue("@ModifiedBy", System.Web.HttpContext.Current.Application["LoginUserID_" + _SessionId]);
                //command.Parameters.AddWithValue("@ModifiedBy", WebConfigurationManager.AppSettings["LoginUserID"]);

                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();


                adocn.Close();
                return "Success";
            }
            catch (SqlException e)
            {
                return "Failed";
            }
        }

        public string saveNewJE(string Subsidiary, string ClosedOrderDate, string Posting_Period, string gl_currency, string gl_account, string gl_account_name, string debit, string credit, string Memo, string Class, string LocationInternalId)
        {
            _SessionId = Request.QueryString["sessionId"];
            try
            {
                SqlConnection adocn = new SqlConnection();
                adocn.ConnectionString = SQL_CONNECTION_STRING;
                adocn.Open();
                string sql = "INSERT INTO [dbo].[GLAccountEntries] (Subsidiary, ClosedOrderDate, Posting_Period, gl_currency, gl_account, gl_account_name, DEBIT, CREDIT, Memo, Class, LocationInternalId, ModifiedDate, CreatedDate, ModifiedBy, CreatedBy)" +
                    "VALUES (@Subsidiary, @ClosedOrderDate, @Posting_Period, @gl_currency, @gl_account, @gl_account_name, @DEBIT, @CREDIT, @Memo, @Class, @LocationInternalId, @ModifiedDate, @CreatedDate, @ModifiedBy, @CreatedBy)";
                SqlCommand command = new SqlCommand(sql, adocn);
                command.Parameters.AddWithValue("@Subsidiary", Subsidiary);
                command.Parameters.AddWithValue("@ClosedOrderDate", ClosedOrderDate);
                command.Parameters.AddWithValue("@Posting_Period", Posting_Period);
                command.Parameters.AddWithValue("@gl_currency", gl_currency);
                command.Parameters.AddWithValue("@gl_account", gl_account);
                command.Parameters.AddWithValue("@gl_account_name", gl_account_name);
                command.Parameters.AddWithValue("@debit", debit);
                command.Parameters.AddWithValue("@credit", credit);
                command.Parameters.AddWithValue("@Memo", Memo);
                command.Parameters.AddWithValue("@Class", Class);
                command.Parameters.AddWithValue("@LocationInternalId", LocationInternalId);
                command.Parameters.AddWithValue("@ModifiedDate", DateTime.Today);
                command.Parameters.AddWithValue("@ModifiedBy", System.Web.HttpContext.Current.Application["LoginUserID_" + _SessionId]);
                //command.Parameters.AddWithValue("@ModifiedBy", WebConfigurationManager.AppSettings["LoginUserID"]);
                command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                command.Parameters.AddWithValue("@CreatedBy", System.Web.HttpContext.Current.Application["LoginUserID_" + _SessionId]);
                //command.Parameters.AddWithValue("@CreatedBy", WebConfigurationManager.AppSettings["LoginUserID"]);
                

                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();


                adocn.Close();
                return "Success";
            }
            catch (SqlException e)
            {
                return "Failed";
            }

         
        }

    }
}
