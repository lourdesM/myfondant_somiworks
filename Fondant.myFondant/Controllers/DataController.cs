﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Ext.Net;
using Ext.Net.MVC;
using Fondant.myFondant.Web.Models;
using Fondant.myFondant.Web.Code;
using Fondant.myFondant.Web.Controllers;
using Fondant.myFondant.Models;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace Fondant.myFondant.Web.Controllers
{
    [HandleError]
    public class DataController : BaseDataController
    {
        private class Users
        {
            public string ID { get; set; }
            public string ClientId { get; set; }
            public string UserId { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string Suffix { get; set; }
            public string UserName { get; set; }
            public string LoweredEmail { get; set; }
            public string CreatedDate { get; set; }
            public string LastLoginDate { get; set; }
            public string IsActive { get; set; }

        }
                
        public StoreResult GetUserLocations(int UserId)
        {
            var query = (from c in this.DBContext.vw_UserLocations
                         where c.ID == UserId
                         orderby c.LocationName
                         select new
                         {
                             c.LocationName,
                             c.ZipCode,
                             c.ASPNETUserID
                         });
            
            return new StoreResult(query);
        }
        //added
        public String GetZipCode(int UserId, string locName)
        {
            var query = (from c in this.DBContext.vw_UserLocations
                         where c.ID == UserId && locName == c.LocationName
                         orderby c.LocationName
                         select new
                         {
                             c.LocationName,
                             c.ZipCode,
                             c.ASPNETUserID
                         });

            String[] temp = query.FirstOrDefault().ToString().Split(',');
            for (var ctr = 0; ctr < temp.Length; ctr++)
            {
                if (temp[ctr].Contains("ZipCode"))
                {
                    String[] tempContainer = temp[ctr].Split('=');
                    tempContainer[1] = tempContainer[1].Replace("{", " ");
                    tempContainer[1] = tempContainer[1].Replace("}", " ");
                    tempContainer[1] = tempContainer[1].TrimStart().ToString();
                    return tempContainer[1].TrimEnd().ToString();
                }
            }
            return "";
        }
       //added
        public String GetASPNETUserID(int UserId)
        {          
            var query = (from c in this.DBContext.vw_UserLocations
                         where c.ID == UserId
                         select new
                         {
                             c.ASPNETUserID
                         });
           return query.FirstOrDefault().ASPNETUserID.ToString();
           
           // return new StoreResult(query);
        }

        // GET: /Data/GetUser

        public StoreResult GetUser(string ASPNETUserId)
        {
            MembershipUser mUpd = Membership.GetUser(new Guid(ASPNETUserId));

            string strEmail = mUpd.Email;

            var query = (from c in this.DBContext.vwUserDetails
                         where c.ASPNETUserID.ToString() == ASPNETUserId
                         select new
                         {
                             c.ClientID,
                             c.ASPNETUserID,
                             c.FirstName,
                             c.MiddleName,
                             c.LastName,
                             c.Suffix,
                             c.CreatedDate,
                             c.ModifiedDate,
                             c.IsActive,
                             c.ClientName,
                             Email = strEmail
                         });

            return new StoreResult(query);
        }
        
        //added for testing

        public String GetUser2(string ASPNETUserId, string field)
        {
            MembershipUser mUpd = Membership.GetUser(new Guid(ASPNETUserId));

            string strEmail = mUpd.Email;

            var query = (from c in this.DBContext.Users
                         where c.ASPNETUserID.ToString() == ASPNETUserId
                         select new
                         {
                             c.ClientID,
                             c.ASPNETUserID,
                             c.FirstName,
                             c.MiddleName,
                             c.LastName,
                             c.Suffix,
                             c.CreatedDate,
                             c.ModifiedDate,
                             c.IsActive,
                             Email = strEmail
                         });
            String[] temp = query.FirstOrDefault().ToString().Split(',');
            for(var ctr = 0; ctr < temp.Length; ctr++){
                if(temp[ctr].Contains(field)){
                    String[] tempContainer = temp[ctr].Split('=');
                    tempContainer[1] = tempContainer[1].Replace("{", " ");
                    tempContainer[1] = tempContainer[1].Replace("}", " ");
                    tempContainer[1] = tempContainer[1].TrimStart().ToString();
                    return tempContainer[1].TrimEnd().ToString();
                    

                }
            }
            return "";
            //return query.FirstOrDefault().[field].toString();
        }
        //end of added
        // GET: /Data/GetUsers

        public StoreResult GetUsers(int ClientId)
        {
            var query = (from c in this.DBContext.vwUsers
                         where c.ClientId == ClientId
                         select new
                         {
                             c.ClientId,
                             c.UserId,
                             c.FirstName,
                             c.MiddleName,
                             c.LastName,
                             c.Suffix,
                             c.UserName,
                             c.LoweredEmail,
                             c.CreateDate,
                             c.LastLoginDate,
                             c.IsActive
                         });

            return new StoreResult(query);
        }

        public Object GetUsers3(int ClientId)
        {
            var query = (from c in this.DBContext.vwUsers
                         where c.ClientId == ClientId
                         select new
                         {
                             c.ID,
                             c.ClientId,
                             c.UserId,
                             c.FirstName,
                             c.MiddleName,
                             c.LastName,
                             c.Suffix,
                             c.UserName,
                             c.LoweredEmail,
                             c.CreateDate,
                             c.LastLoginDate,
                             c.IsActive
                         });

            //return new StoreResult(query);
           // String[] temp = query.FirstOrDefault().ToString().Split(',');
           var temp2 = query.ToList();

            //for (int i = 0; i < temp2.Count(); i++) {
            //    var k = temp2.ElementAt(i);
            //}
           return temp2;


        }

        public void GetAllUsersByClientID(int ClientId)
        {
            var data = "";
            try
            {
           
                Object temp = GetUsers3(ClientId);
                var jsonUserDetail = new JavaScriptSerializer().Serialize(temp);
                JavaScriptSerializer j = new JavaScriptSerializer();
                var users = j.Deserialize<Users[]>(jsonUserDetail);

                var paramResult = "[";

                ParameterCollection parameters = new ParameterCollection();
                int i = 1;
                foreach (Users user in users)
                {
                
                    parameters["ID"] = user.ID;
                    parameters["ClientId"] = user.ClientId;
                    parameters["UserId"] = user.UserId;
                    parameters["FirstName"] = user.FirstName;
                    parameters["MiddleName"] = user.MiddleName;
                    parameters["LastName"] = user.LastName;
                    parameters["Suffix"] = user.Suffix;
                    parameters["UserName"] = user.UserName;
                    parameters["LoweredEmail"] = user.LoweredEmail;
                    parameters["CreateDate"] = user.CreatedDate;
                    parameters["LastLoginDate"] = user.LastLoginDate;
                    parameters["IsActive"] = user.IsActive;
                    paramResult += parameters.ToJson() + ",";
                    i++;
                }
                if (paramResult.Length > 1)
                {
                    data = paramResult.Substring(0, paramResult.Length - 1) + "]";
                }
                else
                {
                    data = "";
                };
            }
            catch (InvalidOperationException e)
            {
                data = e.Message;
            }
           
            new DirectResponse { Result = data }.Return();
        }

        public String GetUsers2(String email, String username, String field)
        {
            try
            {
                var query = (from c in this.DBContext.vwUsers
                             where c.LoweredEmail == email
                             && c.UserName == username
                             select new
                             {

                                 c.LongName,
                                 c.AccountNumber,
                                 c.ID,
                                 c.ClientId,
                                 c.UserId,
                                 c.FirstName,
                                 c.MiddleName,
                                 c.LastName,
                                 c.Suffix,
                                 c.UserName,
                                 c.LoweredEmail,
                                 c.CreateDate,
                                 c.LastLoginDate,
                                 c.IsActive
                             });
                if(query.Count() > 0){
                        String[] temp = query.FirstOrDefault().ToString().Split(',');
                        for (var ctr = 0; ctr < temp.Length; ctr++)
                        {
                            if (temp[ctr].Contains(field))
                            {
                                String[] tempContainer = temp[ctr].Split('=');
                                tempContainer[1] = tempContainer[1].Replace("{", " ");
                                tempContainer[1] = tempContainer[1].Replace("}", " ");
                                tempContainer[1] = tempContainer[1].TrimStart().ToString();
                                return tempContainer[1].TrimEnd().ToString();
                            }
                        }
                }
                else{
                         return "";
                }
                return "";
            }
            catch (MembershipCreateUserException e)
            {
                return "";
            }
        }

        // SET: /Data/SaveUser

        //public FormPanelResult SaveUser() 
        //{
        //    FormPanelResult response = new FormPanelResult();
        //    return response;
        //}
        
        [DirectMethod]
        public FormPanelResult SaveUser(string id, FormCollection values)
        {
            FormPanelResult response = new FormPanelResult();
            bool isNew = false;

            try
            {
                User user; 

                if (string.IsNullOrEmpty(id))
                {
                    // check for username
                    if (string.IsNullOrEmpty(values["UserName"]))
                    {
                        response.Success = false;
                        response.Errors.Add(new FieldError("UserName", "Username is required."));
                        return response;
                    }

                    // check for username
                    if (string.IsNullOrEmpty(values["Password"]))
                    {
                        response.Success = false;
                        response.Errors.Add(new FieldError("Password", "Password is required."));
                        return response;
                    }

                    // check if password and confirmpassword matches
                    if (values["Password"] != values["ConfirmPassword"])
                    {
                        response.Success = false;
                        response.Errors.Add(new FieldError("Password", "The passwords don't match"));
                        return response;
                    }

                    // create membership for user in membership provider
                    MembershipUser newUser;
                    newUser = Membership.CreateUser(values["UserName"], values["Password"]);

                    user = new User();
                    user.ASPNETUserID = new Guid(newUser.ProviderUserKey.ToString());
                    user.ClientID = Convert.ToInt32(values["ClientID"]);
                    user.CreatedDate = DateTime.UtcNow;
                    user.ModifiedDate = DateTime.UtcNow;
                    user.IsActive = true;
                    this.DBContext.Users.InsertOnSubmit(user);

                    isNew = true;
                }
                else
                {
                    user = (from c in this.DBContext.Users where c.ASPNETUserID.ToString() == id select c).First();
                    user.ClientID = Convert.ToInt32(values["ClientID"]); 
                    user.ASPNETUserID = new Guid(values["ASPNETUserID"]);
                    user.FirstName = values["FirstName"];
                    user.MiddleName = values["MiddleName"];
                    user.LastName = values["LastName"];
                    user.Suffix = values["Suffix"];
                    user.ModifiedDate = DateTime.UtcNow;

                    MembershipUser mUpd = Membership.GetUser(new Guid(id));
                    mUpd.Email = values["Email"];

                    if (!string.IsNullOrEmpty(values["IsActive"]))
                    {
                        user.IsActive = true;
                        mUpd.IsApproved = true;
                    }
                    else
                    {
                        user.IsActive = false;
                        mUpd.IsApproved = false;
                    }
                    Membership.UpdateUser(mUpd);

                    // send welcome email if new user
                    if (values["IsNewUser"] == "true")
                    {
                        String strEmail = mUpd.Email;
                        String strSubject = "READY NUMB3R5 for " + values["ClientName"] + " New User Welcome";
                        String strMessage;

                        myFondantUtilities ruSendMail = new myFondantUtilities();
                        strMessage = ruSendMail.CreateNewUserMessage(values["FirstName"], mUpd.UserName, mUpd.GetPassword(), values["ClientName"]);
                        ruSendMail.SendEmail(strEmail, strSubject, strMessage);
                    }
                }

                this.DBContext.SubmitChanges();

                if (isNew)
                {
                    response.ExtraParams["newID"] = user.ASPNETUserID.ToString();
                    System.Web.HttpContext.Current.Application["viewdata_UserId"] = user.ID.ToString();
                    System.Web.HttpContext.Current.Application["viewdata_ASPNETUserId"] = user.ASPNETUserID.ToString();
                    System.Web.HttpContext.Current.Application["viewdata_IsNewUser"] = "true";
                }
            }
            catch (MembershipCreateUserException e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = GetErrorMessage(e.StatusCode);
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ExtraParams["msg"] = e.ToString();
            }

            return response;
        }
        public void save_user() 
        {
           // string sample = "hi";
        }
        //public FormPanelResult SaveUser(string id, string uname, string pword, string cPword)
        //{
        //    FormPanelResult response = new FormPanelResult();
            
   
        //    bool isNew = false;

        //    try
        //    {
        //        User user; //might need to change this part if there is an error

        //        if (string.IsNullOrEmpty(id))
        //        {
        //            // check for username
        //            if (string.IsNullOrEmpty(uname))
        //            {
        //                response.Success = false;
        //                response.Errors.Add(new FieldError("UserName", "Username is required."));
        //                return response;
        //            }

        //            // check for username
        //            if (string.IsNullOrEmpty(pword))
        //            {
        //                response.Success = false;
        //                response.Errors.Add(new FieldError("Password", "Password is required."));
        //                return response;
        //            }

        //            // check if password and confirmpassword matches
        //            if (pword != cPword)
        //            {
        //                response.Success = false;
        //                response.Errors.Add(new FieldError("Password", "The passwords don't match"));
        //                return response;
        //            }

        //            // create membership for user in membership provider
        //            MembershipUser newUser;
        //            newUser = Membership.CreateUser(uname, pword);

        //            user = new User();
        //            user.ASPNETUserID = new Guid(newUser.ProviderUserKey.ToString());
        //            user.ClientID = Convert.ToInt32(this.ViewData["ClientID"]);
        //            user.CreatedDate = DateTime.UtcNow;
        //            user.ModifiedDate = DateTime.UtcNow;
        //            user.IsActive = true;
        //            this.DBContext.Users.InsertOnSubmit(user);

        //            isNew = true;
        //        }              

        //        this.DBContext.SubmitChanges();

        //        if (isNew)
        //        {
        //            response.ExtraParams["newID"] = this.ViewData["ASPNetUserID"].ToString();
        //        }
        //    }
        //    catch (MembershipCreateUserException e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = GetErrorMessage(e.StatusCode);
        //    }
        //    catch (Exception e)
        //    {
        //        response.Success = false;
        //        response.ExtraParams["msg"] = e.ToString();
        //    }

        //    return response;
        //}

        public StoreResult UserLocationsGet(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }

            var query = this.DBContext.spUserLocationsGet(UserIdGuid);
            return new StoreResult(query);
        }

        public StoreResult UserLocationsSet()
        {
            StoreResult StoreResult = new StoreResult(); //StoreResponseFormat.Save);

            try
            {
                StoreDataHandler dataHandler = new StoreDataHandler(HttpContext.Request["data"]);
                //StoreDataHandler dataHandler = new StoreDataHandler(System.Web.HttpContext.Current.Request["data"]);
                ChangeRecords<UserInLocations> data = dataHandler.BatchObjectData<UserInLocations>(); // dataHandler.ObjectData<UserInLocations>();

                foreach (UserInLocations dat in data.Updated)
                {
                    if (dat.Selected)
                    {

                        UserLocation uloc = new UserLocation
                        
                        {
                            UserID = dat.UserId,
                            LocationID = dat.LocationId
                        };
                        this.DBContext.UserLocations.InsertOnSubmit(uloc);
                    }
                    else
                    {
                        var ul = from c in this.DBContext.UserLocations where c.UserID == dat.UserId && c.LocationID == dat.LocationId select c;
                        foreach (var del in ul)
                        {
                            this.DBContext.UserLocations.DeleteOnSubmit(del);
                        }
                    }
                }

                this.DBContext.SubmitChanges();
            }
            catch (Exception e)
            {
                StoreResult.Success = false; // .SaveResponse.Success = false;
                StoreResult.Message = e.Message;
            }

            return StoreResult;
        }

        public StoreResult UserLocationsSet1(string UserId, string LocationId, string LocationName, string Selected)
        {
            StoreResult StoreResult = new StoreResult(); //StoreResponseFormat.Save);

            try
            {
               // StoreDataHandler dataHandler = new StoreDataHandler(HttpContext.Request["data"]);
                //StoreDataHandler dataHandler = new StoreDataHandler(System.Web.HttpContext.Current.Request["data"]);
               // ChangeRecords<UserInLocations> data = dataHandler.BatchObjectData<UserInLocations>(); // dataHandler.ObjectData<UserInLocations>();

                //foreach (UserInLocations dat in data.Updated)
                //{
                    if (Convert.ToBoolean(Selected))
                    {

                        UserLocation uloc = new UserLocation

                        {
                            UserID = Convert.ToInt32(UserId),
                            LocationID = Convert.ToInt32(LocationId)
                        };
                        this.DBContext.UserLocations.InsertOnSubmit(uloc);
                    }
                    else
                    {
                        var ul = from c in this.DBContext.UserLocations where c.UserID == Convert.ToInt32(UserId) && c.LocationID == Convert.ToInt32(LocationId) select c;
                        foreach (var del in ul)
                        {
                            this.DBContext.UserLocations.DeleteOnSubmit(del);
                        }
                    }
                //}

                this.DBContext.SubmitChanges();
            }
            catch (Exception e)
            {
                StoreResult.Success = false; // .SaveResponse.Success = false;
                StoreResult.Message = e.Message;
            }

            return StoreResult;
        }

        public StoreResult UserRolesGet(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }

           // var query = this.DBContext.spUserRolesGet(UserIdGuid);
            var query = (from c in this.DBContext.spUserRolesGet(UserIdGuid)
                         

                         select new
                         {
                             c.UserId,
                             c.RoleName,
                             c.Selected
                         });
            return new StoreResult(query);
        }

        //this will get only the admin and IT role
        public string UserRolesGet2(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }
           
           // var query = this.DBContext.spUserRolesGet(UserIdGuid);

            var query = (from c in this.DBContext.spUserRolesGet(UserIdGuid)
                         where 
                         (c.RoleName == "Corporate IT"  ||
                         c.RoleName == "Corporate Administrator") && 
                         c.Selected == 1

                         select new
                         {
                             c.RoleName
                         })
                         .ToList();

           // String[] temp = query.FirstOrDefault().ToString().Split(',');

           // return temp[0].ToString();
            if (query.Any())
            {

                String[] temp = query.FirstOrDefault().ToString().Split(',');
                for (var ctr = 0; ctr < temp.Length; ctr++)
                {
                    if (temp[ctr].Contains("RoleName"))
                    {
                        String[] tempContainer = temp[ctr].Split('=');
                        tempContainer[1] = tempContainer[1].Replace("{", " ");
                        tempContainer[1] = tempContainer[1].Replace("}", " ");
                        tempContainer[1] = tempContainer[1].TrimStart().ToString();
                        return tempContainer[1].TrimEnd().ToString();
                    }
                }
                return "";
            }
            else
            {
                return "";
            }
           
        }
        //this will get only the accounting
        public string UserRolesGetAccountingForDryBar(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }

          

            var query = (from c in this.DBContext.spUserRolesGet(UserIdGuid)
                         where
                         (c.RoleName == "Accounting") &&
                         c.Selected == 1

                         select new
                         {
                             c.RoleName
                         })
                         .ToList();

         
            if (query.Any())
            {

                String[] temp = query.FirstOrDefault().ToString().Split(',');
                for (var ctr = 0; ctr < temp.Length; ctr++)
                {
                    if (temp[ctr].Contains("RoleName"))
                    {
                        String[] tempContainer = temp[ctr].Split('=');
                        tempContainer[1] = tempContainer[1].Replace("{", " ");
                        tempContainer[1] = tempContainer[1].Replace("}", " ");
                        tempContainer[1] = tempContainer[1].TrimStart().ToString();
                        return tempContainer[1].TrimEnd().ToString();
                    }
                }
                return "";
            }
            else
            {
                return "";
            }

        }

        //this will get only the accounting admin
        public string UserRolesGetAccountingAdminForDryBar(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }



            var query = (from c in this.DBContext.spUserRolesGet(UserIdGuid)
                         where
                         (c.RoleName == "Accounting Admin") &&
                         c.Selected == 1

                         select new
                         {
                             c.RoleName
                         })
                         .ToList();


            if (query.Any())
            {

                String[] temp = query.FirstOrDefault().ToString().Split(',');
                for (var ctr = 0; ctr < temp.Length; ctr++)
                {
                    if (temp[ctr].Contains("RoleName"))
                    {
                        String[] tempContainer = temp[ctr].Split('=');
                        tempContainer[1] = tempContainer[1].Replace("{", " ");
                        tempContainer[1] = tempContainer[1].Replace("}", " ");
                        tempContainer[1] = tempContainer[1].TrimStart().ToString();
                        return tempContainer[1].TrimEnd().ToString();
                    }
                }
                return "";
            }
            else
            {
                return "";
            }

        }

        public string UserRolesGetSSRSReportForDryBar(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }

            var query = (from c in this.DBContext.spUserRolesGet(UserIdGuid)
                         where
                         (c.RoleName == "Run only specified SSRS Reports") &&
                         c.Selected == 1

                         select new
                         {
                             c.RoleName
                         })
                         .ToList();


            if (query.Any())
            {

                String[] temp = query.FirstOrDefault().ToString().Split(',');
                for (var ctr = 0; ctr < temp.Length; ctr++)
                {
                    if (temp[ctr].Contains("RoleName"))
                    {
                        String[] tempContainer = temp[ctr].Split('=');
                        tempContainer[1] = tempContainer[1].Replace("{", " ");
                        tempContainer[1] = tempContainer[1].Replace("}", " ");
                        tempContainer[1] = tempContainer[1].TrimStart().ToString();
                        return tempContainer[1].TrimEnd().ToString();
                    }
                }
                return "";
            }
            else
            {
                return "";
            }

        }

        public StoreResult UserRolesSet()
        {
            StoreResult StoreResult = new StoreResult(); //StoreResponseFormat.Save);

            try
            {
                StoreDataHandler dataHandler = new StoreDataHandler(HttpContext.Request["data"]);
                //StoreDataHandler dataHandler = new StoreDataHandler(System.Web.HttpContext.Current.Request["data"]);
                ChangeRecords<UserRoles> data = dataHandler.BatchObjectData<UserRoles>();

                MembershipUser mUpd = Membership.GetUser(new Guid(data.Updated[0].UserId));
                string uname = mUpd.UserName;

                foreach (UserRoles dat in data.Updated)
                {
                    if (dat.Selected)
                    {
                        Roles.AddUserToRole(uname, dat.RoleName);
                    }
                    else
                    {
                        Roles.RemoveUserFromRole(uname, dat.RoleName);
                    }
                }

            }
            catch (Exception e)
            {
                StoreResult.Success = false;
                StoreResult.Message = e.Message;
            }

            return StoreResult;
        }

        public StoreResult UserRolesSet1(string UserId, string RoleName, string Selected)
        {
            StoreResult StoreResult = new StoreResult();

            try
            {
               //** StoreDataHandler dataHandler = new StoreDataHandler(HttpContext.Request["data"]);
                //StoreDataHandler dataHandler = new StoreDataHandler(System.Web.HttpContext.Current.Request["data"]);
               //** ChangeRecords<UserRoles> data = dataHandler.BatchObjectData<UserRoles>();

                MembershipUser mUpd = Membership.GetUser(new Guid(UserId));
                string uname = mUpd.UserName;

              // foreach (UserRoles dat in data.Updated)
               // {
                    if (Convert.ToBoolean(Selected))
                    {
                        Roles.AddUserToRole(uname, RoleName);
                    }
                    else
                    {
                        Roles.RemoveUserFromRole(uname, RoleName);
                    }
                //}

            }
            catch (Exception e)
            {
                StoreResult.Success = false;
                StoreResult.Message = e.Message;
            }
            return StoreResult;
        }
        public StoreResult UserInSSRSReportsGet(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }


            var query = this.DBContext.spUserInSSRSReportsGet(UserIdGuid);
            return new StoreResult(query);
        }
        public List<UserInSSRSReports> UserInSSRSReportsGetSelected(string ASPNETUserId)
        {
            Guid UserIdGuid = new Guid();

            if (!string.IsNullOrEmpty(ASPNETUserId))
            {
                UserIdGuid = new Guid(ASPNETUserId);
            }

            List<UserInSSRSReports> query = (from c in this.DBContext.spUserInSSRSReportsGet(UserIdGuid)
                                             where c.Selected == 1
                                             select new UserInSSRSReports
                                             {
                                                 UserId = c.UserId,
                                                 SSRS_ReportsId = c.SSRS_ReportsId,
                                                 ReportName = c.ReportName,
                                                 Selected = c.Selected
                                             }).ToList();

            return query;

        }
        public StoreResult UserSSRSReportsSet(string UserId, string SSRS_ReportsId, string ReportName, string Selected)
        {

            StoreResult StoreResult = new StoreResult(); 

            try
            {

                if (Convert.ToBoolean(Selected))
                {

                    UsersInSSRSReports uloc = new UsersInSSRSReports

                    {
                        UserID = Convert.ToInt32(UserId),
                        SSRS_ReportsId = Convert.ToInt32(SSRS_ReportsId)
                    };
                    this.DBContext.UsersInSSRSReports.InsertOnSubmit(uloc);
                }
                else
                {
                    var ul = from c in this.DBContext.UsersInSSRSReports where c.UserID == Convert.ToInt32(UserId) && c.SSRS_ReportsId == Convert.ToInt32(SSRS_ReportsId) select c;
                    foreach (var del in ul)
                    {
                        this.DBContext.UsersInSSRSReports.DeleteOnSubmit(del);
                    }
                }
               

                this.DBContext.SubmitChanges();
            }
            catch (Exception e)
            {
                StoreResult.Success = false; 
                StoreResult.Message = e.Message;
            }

            return StoreResult;

        }
        public class UserRoles
        {
            public string UserId { get; set; }
            public string RoleName { get; set; }
            public bool Selected { get; set; }
        }

        public class UserInLocations
        {
            public int UserId { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }
            public bool Selected { get; set; }
        }
        public class UserInSSRSReports
        {
            public int? UserId { get; set; }
            public int SSRS_ReportsId { get; set; }
            public string ReportName { get; set; }
            public int Selected { get; set; }
        }
        public string GetErrorMessage(MembershipCreateStatus status)
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid.";

                case MembershipCreateStatus.ProviderError:
                    return "System error encountered.  Please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "User creation request has been canceled.";

                default:
                    return "An unknown error occurred.  Please contact your system administrator.";
            }
        }

        public DirectResponse ResetPassword(string userName, string userEmailAdd)
        {
            DirectResponse response = new DirectResponse();
            ParameterCollection parameters = new ParameterCollection();
           

            string strPwd = string.Empty;
            string strMsg = string.Empty;
            try
            {
               
                    // find out first if User's PasswordFormat in DB is 1, if so, update to 2 before calling ResetPassword

                    strPwd = Membership.Provider.ResetPassword(userName, string.Empty);
                    strMsg = "New password was sent to " + userName + ".";

                    myFondantUtilities myFUtils = new myFondantUtilities();
                    string strEmailMsg = myFUtils.CreateResetPasswordMessage(userName, strPwd);

                    string mUser = userEmailAdd; //modified

                    if (mUser == string.Empty || mUser == "")
                    {
                        strMsg = "No email was found under the username provided.  Please contact your company administrator.";
                        parameters["messageFailed"] = "No email was found under the username provided.  Please contact your company administrator.";
                        response.Success = false;
                    }
                    else
                    {

                        myFUtils.SendEmail(mUser, "Your SomiWorks Password", strEmailMsg); //modified
                        parameters["messageSuccess"] = "Success";
                        response.Success = true;
                    }
                
            }
            catch (Exception ex)
            {
                strMsg = string.Concat("The password could not be reset at this time.  ", ex.Message);

                parameters["messageFailed"] = "The password could not be reset at this time. " + ex.Message;
                response.Success = false;
            }

           

            response.ExtraParamsResponse = parameters.ToJson();

            return response;
            
        }
    }
}
