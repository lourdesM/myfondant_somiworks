﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Fondant.myFondant.Web.Controllers
{
    public class RSSProxyController : Controller
    {
        //
        // GET: /RSSProxy/Feed

        public ActionResult Feed(string feedUrl, string zipCode)
        {
            string uri = Server.UrlDecode(ConfigurationManager.AppSettings[feedUrl]); 
            if (zipCode != string.Empty || zipCode != "")
            {
                uri = uri + zipCode;
            }
            XmlReader xr = XmlReader.Create(uri);

            SyndicationFeed feed = SyndicationFeed.Load(xr);

            List<SyndicationItem> items = new List<SyndicationItem>();
            SyndicationItem sitem = null;
            foreach (var item in feed.Items)
            {
                sitem = new SyndicationItem
                {
                    Title = new TextSyndicationContent(item.Title.Text),
                    Content = item.Content,
                    PublishDate = item.PublishDate
                };
                items.Add(item);
            }

            feed.Items = items;

            return new RssActionResult() { Feed = feed };
        }
        
        public abstract class ActionResult
        {
            //protected ActionResult();

            public abstract void ExecuteResult(ControllerContext context);
        }

        public class RssActionResult : ActionResult
        {
            public SyndicationFeed Feed { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.ContentType = "application/rss+xml";

                Rss20FeedFormatter rssFormatter = new Rss20FeedFormatter(Feed);
                using (XmlWriter writer = XmlWriter.Create(context.HttpContext.Response.Output))
                {
                    rssFormatter.WriteTo(writer);
                }
            }
        }
    }
}
