﻿using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace Fondant.myFondant.Web.Controllers
{
    public class GLUploadSystemController : Controller
    {
        //
        // GET: /GLUploadSystem/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GLUploadPage()
        {
            return this.View();
        }

        public ActionResult GLDetailReport()
        {
            return this.View();
        }

        public ActionResult FranchiseSalesReport()
        {
            return this.View();
        }

        public ActionResult JournalEntry()
        {
            return this.View();
        }

        public ActionResult TestExtControld()
        {
            return this.View();
        }

      

    }
}
