﻿using System.Web.Mvc;
using Fondant.myFondant.Models;
using Fondant.myFondant.Web.Models;


namespace Fondant.myFondant.Web.Controllers
{
    public abstract class BaseDataController : Controller
    {
        public BaseDataController()
        {
            this.db = new ReadyMembershipDataContext();
        }

        ReadyMembershipDataContext db;

        public ReadyMembershipDataContext DBContext
        {
            get { return this.db; }
        }
    }
}
