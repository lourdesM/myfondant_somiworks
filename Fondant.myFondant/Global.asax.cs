﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Fondant.myFondant.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

       protected void Application_AuthenticateRequest(object sender, System.EventArgs e)
        {
            string url = HttpContext.Current.Request.RawUrl.ToLower();
            //string url = System.Web.HttpContext.Current.Request.RawUrl.ToLower();
     
            if (url.Contains("ext.axd"))
            {
                HttpContext.Current.SkipAuthorization = true;
                //System.Web.HttpContext.Current.SkipAuthorization = true;              
            }
            else if (url.Contains("returnurl=/default.aspx") || url.Contains("returnurl=%2fdefault.aspx"))
            {
                Response.Redirect(url.Replace("returnurl=/default.aspx", "r=/").Replace("returnurl=%2fdefault.aspx", "r=/"));
            }
        }

        //protected void Application_AuthenticateRequest(object sender, System.EventArgs e)
        //{
        //    // skip authenticating all ext.axd embedded resources (.js, .css, images)
        //    if (HttpContext.Current.Request.FilePath.EndsWith("/ext.axd"))
        //    {
        //        HttpContext.Current.SkipAuthorization = true;
        //    }
        //}
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{exclude}/{extnet}/ext.axd");
            routes.IgnoreRoute("{*path}", new { path = @"FileDownload\/(.*)" });
           // routes.RouteExistingFiles = true; //added
            

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults

                //change this part
            );

            /* routes.MapRoute(
              "Default", // Route name
              "{Account}/{ClientLogInNow}/{id}", // URL with parameters
              new { controller = "Account", action = "ClientLogInNow", id = UrlParameter.Optional } // Parameter defaults
             
              //change this part
          );*/

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
        }

       protected void Session_Start(Object sender, EventArgs e)
        {
            int temp = 1;
            HttpContext.Current.Session.Add("User_Email", temp);
            if (System.Web.HttpContext.Current.Application["SessionId"] == null)
            {
                HttpContext.Current.Session.Add("SessionId", Session.SessionID);
            }
                                   
            
        }
       
    }
}