﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SomiWorks.Class
{
    public class GeneralLedger
    {
        public int id { get; set; }
        public int locationId { get; set; }
        public string subsidiary { get; set; }
        public DateTime business_date { get; set; }
        public string posting_period { get; set; }
        public string debit_total { get; set; }
        public string credit_total { get; set; }
        public string status { get; set; }
        public int customerInternalId { get; set; }
       
    }
    
}