﻿using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Fondant.myFondant.Web.Code;
using Fondant.myFondant.Web.Models;
using System.Web.Profile;

namespace Fondant.myFondant.Web
{
    public partial class _Default : Page
    {
        public void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                // Change the current path so that the Routing handler can correctly interpret
                // the request, then restore the original path so that the OutputCache module
                // can correctly process the response (if caching is enabled).

                //added
                
                 Response.Redirect("/Home/Index");
                 ////****Response.Redirect("/Account/ClientLogIn");

                /*Response.Redirect("/Home/Home"); 
                IFormsAuthenticationService FormsService= new FormsAuthenticationService();
                IMembershipService MembershipService = new AccountMembershipService();          

                MembershipService.ValidateUser("50200_admin", "ST4RTR3#");
                FormsService.SignIn("50200_admin", false);*/
                //end of added code
                //myFondantBaseProfile clientaccount2 = new myFondantBaseProfile(); //removed for testing

                //myFondantBaseProfile clientaccount2 = ((myFondantBaseProfile)(ProfileBase.Create("50200_admin"))); //added for testing

                //ProfileBase profile = ProfileBase.Create("50200_admin");
                //profile.SetPropertyValue("UserName", "50200_admin");
                //profile.Save();
                //System.Web.Profile.ProfileBase.Create("50200_admin"); //removed. Try to uncomment once default.aspx.cs is being called
                //clientaccount2.ClientAccountNumber = "50200";

                string originalPath = Request.Path;
                HttpContext.Current.RewritePath(Request.ApplicationPath, false);
                //System.Web.HttpContext.Current.RewritePath(Request.ApplicationPath, false);
                IHttpHandler httpHandler = new MvcHttpHandler();
                httpHandler.ProcessRequest(HttpContext.Current);
                //httpHandler.ProcessRequest(System.Web.HttpContext.Current);
                HttpContext.Current.RewritePath(originalPath, false);
                //System.Web.HttpContext.Current.RewritePath(originalPath, false);
            }
        }
    }
}
